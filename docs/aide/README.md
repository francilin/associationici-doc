---
title: "Aide"
---
# Questions fréquentes

## Comment ajouter des icones ?

Simplement copier-coller des emojis dans le texte. Vous pouvez par exemple en trouver [ici](/aide/emojis.md).

Les codes comme ceci `:smiley:` fonctionnent aussi, essayez !