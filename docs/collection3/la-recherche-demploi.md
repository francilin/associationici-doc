---
title: 📑 La recherche d'emploi
---
# Recherche d’emploi (avec un ordinateur et un Smartphone)

::: tip Modules APTIC
* *20 : Panorama des plateformes de recherche d’emploi *
* *125 : Diffuser son CV en ligne *
* *127 : Organiser sa recherche d’emploi*
* *128 : Découverte et usages de l’Emploi Store*

* **Résumé**

L’objectif de ce module est de s’approprier différents sites et applications permettant d’accéder à des offres d’emploi (Pôle Emploi, Indeed, seront particulièrement détaillés) et d’y postuler au moyen d’un ordinateur ou d’un Smartphone.

Sera également présenté l’Emploi Store et quelques sites partenaires (emploi, formation), dont Memo, qui permet d’organiser sa recherche d’emploi (état des candidatures, historique des actions...).

* **Nombre d’heures** 3h
* **Publics** Grands débutants / Intermédiaires / Agiles
* **Pré-requis**

  * Avoir un Smartphone pour faire une recherche d’emploi au moyen d’une application.
  * Avoir suivi le module manipulation du Smartphone.
  * Manipulation du clavier et de la souris, navigation.
  * Avoir une boîte mail fonctionnelle pour envoyer une candidature.
  * Avoir un compte Pôle Emploi si des recherches veulent être faites sur le site de Pôle Emploi.
  * Pour une candidature (exercice), disposer d’un CV et d’une LM.
* **Objectifs**

  * Découvrir le fonctionnement de la recherche d’offres d’emploi via certains sites et applications importants (Pôle Emploi/Mes offres, Indeed...).
  * Savoir où et comment candidater (pas-à-pas).
  * Mobiliser son Smartphone pour répondre à des offres d’emploi.

:::

* **Sommaire**

[[toc]]

## **1. Rechercher un emploi à l’ère numérique**

« Les nouvelles technologies ont transformé la société… et le marché de l’emploi. Entre les sites spécialisés, les [candidatures par e-mail](https://www.pole-emploi.fr/candidat/5-astuces-pour-ecrire-un-e-mail-de-motivation-percutant-@/article.jspz?id=249713) \[contact par e-mail avec en PJ, généralement, CV et LM] ou les réseaux sociaux professionnels \[comme LinkedIn], internet représente le 1er endroit où candidats et recruteurs prennent contact. C’est ce que montre une étude menée par RégionsJob auprès de 346 recruteurs en 2015: près de 9 recruteurs sur 10 déclarent utiliser les *jobboards* (sites d’emploi) pour rechercher des candidats.

**Les canaux de recrutement par secteur d’activité**

• Les moyennes et grandes entreprises envisagent de recourir à Pôle emploi

Plus l’entreprise est importante en termes d’effectifs, plus elle envisage de recourir aux services de Pôle emploi. La moitié des établissements de plus de 10 salariés anticipe un recours exclusif à Pôle emploi ou en association avec d’autres modes de recrutement dès le départ de leurs procédures de recrutement. Cette confiance est encore plus forte si l’entreprise connaît des difficultés de recrutement.

• TPE \[Très Petite Entreprise], agriculture, commerce de détail : avantage aux relations personnelles et aux salariés ayant déjà travaillé dans l’entreprise

Les petites entreprises (moins de 10 salariés) ainsi que le secteur agricole s’appuient plus sur leur réseau et relations personnelles que sur les professionnels du recrutement. Parmi les canaux préférés de ces recruteurs : un vivier de candidats déjà en vue, comme d’anciens salariés ayant déjà travaillé dans l’entreprise… On remarque la même tendance pour le commerce de détail.

• Cadres, industrie et services qualifiés aux entreprises : des recrutements ciblés

Quand le recrutement concerne des postes de cadre, le réseau professionnel (collègues, partenaires, clients, fournisseurs) ou des modes de recrutements spécifiques comme le recours à des cabinets de recrutement sont privilégiés. \[...] »

## **2. Où cherchez des offres d’emploi et de formation ?**

### **1. Les jobboard**

« Un site d'emploi (en anglais, *jobboard* ou *job board*) est un site web qui affiche des offres d’emploi. L'un des tout premiers site d'emplois français fut Cadremeploi \[1996].Les autres grands sites d'emplois généralistes français sont : Keljob, \[…] [Monster, Indeed etc.

Les sites d'emploi se divisent généralement en deux parties :

* les *offres d’emploi* : généralement accessibles à l'aide d'un moteur de recherche permettant de filtrer les résultats (par zones géographiques, spécialités, ou fonctions) ;
* la *CVthèque* : les candidats peuvent y déposer leur [CV](https://fr.wikipedia.org/wiki/CV) afin de se rendre visibles aux recruteurs.

La vocation première d’un site d'emplois réside dans la publication et la diffusion d’offres d’emploi à destination des demandeurs d’emploi.

Les offres d’emploi, déposées par les recruteurs, peuvent être consultées librement par les visiteurs. Elles sont généralement accessibles dès la première page du site, via un formulaire ou un moteur de recherche.

La Cvthèque est la base de données des CV enregistrés par les demandeurs d’emploi sur le site d'emplois. Elle est généralement accessible uniquement par les recruteurs adhérents ou clients du site. \[...]

Les sites d'emplois proposent deux grands types de services aux recruteurs :

* dépôt d’offres d’emploi
* consultation des CV de la CVthèque

Les sites d'emplois proposent deux grands types de services aux candidats :

* dépôt de candidature aux offres d’emploi
* dépôt de CV dans la CVthèque

La plupart des sites d'emploi proposent par ailleurs une fonctionnalité d’alerte e-mail. Les candidats reçoivent ainsi par e-mail les nouvelles offres parues correspondant à leurs critères de recherche.

Certains proposent enfin du contenu éditorial sur l’emploi et des conseils sur la recherche d’emploi.

\[Parmi les sites d’emplois, certains] sites d'emplois sont généralistes, d’autres spécialisés. Peut être qualifié de « généraliste » un site d'emplois qui propose des offres d’emploi à tout type de demandeur d’emploi, quel que soit son métier, son secteur d’activité ou encore son niveau d’étude. Quelques sites d'emplois généralistes : \[Indeed, Pôle Emploi (fonctions pour la recherche d’offres), Monster, Keljob, MeteoJob etc.]...

Au contraire, un site d'emploi « spécialisé » propose des offres d’emploi à un public restreint. » Ce sont généralement des sites qui se distinguent par leurs publics-cibles : cadres (APEC, cadremploi), demandeurs d’emploi dans tel secteur d’activité, par exemple, culturel (jobculture.fr), santé (emploisoignant.com), dans le public (emploipublic.fr), mécanique (mecajob.fr) etc.

### **2. L’Emploi Store**

![](/upload/emploistore1.png "Page d'accueil Emploi Store")

*« Qu’est-ce que l’Emploi Store ? »*: « Vous cherchez des outils efficaces pour votre recherche d’emploi ? Ou bien de quoi vous aider à changer de voie ou à améliorer votre projet professionnel ? Le site emploi-store.fr vous propose le meilleur du web de l’emploi et de la formation.

Se former avec un MOOC, enrichir votre CV, vous préparer à un entretien en mode virtuel, rechercher des offres d’emploi… , l’Emploi Store améliore votre recherche d’emploi et s’adapte à vous.

Avec l’Emploi Store, de nouvelles idées et une énergie renforcée sont à votre portée.

L’Emploi Store ? Le meilleur de Pôle emploi et de ses éditeurs pour l’emploi et la formation. \[...] »

L’Emploi Store est un site, mis notamment en place par Pôle Emploi, qui recense tout un ensemble de sites et d’applications en lien avec la recherche d’emploi au sens large : des méthodes de création du CV et de la LM aux sites de recherche d’offres d’emploi et de formations, en passant par des sites dédiés aux MOOC pour améliorer ses compétences/connaissances dans divers domaines (langues, management, environnement…). On peut s’y connecter au moyen de son identifiant et mot de passe associés à son compte Pôle Emploi ou en créant un compte (favoris, notifications etc.).

Une barre de recherche (mots-clés) et des filtres permettent d’affiner les résultats de sa recherche (\[les sites] les plus récents, les plus utilisés, les mieux notés ; est-ce que l’on cherche un MOOC \[Massive Open Online Course], un moteur de recherche, réseau social… ; est-ce que le site est compatible avec une application mobile…).

Chaque site/application est présenté en quelques mots, dans ses fonctions principales, sont mentionnés les contenus que l’on pourra y trouver, etc. La présentation peut être accompagnée d’une vidéo. Très souvent, l’internaute est redirigé vers le site via un lien.

Pratique, il est décomposé en plusieurs rubriques \[Plan du site]:

![](/upload/emploistoreplandusite.png "Plan du site Emploi Store")

![](/upload/preparersacandidature.png "Préparer sa candidature à l'Emploi Store")

Sites et applications recommandés, rubrique « Préparer sa candidature ».

Présenter les différents filtres possibles ; déroulant en haut à gauche des résultats (les plus récents, les plus utilisés…) + filtres (MOOC, moteur de recherche, compatible avec application...)

« C’est parti » => redirection vers le site Memo, dans un nouvel onglet.

Descriptif du site Memo, pour organiser ses candidatures.

Quelques vidéos de présentation

Notes par les utilisateurs

Services associés

![](/upload/memoemploistore.png "Recherche Mémo Emploi Store")

![](/upload/memoemploistore2.png "Avis sur Memo")

En dehors des jobboards, « La bonne boîte », un outil pratique pour accéder au « marché caché de l’emploi ».

Aussi, à côté de la recherche d’offres d’emploi à proporement parler, « La bonne alternance » et « La bonne formation » sont deux outils essentiels pour la recherche d’une formation.

![](/upload/jobboards.png "Service La Bonne Emploi Store")

### **3. La bonne boîte**

*Description sur Memo :*

« Dès décembre 2018, utilisez le nouveau bouton 'postuler' sur les fiches entreprises. :

Avantages pour vous :

La possibilité de postuler facilement, sans sortir du site.
Un modèle de réponse mail modifiable pour accompagner votre candidature.
L’optimisation des réponses des recruteurs, grâce aux boutons de réponse rapide ajoutés sur le mail de candidature (Refus / demande d’informations complémentaires/Proposition de rendez-vous).

Envoyez votre CV à la bonne entreprise ! Découvrez en un clic les entreprises qui recrutent dans votre métier, près de chez vous.

Envoyez-vous des candidatures spontanées ?

Oui… alors essayez ce nouveau service ouvert à toute la France métropolitaine et DOM.

Si vous êtes donc confrontés à la difficulté de cibler les entreprises auxquelles envoyer votre CV, Pôle emploi vous propose de tester ce service qui vous indique les entreprises qui ont le plus de chance de vous embaucher.
Sur la base d’algorithmes statistiques évolués, La Bonne Boite vous restitue une liste d’entreprises ayant une forte probabilité d’embaucher dans les 6 mois à venir.

Mieux qu’un annuaire, La Bonne Boite cible pour vous uniquement les entreprises n’ayant que des perspectives élevées d’embauche. Utiliser ce nouveau service va vous permettre d’être plus efficace dans vos démarches spontanées, en limitant l’envoi aléatoire de candidature, en limitant les retours négatifs et en augmentant vos chances de décrocher un entretien d’embauche. »
« La bonne boîte » recense des entreprises et autres structures qui sont susceptibles de recruter dans les 6 prochains mois. Il est possible de se connecter au site via son compte Pôle Emploi.

Une barre de recherche permet, à partir de mots-clés (métier, secteur d’activités…), d’afficher des entreprises/structures qui recrutent dans un secteur géographique donné. Il est possible de générer une fiche par entreprise, avec un certain nombre d’informations (le nom de la personne à contacter, son adresse mail et un numéro de téléphone).

On peut envoyer une candidature spontanée à l’entreprise de son choix via le site ou via sa boîte e-mail.

![](/upload/la-bonne-boite.png "Page d'accueil La Bonne Boite")

![](/upload/recherche-sur-la-bonne-boite.png "Recherche sur La Bonne Boite")

Le nombre d’entreprises identifiées apparaît en haut de la page. Chaque bloc représente l’une d’entre elles.

Les blocs sont triés par « potentiel d’embauche » par défaut. Ce qui signifie que les premiers résultats ont plus de chance que les suivants d’embaucher sur un métier donné (algorithmes du site à l’œuvre).

A partir du bouton « Plus d’infos + » présent sur chaque bloc, il est possible de trouver plusieurs renseignements nécessaire à la candidature spontanée.

Vous pouvez également « Postuler »

![](/upload/selectiond-uneentrepriselabonneboite.png "Sélection d'une entreprise La Bonne Boite")

Quatre informations à retenir :

* La personne à contacter
* Le mode de contact à privilégier
* L’adresse et la localisation de l’entreprise sur la carte interactive
* La possibilité de télécharger la fiche et de se l’envoyer par mail

Vous pouvez envoyer également une candidature spontanée via le site La bonne boîte ou à partir de votre propre boîte e-mail.

Pour envoyer une candidature spontanée, il suffit de cliquer sur « Postuler ». C’est une mention qui figure sur chaque espace dédiée à une entreprise.

![](/upload/postulerlabonneboite.png "Postuler La Bonne Boite")

Après avoir cliqué sur « Postuler » : deux options : faire une candidature spontanée via La bonne boîte ou la faire à partir de sa boîte e-mail personnelle.

![](/upload/postulerlabonneboite2.png "Postuler La Bonne Boite 2")

Le moment de candidater avec des informations à remplir : nom du poste / n° de tél + adresse / écriture d’un e-mail… / Dans un second temps, après avoir cliqué sur « Je continue », vous pourrez insérer des PJ : CV / LM, ...

Un récapitulatif final de la candidature peut être consulté avant l’envoi (ici via le site donc) de celle-ci.

![](/upload/recapitulatifcandidaturelabonneboite.png "Recapitulatif La Bonne Boite 1")

![](/upload/recapitulatifcandidaturelabonneboite2.png "Recapitulatif La Bonne Boite 2")

### **4. La bonne formation**

Présentation de La bonne formation sur le site Mémo :

« *Accédez aux formations efficaces sur votre marché du travail. Repérez en un clic comment les financer en fonction de VOTRE profil !*»

Avec La Bonne Formation accédez :

* à l’ensemble des formations \[sur la] France entière, regroupant les catalogues de formations accessibles aux demandeurs d'emploi.
* aux données sur l’efficacité de l’ensemble des domaines de formation par région
* au moteur de financement des formations pour les régions Pays de la Loire, Centre Val de Loire, Corse et Île de France intégrant l’ensemble des règles nationales et les règles spécifiques à ces régions. Et bientôt pour les autres régions ... ».

![](/upload/labonneformation.png "La Bonne Formation")

Grâce à La bonne formation, il est possible de chercher à partir des mots-clés et d’une géolocalisation des formations.

A partir de la liste des différentes formations recensées, on peut accéder à des informations plus précises sur chaque formation (volume d’heures, contenu du programme, coordonnées, contact…).

La bonne formation permet également d’estimer les modalités de financement de la formation qui vous intéresse. Lorsque vous êtes sur la page d’une formation, il faut alors cliquer sur « Combien ça me coûte ? » => une nouvelle page s’ouvre « Renseigner ma situation pour connaître mes possibilités de financement », à partir de laquelle, il faut renseigner un certain de données concernant votre profil professionnel, vos revenus, etc. 

Le site permet aussi d’obtenir des informations concernant des entreprises où il sera possible de faire un stage immersif. La procédure pour faire un tel stage est explicité sur la page d’accueil du site La bonne formation.

![](/upload/recherchelabonneformation.png "Recherche sur La Bonne Formation")

Liste des formations disponibles après une recherche : « Intervention socioculturelle » et « Paris ».

Sur la gauche, un certain nombre de filtres qui permettent d’affiner la recherche en cours : public / financement Pôle Emploi / contrat en alternance / niveau de diplôme souhaité / ...

![](/upload/pageduneformation.png)

Page d’une formation : son descriptif, les sessions en cours et à venir, la durée, les conditions pour accéder à la formation, les coordonnées de la structure...

![](/upload/renseignementsituation.png "Renseignement de la situation La Bonne Boite")

Après voir cliqué sur « Combien ça me coûte ? » ===> ouverture d’une page « Renseigner ma situation pour connaître mes possibilités de financement » : en plusieurs étapes, il s’agit d’un parcours où l’on remplit des informations sur sa situation professionnelle, ses revenus, etc., pour que s’affichent les différentes options de financement de la formation qui nous intéresse...

![](/upload/optionsdefinancement.png "Options de Financement La Bonne Boite")

Options proposées pour financer la formation choisie.

Pour accéder au moteur de recherche d’entreprises afin d’y faire éventuellement un stage immersif, il faut cliquer sur la page d’accueil de La bonne formation sur « Tester un métier »…

![](/upload/testerunmetier.png "Tester un Métier La Bonne Formation")

Page de recherche d’entreprises pour faire un stage immersif.

Un tuto vidéo rappelle ce qu’est le stage immersif, le but, les démarches qu’il faut faire pour pouvoir en faire un. Pour qu’un stage immersif commence dans les règles, il faut qu’une convention, téléchargeable notamment sur La bonne boîte, soit signée entre les trois parties : le stagiaire / l’entreprise / le Pôle Emploi.

![](/upload/listeentrepriseaprèsrecherche.png "Liste Entreprises après une recherche La Bonne Formation")

Liste d’entreprises après une recherche.

Il est possible de télécharger :

* La « Liste des entreprises » recensée après votre recherche.
* Une fiche de « Conseils » pour aider le potentiel stagiaire à « vendre » son stage immersif auprès de l’entreprise qui l’intéresse.
* « La convention » de stage tripartite.

![](/upload/contacterlentreprise.png "Contacter une entreprise La Bonne Formation")

En cliquant sur « Contacter l’entreprise », une petite fenêtre s’ouvre sur la même page avec les coordonnées téléphoniques et e-mail de l’entreprise, ainsi que quelques conseils à destination du futur stagiaire….

![](/upload/contacterlentreprise.png)

### **5. La bonne alternance**

*Qu’est-ce que La bonne alternance ?*

La bonne alternance est recommandé par l’Emploi Store, il s’agit d’un service proposé par le Pôle Emploi.

« Pôle Emploi innove et propose ce service pour vous permettre de trouver plus facilement des entreprises proposant régulièrement des contrats en alternance. La Bonne Alternance est une start-up interne de Pôle Emploi créée et développée par des conseillers »

« 7 employeurs sur 10 recrutent sans déposer d’offres d’emploi. Il est donc essentiel dans votre recherche de proposer votre candidature à des entreprises n’ayant pas forcément déposé d’offre d’emploi en alternance. Pas toujours facile de cibler vos recherches ! Notre algorithme La Bonne Alternance analyse les offres et les recrutements des 5 dernières années pour vous proposer les entreprises qui recrutent régulièrement en alternance (contrat d'apprentissage ou contrat de professionnalisation). Pour une meilleure lisibilité, les résultats sont affichés sur une carte et sur une liste. Vous pouvez affiner la liste des entreprises par taille, métier et domaine. En cliquant sur une entreprise, vous accédez à ses données-clés, à ses coordonnées ainsi qu'à des conseils pour postuler. Maximisez vos chances ! Postulez auprès des entreprises qui recrutent régulièrement en alternance sans forcément avoir déposé d’offre. C’est parti ! »

![](/upload/ecranaccueillabonnealternance.png "Ecran d'accueil La Bonne Alternance")

Écran d’accueil du site La bonne alternance : une barre de recherche à partir de laquelle on inscrit un ou plusieurs mots-clés pour définir un métier ou un domaine d’activités.

![](/upload/définitionmetiersinteretlabonnealternance.png "Définition Metiers Interet La Bonne Alternance")

Après avoir entré la mot-clé « animation », plusieurs propositions de domaines sont proposés...

![](/upload/définition-secteur-géographique-la-bonne-alternance.png "Définition Secteur Géographique La Bonne Alternance")

Définir une zone géographique pour délimiter la recherche de structures à contacter pour faire son alternance...

![](/upload/carte-des-résultats-la-bonne-alternance.png "Carte des résultats La Bonne Alternance")

Carte des résultats avec localisation des différentes structures + liste à droite des structures. Cliquez sur « En savoir plus » pour obtenir plus d’informations concernant chaque structure...

![](/upload/enregistremententreprisefavoris.png "Enregistrement d'entreprises dans les favoris")

La sélection d’entreprises enregistrées dans la liste « Mes favoris ». 

Pour enregistrer une entreprise dans la liste, cliquez sur l’icône cœur à droite du nom de l’entreprise...

![](/upload/louperecherche.png "Loupe Recherche La Bonne Formation")

La loupe « Recherche » permet de vous des modifications concernant sa recherche d’entreprise, de changer notamment de domaines d’activités par rapport à celle choisi lors de la recherche initiale...

## 3. Pôle Emploi, Indeed, CornerJob

### 1.Pôle Emploi

#### 1. Le site de Pôle Emploi

Le site de Pôle Emploi, outre les informations que l’on peut y trouver et les démarches qu’il est essentiel d’y faire pour les demandeurs d’emploi, est également important pour effectuer une recherche d’offres d’emploi.

A partir de son compte Pôle Emploi, on peut accéder à tout un ensemble d’offres visibles sur la page « Ma recherche d’offres ».

« Pour chaque offre d’emploi, vous disposez des informations clés :
• Description du poste et du contrat de travail
• Profil recherché par l’entreprise
• Informations sur l’entreprise
• Modalités de candidature »

Possibilité d’enregistrer des annonces et des recherches effectuées avec des mots-clés + une localisation, que l’on peut retrouver plus tard. 

Des alertes emails emails peuvent être crées pour chaque recherche enregistrée (activation/désactivation à tout moment).

Les fonctions principales de « Ma recherche d’offres » (« Ma recherche d’emploi », site de Pôle Emploi)

A l’image du site Indeed, la page « Ma recherche d’emploi » de Pôle Emploi permet d’avoir globalement accès aux mêmes fonctions :

A l’image du site Indeed, la page « Ma recherche d’emploi » de Pôle Emploi permet d’avoir globalement accès aux mêmes fonctions :

• Faire une recherche simple d’offres d’emploi au moyen de mots-clés, de noms de métier, compétences, voire de n° d’offre + d’une localisation (ville, département, région). D’autres filtres permettent également d’affiner les résultats : type de contrat, date de parution de l’annonce, domaine professionnel, expérience, qualification etc.

• Enregistrer les offres que l’on souhaite, et qui apparaissent ensuite dans une rubrique « Ma sélection d’offres ».

• Enregistrer une recherche effectuée, et qui apparaît ensuite dans « Mes recherches enregistrées » (+ activation/désactivation d’alertes emails).

• Postuler en ligne directement avec un CV Pôle Emploi (création possible) ou avec un CV importé (importation se fait depuis « Mes compétences ») + une lettre de motivation (écrire ou faire un copier/coller à partir d’un autre texte). On peut également être redirigé vers le site où est initialement parue l’annonce, s’il s’agit d’une offre d’emploi éditée par un partenaire de Pôle Emploi.

• D’autres fonctions permettent aussi d’explorer le marché du travail dans tel secteur d’activité ou selon tel métier, ou encore, d’accéder à une page Pôle Emploi région selon sa région de résidence.

• Dans « Mes candidatures » (« Ma recherche d’emploi » => « Mes candidatures), il est possible de suivre l’état des candidatures envoyées ou des propositions qui ont été faites au candidat.

Les +

* Accès à de nombreuses offres en quelques clics au moyen de filtres efficaces.
* Postuler rapidement avec un CV et une LM.
* Fonctions sauvegarde d’offres d’emploi et enregistrement des recherches effectuées.

Les -

* Attention, si l’on souhaite candidater avec son profil Pôle Emploi (« Ce que je montre » = « Carte de visite » sur l’application), que l’on peut compléter dans « Ma recherche d’emploi » => « Mon profil de compétences », il vaut mieux s’assurer que celui-ci est bien rempli, soigné, et correspond au poste pour lequel on candidate.
* Pas de possibilité de joindre une LM, importée depuis son ordinateur. Possible depuis le Smartphone (?)

![](/upload/ecanconnexionpoleemploi.png "Ecran connexion candidat Pole Emploi")

Écran connexion candidat Pole Emploi

![](/upload/ecranpersopoleemploi.png "Écran Personnel Pole Emploi")

![](/upload/ecranrecherchepe.png "Écran Recherche Pôle Emploi")

![](/upload/ecranrecherchepe2.png "Options de recherche Pole Emploi")

Page principale pour effectuer une recherche d’offres avec mots-clés + localisation.
Sur la gauche, différents filtres permettent d’affiner les résultats (type de contrat, expérience demandée, domaine professionnel, qualification...)

![](/upload/descriptif-d-une-offre-d-emploi.png "Descriptif d'une offre d'emploi")

![](/upload/postulerenligne.png "Postuler en ligne Pole Emploi")

Postuler en ligne : 

* joindre un CV de sa Cvthèque (CV Pôle Emploi ou importé) \[« Ma recherche d’emploi » => « Mes compétences » => « CV et réalisations »]
* joindre un profil (facultatif)
* coordonnées
* copie de la candidature par email (facultatif)

Si vous candidater à une offre en y adjoignant votre Profil de compétence, pensez à vérifier la rubrique « Ce que je montre », à partir de laquelle vous accédez depuis Mon espace personnel (sur le site de Pôle Emploi) => Profil de compétences => Ce que je montre.

Le profil de compétences est constitué de plusieurs parties, que vous pouvez alimenter au gré des formations, des expériences professionnelles que vous avez.

![](/upload/cequejemontrepe.png "Espace Personnel détaillé Pôle Emploi")

**Partie « Votre parcours » :** « Votre parcours, c’est l’ensemble de vos expériences professionnelles, extra-professionnelles et de vos formations. » 

**Partie « Compétences » :** Les compétences regroupent l’ensemble de vos savoirs, savoir-faire, savoir-être professionnels, langues et permis.

Renseigner vos compétences augmente vos chances d’être contacté par un recruteur. Valorisez-les en les rattachant à une expérience ou une formation de votre parcours.

Afin de rendre votre profil plus attractif pour les recruteurs, renseignez votre niveau de maîtrise pour chaque savoir, savoir-faire et langue.

3 niveaux vous sont proposés sous forme d’étoiles : débutant - intermédiaire - avancé. Sélectionnez votre niveau en cliquant sur les étoiles. »

**Partie « CV et réalisations »** : « Dans cet espace, déposez l’ensemble des documents illustrant vos compétences : CV, photos, diplômes, lettres de recommandation, liens vers des vidéos ou des sites internet...

Ces documents vous permettent de vous mettre en valeur auprès des recruteurs.»

**Partie « Votre parcours »** : « Votre parcours, c’est l’ensemble de vos expériences professionnelles, extra-professionnelles et de vos formations. »

**Partie «Métiers recherchés et projets» :** « Jusqu'à 5 métiers et projets peuvent être précisés. Le métier recherché correspond au métier qui peut être exercé dès à présent. Le projet correspond à un nouveau métier ou à un projet de création/reprise d'entreprise. »

![](/upload/cequejemontrepe2.png "Gestion Visibilité Pole Emploi")

**Partie «Ce que je montre» :** Vous pouvez ici voir à quoi ressemble votre profil du point de vue d’un recruteur, si vous toutefois vous candidatez avec une Carte de visite, ce qui n’est pas obligatoire bien sûr. Au minimum, pour candidater il faut généralement un CV et une lettre de motivation.

Il est possible de faire plusieurs Cartes de visite.

Ce sont avec ces Cartes de visite qu’il est possible de postuler sur l’application « Mes offres ». Encore une fois, il n’est pas obligatoire d’en adjoindre une à une candidature. Les modifications que vous voudrez faire ne seront cependant possibles qu’à partir de la version Web de Pôle Emploi, non à partir d’une application.

#### **2. L’application Pôle Emploi « Mes offres »**

Simple d’utilisation, l’application « Mes offres » de Pôle Emploi correspond globalement aux fonctions présentes dans « Ma recherche d’offres » sur le site Internet bien qu’elles soient plus limitées.

L’application permet de :

* Rechercher des offres d’emploi à partir de mots-clés (métier, compétences, entreprise…) + une localisation. On peut effectuer un tri des résultats seulement en fonction de la date et de la pertinence des offres ; pas de filtres avancés.
* Candidater avec un CV (CV de la CVthèque ou import d’un CV) + carte de visite (= « Ce que je montre » sur le site PE) (facultatif) + LM (à écrire ou copier/coller à partir d’un document) + coordonnées. On peut également candidater par email ou être redirigé sur le site où est parue l’annonce.
* Enregistrer ses recherches + activer/désactiver les notifications les concernant : elles se retrouvent dans l’onglet « Recherche » (page principale).
* Enregistrer des offres d’emploi (petite étoile à gauche de l’intitulé du poste) : elles se retrouvent dans l’onglet « Ma sélection ».
* Consulter son profil (« Mon profil ») = parcours et compétences sur le site Pôle Emploi (« Ma recherche d’emploi » => « Mes compétences »). On ne peut pas éditer son profil de compétences à partir de l’application mais seulement du site (redirigé).
* Partager l’annonce à un contact via Whatsapp, SMS, Facebook Messenger, Gmail.

Pour postuler via l’application, il faut s’assurer qu’il y ait la mention « via Pôle Emploi » sur l’offre d’emploi , sinon on est redirigé vers le site où est parue initialement l’annonce.

![](/upload/pagedaccueilapplicationmesoffres.jpg "Page d'accueil Application Mes Offres")

Se connecter permet notamment de postuler via l’application et donc de générer des données concernant son profil.

![](/upload/rechercheapplimesoffres.jpg "Recherche Appli Mes Offres")

Barre de recherche d’offres d’emploi, liste des offres (petite étoile à gauche de l’intitulé du poste pour enregistrer l’offre), enregistrer la recherche avec l’étoile en haut à droite de la page...

![](/upload/modifierprofilapplimesoffres.jpg)

On peut éditer son profil et donc sa carte de visite (= cf. « ce que je montre » sur le site de Pôle Emploi) uniquement sur le site de pôle-emploi.fr

![](/upload/exemple-d-une-annonce-pe-mes-offres.jpg "Exemple d une annonce Mes Offres Pole Emploi")

Descriptif d’une annonce, possibilité de partager l’annonce ou de postuler.

![](/upload/rubriques-mes-offres-pe.jpg "Rubriques Mes Offres Pole Emploi")

Les différentes rubriques à remplir avant d’envoyer la candidature.

Carte de visite \[facultative].

Pas possible d’importer une lettre de motivation.

*Pour rappel, les Cartes de visite sont des Profils de compétences que l’on peut éditer à partir du site de Pôle Emploi, qui reprennent des données du type : métier recherché, formations, exp. pro. et  extra-pro, compétences, savoir-être, etc.*

#### **3. L’application Mon espace**

L’application « Mon espace » est une autre application Pôle Emploi, plutôt dédié aux démarches administratives. Par exemple, on pourra y faire son actualisation mensuelle en tant que demandeur d’emploi, obtenir des informations sur son indemnisation, transmettre des documents, ou encore, générer au format PDF des attestations de situation.

![](/upload/pagedaccueilapplicationmonespace.png "Page d'accueil Application Mon Offre Pôle Emploi")

Écran d’accueil lorsque l’on ouvre l’application « Mon espace », avec les différentes rubriques : Mon actualisation, Mon indemnisation, Mes attestations...

![](/upload/se-connecter-mon-espace-pe.jpg)

Pour pouvoir utiliser l’application « Mon espace », il faut être demandeur d’emploi, avoir donc un identifiant Pôle Emploi et un mot de passe associé. 

En vous connectant à l’application Mon Espace, Pôle Emploi mobilise un certain nombre de données qui sont présentes sur votre compte Pôle Emploi (indemnisation, profil de compétences, etc...)

![](/upload/optionsmonespacepe.png "Menu Mon Espace PE")

**Ce que l’on trouve par rubriques :**

**Paramètres \[icône écrou en haut à gauche]:**

* Activer ou désactiver les notifications
* Modifier son code secret
* Changer de compte
* Se déconnecter

**Mon actualisation :**

* Les intervalles pendant lesquels il faut s’actualiser (entre le \[date] et le \[date]) = déclarer si on a travaillé, si on a suivi une formation, si on a perçu une pension (invalidité), déclarer ses revenus etc.
* Dates de paiement si l’on perçoit une indemnité chômage.
* Possibilité d’ajouter ces intervalles pour l’actualisation à son propre agenda, sur son Smartphone.

**Mes courriers :**

* Tous les échanges avec Pôle Emploi : relevé de situation (= pour la période et le montant des indemnisations…), rappel à un RDV pour un entretien avec son conseiller, notification ARE (= Allocation chômage d’aide pour le Retour à l’Emploi)
* Possibilité de télécharger les documents en PDF
* On ne peut pas écrire directement à Pôle Emploi, seulement à son conseiller ou sa conseillère. Sinon, il faut passer par une plateforme téléphonique, qui nous redirige généralement vers notre Pôle Emploi de référence (on donne informations sur identifiant, mot de passe (anciens numéros) + numéro du département où l’on vit).

**Mes justificatifs :**

* Les justificatifs que PE nous demande : RIB, par exemple, Relevé d’Identité Bancaire, pour pouvoir recevoir les indemnisations, si on en a.
* Les justificatifs que l’on envoie nous-même lorsque par exemple on a fini une période de travail, par exemple un CDD de 6 mois => Attestation Employeur, document qui récapitule la période travaillée, le poste occupée dans telle entreprise.

**Ma situation :**

« Ce service permet de déclarer vos changements de situation, votre dossier sera mis à jour le lendemain. Vous ne pouvez déclarer par jour qu’un type de changement, celui-ci étant modifiable jusqu’à 16h30 ».

* Absence (35j max)
* Arrêt maladie
* Entrée en stage
* Reprise d’activité

**Mon indemnisation :**

* Montant de l’allocation mensuelle + le reliquat (= nombre de jours d’indemnisation restants à telle date)
* Attestation : important : justificatif qui peut être demandé par une administration, dans le cadre de certaines démarches, ou pour bénéficier de tarifs réduits pour des activités, des sorties, des spectacles…
* Attestation de paiement : les indemnités perçues sur telle période
* Attestation des périodes indemnisées…
* Attestation de situation
* Attestation des périodes d’inscription

**Mon agence :**

* Chercher des adresses de Pôle Emploi proche de là on on est.

**Contacter mon conseiller** :

* Le nom et prénom du conseiller, vous pouvez envoyer depuis l’application un mail (choix du sujet du mail, pourquoi vous contactez la personne) ===> redirige vers Gmail, par exemple, ou Outlook

#### **4. Sites partenaires de Pôle Emploi**

Il existe une grande quantité de sites Internet et d’applications, généralistes ou spécialisés, permettant d’effectuer des recherches d’emploi, bien que les offres peuvent souvent se recouper d’un site à un site.

Quelques exemples de sites mentionnés sur l’Emploi Store et partenaires de Pôle Emploi :

![](/upload/applisemploi.jpg)

La liste des partenaires de Pôle Emploi :

« *Ces partenaires sélectionnés par Pôle emploi sont liés par une convention qui garantit la gratuité, la qualité, la légalité et la fraîcheur de leurs offres mises en visibilité sur pole-emploi.fr.* »

![](/upload/partenairespe.jpg)

### **2. Indeed, site et application**

**Le site Indeed**

Dans l’« A propos » du site Internet, on peut lire :

« *Indeed est le 1er site d'emploi mondial enregistrant plus de 250 millions de visiteurs uniques2 chaque mois. Indeed s'efforce de faire des candidats une priorité en leur permettant de chercher un emploi, de publier leur CV et de se renseigner au sujet des employeurs, le tout gratuitement. Chaque jour, nous offrons de nouvelles opportunités à des millions de personnes.* ».

En quelques chiffres, Indeed, c’est :

250 millions de visiteurs / mois, 150 millions de CV, 150 millions d’évaluations et d’avis d’entreprises, 10 offres d’emploi ajoutées partout à travers le monde / seconde.

Indeed est le site leader de recherche d’offres d’emploi. La plupart des offres que vous trouverez sur Indeed se retrouvera également sur d’autres jobboards généralistes comme Monster ou KelJob. Indeed agrège les offres disponibles sur d’autre sites de recherche d’emploi, généralistes ou spécialistes, qui peuvent également provenir de journaux ou de pages d’entreprises.

**Les fonctions principales d’Indeed :**

* Faire une recherche simple d’offres d’emploi au moyen de mots-clés, de noms de métier, d’entreprise, de secteur d’activités... + d’une localisation (ville, département, région). Faire une recherche avancée avec des critères plus précis pour mieux cibler les annonces qui peuvent nous intéresser.
* Consulter les dernières offres parues depuis les dernières recherches que l’on a effectuées (« Mes recherches récentes », n’apparaissent plus si historique supprimé).
* Postuler en ligne directement (« Postuler ») avec un CV Indeed (création possible) ou avec un CV importé + une lettre de motivation importée (facultatif). Des questions supplémentaires peuvent être demandées par l’employeur selon l’offre. On peut également être redirigé vers le site où est initialement parue l’annonce (« Continuer pour postuler »).
* Sauvegarder ou envoyer par email ses offres favorites.
* Créer des alertes emails.
* Consulter et laisser des avis sur les entreprises.
* Enfin, la création d’un compte Indeed permet de gérer ces différentes fonctions, c’est-à-dire, principalement : voir les annonces sauvegardées ; voir les CV qui ont été envoyés ; avoir une messagerie si échanges avec les structures où l’on a candidaté ; créer un CV ou en importer un depuis son ordinateur/Smartphone ; suivre et supprimer ses alertes emails.

**Les + :**

* Accéder en quelques clics à un grand nombre d’offres d’emploi en effectuant une recherche simple ou avancée (des offres que l’on retrouve souvent sur d’autre sites).
* Postuler rapidement avec un CV et une LM (facultative). Pas besoin d’avoir un compte pour postuler.
* Fonctions sauvegarde d’annonces et consultation des offres parues depuis les dernières recherches effectuées.

**Les - :**

* Beaucoup de candidatures envoyées chaque jour par les internautes, risque que sa candidature se retrouve dans un océan d’autres candidatures (?).
* Une candidature *via* Indeed n’est pas une candidature où l’on contacte directement l’entreprise, le recruteur ; le site sert alors d’intermédiaire lorsque l’on n’est pas redirigé vers le site où est parue l’annonce.

![](/upload/pageaccueilindeed.png "Page d'accueil d'Indeed")

Comme sur beaucoup de jobboards, dès la page d’accueil, il est possible d’effectuer une recherche d’offres d’emploi : on peut mentionner un métier, un ou plusieurs mot-clés, un nom d’entreprise + une localisation (ville, département…).

![](/upload/recherchesimpleindeed.png "Recherche Simple Indeed")

Recherche Simple Indeed

![](/upload/rechercheavancee.png "Recherche Avancée Indeed")

Recherche avancée avec des critères plus précis concernant les mots-clés, le type de contrat, la date de parution de l’annonce...

![](/upload/resultatsrechercheindeed.png "Résultats Indeed")

Page générée après une recherche simple avec le mot-clé « vendeur », « Île-de-France ».

Liste d’offres avec intitulé du poste, nom de l’entreprise, localisation ; pour un descriptif du poste, il faut cliquer sur l’offre et un espace s’ouvre à droite, à partir duquel on peut postuler.

A gauche, plusieurs critères disponibles pour affiner les résultats : entreprise, lieu, intitulé de métier plus précis etc.
A droite, espace « Recevez par email les nouveaux emplois correspondant à cette recherche » (possibilité de faire cette action quand on postule à une offre « M'avertir par email dès que des emplois similaires sont disponibles » ou « Recevoir des notifications par SMS relatives à cette candidature » (case pré-cochée !))

![](/upload/rubriquesindeed.png "Rubriques Indeed")

Rubriques du compte Indeed : gestion de CV (importé ou version Indeed), offres sauvegardées, avis laissés, alertes emails, paramètres du compte etc.

**L’application Indeed**

L’application, plus sommaire, ressemble largement au site Internet. Possibilité de se connecter à son compte, d’en créer un ou de postuler sans se connecter à des offres(« Postuler » => « Téléchargez votre CV »). Certaines fonctions plus avancées sur le site n’apparaissent pas, notamment en ce qui concerne la recherche avancée.

La recherche d’offres d’emploi s’effectue au moyen de « filtres » (date de parution de l’annonce, type d’emploi, tri par date ou pertinence). Les filtres sont moins nombreux que les multiples critères qui permettent sur le site d’affiner la recherche et donc les résultats. Un filtre « Emplois compatibles mobile » permet de sélectionner les offres auxquelles il est possible de candidater au moyen du Smartphone.

![](/upload/appliindeed.jpg "Page d'accueil, écran de connexion et paramètres application Indeed")

Page principale de l’application (avec un compte connecté) : possibilité de faire directement une recherche, puis accès ensuite aux rubriques du compte, offres sauvegardées, alertes emails crées etc.

![](/upload/optionsappliindeed.jpg "Options Application Indeed")

A gauche, rubrique « Mes emplois » avec offres sauvegardées, offres consultées etc.

A droite, rubrique des « Alertes Emploi » (= « Mes souscriptions »)

![](/upload/resultatsrechercheindeed2.png "Résultats Recherche Indeed")

Page de résultats et page du descriptif du poste.

Possibilité de sauvegarder des annonces (îcone coeur), possibilité de ne plus faire apparaître des annonces (îcone ensemble vide).

![](/upload/candidaterposteindeed.png "Candidater à une annonce sur l'application Indeed")

Candidater, comme sur le site, implique de renseigner quelques informations dont prénom et nom, email, de fournir un CV (Inded ou importé) et éventuellement une LM (+ quelques questions supplémentaires).

Possibilité de créer une alerte email pour des postes similaires (cocher ou non « M’avertir lorque des emplois similaires sont disponibles »).

### **3. CornerJob**

CornerJob est un jobboard qui recense tout un ensemble d’offres d’emploi. Il est plutôt axé sur certains secteurs d’activité comme l’hôtellerie, le service à la personne, BTP et industrie.

Un moteur de recherche simple permet d’entrer des mots-clés liés à un métier.

![](/upload/pagedaccueilcornerjob.png "Page d'accueil de JobCorner")

![](/upload/pagedaccueilcornerjob2.png "Bas de la page CornerJob")

![](/upload/resultatsrecherchecornerjob.png "Recherche sur CornerJob")

Résultats d’une recherche à partir du mot-clé « Animateur ».

Pour affiner les résultats, et notamment la localisation des offres à pourvoir, il faut utiliser les différents filtres mis à la disposition sur la gauche de la page de résultats : des filtres par ville, type de poste, secteur d’activité, région, peuvent être sélectionnés.

![](/upload/offresurcornerjob.png "Offre sur CornerJob")

Page d’une offre d’emploi : descriptif, date de début et de fin (ici CDD), expérience demandée, salaire, secteur d’activité...

Pour pouvoir postuler à une offre, il faut compléter un profil avec un certain nombre de données, expériences professionnelles, formations suivies… Ici le CV c’est les informations qui sont remplies lors de la constitution du profil.

![](/upload/profilcornerjob1.png "Page d'informations personnelles CornerJob")

Première partie des informations qu’il faut remplir pour éditer le Profil.

![](/upload/profilcornerjob2.png "Page d'informations personnelles CornerJob 2")

Seconde partie des informations qu’il faut remplir pour éditer le Profil.

**Dans sa version « application »,** il est écrit sur l’Emploi Store :

*« CornerJob est la première application mobile gratuite qui facilite l'interaction entre candidats à l'emploi et recruteurs.»*

CornerJob en 4 mots :

* Simplicité : Création de profil facile et annonces au format tweet (140 caractères).
* Rapidité : Fini les candidatures qui restent sans réponse !
* Interaction : Une fois présélectionné, chattez en direct avec les recruteurs.
* Géolocalisation : Trouvez des opportunités où que vous soyez. 

![](/upload/applicornerjob.png "Application CornerJob")

Création d’un profil avec expériences professionnelles, encart de présentation, formations, compétences linguistiques, secteurs d’activité...

![](/upload/applicornerjob2.png "Application CornerJob")

![](/upload/consultation-offre-cornerjob.png "Consultation des offres CornerJob")

Consulter les offres, leur descriptif, en quelques clics, à partir de la barre de recherche + utiliser des filtres pour affiner les résultats (localisation, type de poste, expérience demandée...), puis postuler avec votre profil  professionnel CornerJob

![](/upload/profil-cornerjob.png "Profil CornerJob")

L’« Aperçu du profil », ainsi que les « Candidatures » en cours.

## **4. Organiser sa recherche d’emploi avec Memo**

Présentation de Memo sur l’Emploi Store :

« *Suivez l'ensemble de vos candidatures en un seul endroit et boostez leur potentiel !*

*Vous postulez à plusieurs endroits et avez du mal à vous souvenir de tout ?*
*Vous attendez trop souvent une réponse à vos candidatures sans engager de nouvelles actions ?*

*Memo est le premier service en ligne qui permet de suivre l'ensemble de ses candidatures et de booster leur potentiel !*

Enregistrez en un clic les offres d'emploi depuis tous les sites \[copier/coller d’URL], ainsi que vos candidatures spontanées et celles auprès de votre réseau relationnel, vous les retrouverez alors sur un véritable *tableau de bord interactif* !

Memo vous dit ce qu’il faut faire au bon moment et comment s’y prendre. Vos candidatures se démarquent alors positivement auprès des recruteurs ! \[…] »

**Les fonctions principales de Memo :**

* \[La connexion à Memo peut se faire avec ses identifiants Pôle Emploi ou avec une adresse email et un mot de passe]
* Recenser toutes les candidatures que l’on fait ou que l’on envisage de faire. On peut les regrouper en quatre catégories : Je vais postuler, j’ai postulé, j’ai relancé, j’ai un entretien.
* Éditer des informations sur l’état d’avancement des candidatures : passer une offre d’une catégorie à une autre, ajouter des notes à une offre, indiquer s’il y a eu des échanges avec le recruteur/l’entreprise, par email, téléphone, s’il y a un entretien de prévu etc.
* Dans le calendrier, consulter l’historique des actions qui ont été faites sur le tableau de bord (par exemple, déplacement d’une carte (une offre) d’une catégorie à une autre).
* Accéder des petits conseils qui s’affichent automatiquement pour chaque offre qui figure dans le tableau : postuler, relancer, remercier… + « Recevez des alertes personnalisées. MEMO identifie pour vous, les actions prioritaires pour accélérer votre retour à l'emploi. Ces actions sont déterminées en fonction de l'état de vos démarches »

L’avantage principal de Memo est qu’il permet d’avoir une vue d’ensemble de l’état d’avancement des candidatures et donc de voir en un coup d’œil les actions qu’il faut entreprendre.

![](/upload/tableaudebordmemo.png "Aspect global du tableau de bord Memo")

Aspect global du tableau de bord Memo

![](/upload/optionmemo.png "Catégorie j’ai postulé Mémo")

Catégorie j’ai postulé, possibilité de voir/modifier la candidature, de déplacer la carte d’une catégorie à l’autre, d’archiver l’offre dans « candidatures terminées »...

![](/upload/pagedetailléejaipostule.png "Catégorie J'ai postulé détaillée Mémo")

Page détaillée d’une offre d’emploi dans la rubrique « j’ai postulé ».
Possibilité d’ajouter des informations si contact par tél ou email (« Mes échanges »), d’ajouter une note, en indiquant « c’est gagné » ou « refus » l’offre sera archivée dans « candidatures terminées »…

**Exercices :**

1. Aller sur le site d’Indeed ou de Pôle Emploi, sauvegarder une annonce et créer une alerte emails (offres similaires ou par rapport à une recherche effectuée selon des mots-clés).
2. Postuler à une annonce sur le site ou l’application Indeed ou Pôle Emploi.
3. Se connecter à Memo pour y importer une offre d’emploi à laquelle on a postulé.

::: tip Ressources

<https://www.pole-emploi.fr/candidat/quels-sont-les-canaux-de-recrutement-utilises-par-les-recruteurs--@/article.jspz?id=409707>

<https://fr.wikipedia.org/wiki/Site_d%27emplois>

<https://candidat.pole-emploi.fr/offres/descriptionrechercheoffre>

<https://www.emploi-store.fr/portail/centredinteret/trouverunemploi>

<https://www.pole-emploi.fr/candidat/de-nouveaux-partenaires-pour-vous-proposer-des-offres-d-emploi-@/article.jspz?id=61463>

<https://www.emploi-store.fr/portail/services/memo>

<https://memo.pole-emploi.fr/>

<https://labonneformation.pole-emploi.fr/>

:::