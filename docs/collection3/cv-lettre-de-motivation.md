---
title: 📃 CV/Lettre de Motivation
---
# Faire un CV et une Lettre de Motivation

:::  tip Modules APTIC

* *124 :  Réalisation d’un CV*
* **Résumé**

Il s’agit de s’approprier le CV et la LM numériques comme outils à mobiliser dans la recherche d’emploi en passant par l’ordinateur (création) et le Smartphone (transfert et mise à disposition du CV).

Une fois le CV créé et complété, il s’agit de l’envoyer par email et/ou de le transférer sur son Smartphone pour le rendre accessible et pouvoir candidater via certaines applications. Deux outils de création seront présentés pour le CV, deux également pour la LM.

* **Outils :** CVDesignR, My-Edwin ; Himp, Motivator.
* **Nombre d’heures :** 3h
* **Publics:** Grands débutants
* **Pré-requis**

  * Manipulation du clavier et de la souris, navigation.
  * Avoir un Smartphone (pour transférer le CV (par câble), aller chercher CV (dans ses fichiers, email))
  * Manipulation du Smartphone.
  * Avoir une boîte mail fonctionnelle pour envoyer le CV.
  * Avoir un CV version « papier » pour remplir, sur la base d’informations existantes, un CV via CVDesignR ou autre.
  * Avoir un compte Pôle Emploi si les personnes veulent se connecter par exemple via PE sur les sites CVDesignR ou My-Edwin. 
* **Objectifs** 

  * S’approprier un outil de création de CV : CVDesignR, My-Edwin.
  * Rendre accessible un CV sur son Smartphone, que l’on peut aller chercher dans ses dossiers ou récupérer sur sa boîte mail (et donc avoir un CV sous la main pour pouvoir candidater via des applications sur Smartphone).
  * S’approprier un outil de création de LM : Motivator, Himp.

:::

**Sommaire**

[[toc]]

## I. Le CV

### 1.Qu’est-ce qu’un CV ?

Loin du parcours de vie, le CV aujourd’hui doit refléter la personnalité du candidat, son parcours, ses compétences, ses qualités. Le CV doit permettre de se différencier.

En principe, un CV doit être suffisamment concis pour que la personne qui recrute puisse rapidemment cerner le candidat/la candidate, son parcours profesionnel, ses compétences, etc., et avoir envie de le/la rencontrer pour un futur entretien. 

Selon Pôle Emploi, un recruteur consacre seulement une trentaine de secondes en moyenne à la lecture d’un CV (voire moins selon la source) !

C’est un exercice où le visuel joue beaucoup et où il convient d’adapter son CV au recruteur potentiel. 

Il existe plusieurs formes de CV, des plus épurés aux plus « visuels » et « colorés », avec des structures et des logiques parfois différentes. 

On peut distinguer trois grands types de CV : le CV chronologique / le CV thématique / le CV combiné (ou mixte). Ils ont des structures différentes et répondent également à des logiques qui ne sont pas tout à fait les mêmes.

1. Le CV chronologique présente les expériences et formations par ordre anté-chronologique, c’est-à-dire des plus récentes aux plus anciennes. Elles peuvent traduire une évolution des missions et responsabilités endossées durant le parcours professionnel, un changement de secteur d’activités etc. Le CV chronologique reste le plus prisé.

   « Très répandu encore, il commence à s’essouffler au profit du CV thématique ou combiné qui incarnent le reflet d’une tendance croissante dans le recrutement : le recentrage d’un profil sur les compétences. »

![](/upload/cvchronologique.png "Exemple de CV chronologique")

2. Le CV thématique : « Le CV thématique est une forme de CV fonctionnelle, très flexible et permettant de souligner vos compétences, votre expérience et vos réalisations lors de votre évolution professionnelle.

   Dans sa conception, on présente dans un premier temps les compétences dans le détail en y expliquant le contexte, les tâches et les résultats liées à l’apprentissage de cette compétence puis on fait un récapitulatif plus bref des formations et expériences professionnelles dans des rubriques séparées. »

 ![](/upload/cvthématique.png)

3. Le CV combiné ou mixte : « De prime abord, la présentation ressemble fortement à celle du CV chronologique : on détaille dans un premier temps sa formation et ses expériences professionnelles du plus récent au plus ancien sans rentrer particulièrement dans le détail des compétences développées. Par la suite, on dédie une rubrique à part entière où l’on évoque un peu plus en détail ses différentes compétences pour terminer sur ses atouts \[/qualités].»

![](/upload/cvmixte.png)

### 2.Présentation de deux outils : CVDesignR, My-Edwin (outil Emploi Store)

[CVDesignR](https://cvdesignr.com/fr)  \[Emploi Store]

Les + :

* Facile de prise en main, contrairement à d’autres sites qui permettent de générer des modèles CV (au format Word, PP).
* On peut aisément changer de modèle de CV pendant ou après avoir rempli le CV.
* Lorsque les informations débordent sur une autre page, deux possibilités : continuer sur une autre (max. 2 pages, après payant) ou réduire la taille d’écriture du texte.
* Possibilité de travailler la mise en page (taille/police/couleur des titres, textes etc. ; agencement des blocs).
* Possibilité de dupliquer un CV créé.

Les - :

* Lors d’un changement de modèle à l’autre, toutes les rubriques ne trouvent pas toujours de correspondance, possibilité de perte d’informations ou problèmes de spatialisation des infos; aussi, tel modèle ne sera pas du tout adapté pour un CV plus détaillé par exemple, donc nécessitera plusieurs pages…
* Il faut payer pour CV de plus de deux pages ; certains modèles de CV sont payants.
* Si connexion via Pôle Emploi, cela génère des CV pré-formatés avec beaucoup d’informations, notamment compétences (si profil Pôle Emploi est un minimum rempli).
  	

Présentation :

CVDesignR est un site Internet qui permet de créer des CV et de les télécharger. Il s’agit d’un site freemium (gratuit, avec des fonctions plus avancées qui sont payantes, par exemple : payer 3€ pour avoir une troisième page). On peut également écrire des LM (à partir de modèles) et chercher des offres d’emploi qui correspondent aux CV créés.

Le site propose différentes formes de CV dont les rubriques sont pré-définies. Les CV répertoriés n’ont évidemment pas tous le même design et la même logique donc (CV thématique, mixte…).

Pour accéder aux fonctionnalités du site, il faut créer un compte. Après avoir choisi un modèle de CV, il faut remplir un certain nombre d’informations pour pouvoir compléter au fur et à mesure le contenu du CV : nom, prénom, expériences, compétences etc.

Une fois que celui-ci est complété, il est possible de le télécharger au format PDF. C’est un format qui permet d’envoyer aisément sa candidature aux recruteurs, par email notamment, en attachant son CV en pièce-jointe.

Il faut dans un premier temps se créer un compte. Attention les informations qui y figurent sont importantes et doivent être bien renseignées, exactes, car elles apparaîtront ensuite dans le contenu du CV.

\[Il est également possible de se connecter au moyen de ses identifiants Pôle Emploi [cf. inconvénient mentionné ci-dessus], LinkedIn]

![](/upload/inscriptionacvdesignr.png "Connexion à CV DesignR")

![](/upload/optionscvdesignrtxt.png "Options CV DesignR")

Dans chaque « bloc » (par exemple, bloc expériences professionnelles), il est possible de supprimer ou d’ajouter des « sous-blocs » supplémentaires  (par exemple, une autre expérience professionnelle), de les déplacer également à l’intérieur du bloc dont ils font partie, de les renommer (manuellement en cliquant directement dessus).

![](/upload/diplomesformationcvdesignr.png "Diplomes et Formations CV DesignR")

![](/upload/blocscvdesignr.png)

Une fois terminée la phase rédaction du contenu et disposition du CV, de sa forme, on peut le télécharger via un clic sur « Télécharger » dans la barre de commandes présente en haut du site.

![](/upload/barremenucvdesignr.png)

[](https://my-edwin.com/signup/pro)

[My-Edwin](https://my-edwin.com/signup/pro) \[Emploi Store]

Nota bene : 

Il faut prendre le temps de bien remplir le profil des activités et des compétences, elles permettent ensuite de les valider ou non pour les expériences professionnelles passées, qui apparaîtront sur le CV.	

Les + : 

* Format parcours où l’on remplit progressivement les informations nécessaires à l’élaboration du CV.
* Permet de faire un CV mixte qui met en avant les compétences acquises de la personne le long de son parcours professionnel (intéressant quand « on ne les connaît pas déjà »).
* CV épuré.

Les - : 

* Pas de prise en main réelle sur l’édition du CV (couleurs, style, agencement des rubriques etc.).
* Requiert un peu de temps (45mns +/-, voire plus selon nombre d’informations à inscrire) pour la création d’une première version de CV.
* Si on indique que l’on a réalisé les mêmes activités à l’occasion de deux expériences pro, ces mêmes activités apparaissent sur le CV, redondance possible. Attention aux choix qui sont faits dans la sélection des activités par exp. pro.

Présentation :

Pour utiliser le site My-Edwin, il faut s’inscrire. Cela nécessite d’avoir une adresse email et de créer un mot de passe. Il faut aller chercher le lien de confirmation d’inscription dans sa boîte emails.

Ensuite, on peut commencer à remplir les différentes informations demandées afin d’alimenter les rubriques du CV. Il s’agit d’une sorte de parcours où l’on commence de l’intitulé du poste recherché (= « projet professionnel », selon les termes du site) jusqu’aux compétences linguistiques.

Pas de surprise, on retrouve les catégories classiques d’un CV.

Le CV édité sera structuré de la façon suivante (forme mixte) \[de haut en bas] :

nom et coordonnées de la personne
intitulé du poste recherché
compétences-clés
expériences professionnelles
formation
expériences extra-professionnelles
langues.

Au fur et à mesure, la personne devra remplir les rubriques suivantes :

* Intitulé du poste pour lequel on candidate (« Quel poste cherchez-vous ? »).
* Activités et responsabilités endossées par le candidat jusque là et niveaux de maîtrise (= «activités» et « niveaux d’expertise » pour chaque activité, de « théorie » à « transmission et évolution »).
* « Compétences » et « Niveau d’expertise » pour chaque compétence.
* Expériences professionnelles (intitulé du poste, lieu, durée) + réalisations (qui s’afficheront en dessous de l’expérience pro à laquelle elles correspondent, une réalisation = le titre de la réalisation + le « résultat obtenu et bénéfice pour l’entreprise »).
* Formation (diplômes, etc.)
* Langues
  On est libre de modifier ensuite les contenus des différentes rubriques, d’ajouter de nouvelles expériences professionnelles, d’enrichir son CV avec des expériences de bénévolat (expériences extra-professionnelles), par exemple, ou de compléter avec des compétences linguistiques.

Déroulé de la création du CV (quelques moments importants) :

![](/upload/deroulecreationcv1.png "Creation CV Etape 1")

Intitulé du poste recherché. Libre de choisir parmi les postes proposés ou d’inscrire celui qui nous correspond en dehors de la liste

![](/upload/deroulecreationcv2.png "Creation CV Etape 2")

Utiles pour remplir ensuite les activités effectuées lors des exp pro, qui apparaissent sur le CV.

![](/upload/deroulecreationcv3.png "Creation CV Etape 3")

Compétences qui apparaîtront dans les compétences-clés en haut du CV.

![](/upload/deroulecreationcv4.png "Creation CV Etape 4")

Les « activités distinctives » forment la base à partir de laquelle, on peut valider les activités effectuées lors des précédentes expériences professionnelles.

![](/upload/deroulecreationcv5.png "Creation CV Etape 5")

Activités qui apparaîtront dans le CV, sous l’exp professionnelle correspondante.

![](/upload/deroulecreationcv6.png "Creation CV Etape 6")

Les formations suivies.

## II. La lettre de motivation (Himp / Motivator)

### 1. Qu’est-ce qu’une lettre de motivation ?

« La lettre de motivation est déterminante si vous voulez décrocher un entretien. Elle doit mettre en avant les savoir-faire que vous avez exposés dans votre CV. C'est en quelque sorte une “répétition générale” de l’entretien d’embauche.
Le pari gagnant d’une bonne lettre de motivation :
    • Vous soignez la présentation. 
    • Vous mettez en avant les points communs entre votre expérience et le poste. 
    • Vous démontrez votre intérêt pour l’entreprise. 
    • Vous mettez en avant vos motivations et votre disponibilité. »

« La lettre de motivation \[...] met en avant les compétences et qualités du candidat nécessaires à la réussite de son projet professionnel. La lettre est un outil essentiel pour structurer son discours lors d'un entretien d'embauche. 

Elle est concise, personnalisée, rapide à lire, et jointe à un curriculum vitae dans l'intention d’« accrocher » le recruteur et de favoriser l'obtention d'un entretien d’embauche. Depuis l'usage généralisé des messageries électroniques dans les entreprises, la lettre de motivation est peu à peu remplacée par des messages électroniques de candidature \[= aussi appelés emails de motivation]. »

### 2. Himp

Présentation

Himp est un outil proposé par l’Emploi Store (Pôle Emploi) pour aider à la création de lettres de motivation. On peut également faire sur le site des emails de motivation ainsi que des emails de relance. Un tableau de bord recense toutes les lettres et tous les emails rédigés par l’utilisateur.

Il faut se créer un compte pour accéder aux fonctions du site. On peut se connecter au moyen d’une adresse email personnelle (adresse + création d’un mot de passe, il faut cliquer sur un lien qui renvoie au site dans un email de confirmation)  ou à partir d’un compte Pôle Emploi, Facebook, LinkedIn.

Pensé sous la forme d’un parcours où l’on remplit progressivement des informations sur l’offre qui correspond à la lettre de motivation (ça peut aussi être une lettre pour une candidature spontanée), sur soi-même (coordonnées, « point fort »…), c’est un site pratique et plutôt simple au niveau de la prise en main. 

Au fur et à mesure que les différentes rubriques sont remplies (type d’offre, point fort, destinataire et entreprise etc.), une lettre de motivation rédigée de A à Z  « automatiquement » apparaît. Il est alors possible de faire des modifications : modifier des paragraphes, changer de phrases toutes faites, selon les différentes propositions du site. Des modifications manuelles, dans le corps des paragraphes, peuvent également être faites.

Enfin, il est possible de la télécharger au format Word ou PDF.

Les + :

Site ergonomique ; prise en main de la mise en page, mise en forme de la LM (polices, tailles des textes etc.).

Possibilité de modifier manuellement le corps du texte de la LM.

LM plutôt complètes, style épuré.

Une fonction « feedback » existe avec système de commentaires. On peut envoyer la lettre de motivation à la personne que l’on souhaite : il faut renseigner nom et prénom et l’adresse email de la personne.

Les - :

Si l’on souhaite renseigner des compétences, des qualités personnalisées ou des expériences professionnelles, il faut le faire manuellement. 

Dans le parcours à suivre afin de générer la LM, comme élément vraiment personnel, il y a seulement le « point fort » (un seul!) qu’il faut renseigner.

![](/upload/lm1.png "Himp Etape 1")

![](/upload/lm2.png "Himp Etape 2")

![](/upload/lm3.png "Himp Etape 3")

![](/upload/lm4.png "Himp Etape 4")

![](/upload/lm5.png "Himp Etape 5")

### 3.Motivator

Motivator est un site gratuit permettant de générer des lettres de motivation de façon « automatique ». A partir d’informations sur le poste auquel on candidate, l’employeur, sur ses expériences professionnelles et ses qualités, des informations que l’on remplit au fur et à mesure, le site va proposer une lettre de motivation clé en main. 

Il est ensuite possible de faire  un copier/coller dans un mail pour inscrire directement dans le corps la LM ou de faire un copier/coller du texte sur une page d’un logiciel de traitement de texte, si l’on souhaite travailler la mise en forme (ajustements des paragraphes, polices, couleurs etc.) (et pouvoir l’enregistrer/convertir en PDF).

Si l’on est pas totalement satisfait des mots employés, de certains tournures de phrases, il est possible de réactualiser la lettre de motivation avec d’autres mots, d’autres tournures, un contenu différent (cliquer sur « Rédiger ma lettre de motivation » ). Les informations sur le poste, les expériences professionnelles passées, etc., resteront inchangées.

Motivator ne « corrige pas les fautes d’orthographe, de grammaire et de syntaxe. Il faut donc être vigilant lorsque l’on voit un mot souligné en rouge. Cela signifie qu’il y a une faute à corriger. A noter : faire un clic droit sur le mot en question permet d’avoir une proposition de mots correctement écrits qui peuvent correspondre. ».

Les + :

Rapide et efficace pour faire de la création de lettres de motivation concises.

Pas besoin de se créer un compte, on écrit directement la LM sur le site.

Les - :

On peut seulement renseigner deux expériences professionnelles précédentes (on ne peut pas faire de développements autour de ces expériences, sur les activités que l’on a réalisées, les missions).

On peut seulement renseigner trois qualités personnelles.

![](/upload/motivator1.png "Creation de LM sur Motivator Etape 1")

![](/upload/motivator2.png "Creation de LM sur Motivator Etape 3")

![](/upload/motivator3.png "Creation de LM sur Motivator Etape 4")

![](/upload/motivator4.png "Creation de LM sur Motivator Etape 5")

![](/upload/motivator5.png "Creation de LM sur Motivator Etape 6")

![](/upload/motivator6.png "Creation de LM sur Motivator Etape 7")

![](/upload/motivator7.png "Creation de LM sur Motivator Etape 8")

Exercices

CV

* Réaliser un CV via DesignR ou My-Edwin
* Envoyer sur sa boîte mail le CV pour le rendre accessible ou transférer sur son Smartphone le CV.

LM

* Réaliser une LM via Himp ou Motivator

::: tip Ressources

* Tuto Pôle Emploi / Emploi Store sur le CV  (animations interactives, 20 minutes en moyenne ; quelques rappels sur le CV, les erreurs à éviter etc.) :
  [https://www.emploi-store.fr/static/esu/services/CV/index.html](*https://www.emploi-store.fr/static/esu/services/CV/index.html)
* Mooc CV et LM sur Emploi Store (4 semaines, 2/3h de travail par semaine):  [https://www.emploi-store.fr/portail/services/reussirCvEtLettreDeMotivation](*https://www.emploi-store.fr/portail/services/reussirCvEtLettreDeMotivation)
* Créer des CV avec My-edwin : [https://my-edwin.com/signup/pro](*https://my-edwin.com/signup/pro)
* A propos des différents types de CV : [https://cvdesignr.com/blog/differents-types-de-cv/](*https://cvdesignr.com/blog/differents-types-de-cv/)
* Capitalisation à partir productions présentes dans le Cloud.
* LM, astuces Pôle Emploi : [https://www.pole-emploi.fr/candidat/10-astuces-pour-ecrire-votre-lettre-de-motivation-@/article.jspz?id=465513](*https://www.pole-emploi.fr/candidat/10-astuces-pour-ecrire-votre-lettre-de-motivation-@/article.jspz?id=465513)
* La LM (Pôle Emploi)
  [https://www.pole-emploi.fr/region/auvergne-rhone-alpes/candidat/rediger-votre-lettre-de-motivation-@/region/auvergne-rhone-alpes/article.jspz?id=266196](*https://www.pole-emploi.fr/region/auvergne-rhone-alpes/candidat/rediger-votre-lettre-de-motivation-@/region/auvergne-rhone-alpes/article.jspz?id=266196)
* La LM (Wikipédia) [https://fr.wikipedia.org/wiki/Lettre_de_motivation](*https://fr.wikipedia.org/wiki/Lettre_de_motivation)

:::