---
title: 👍 Introduction aux Réseaux sociaux
---

::: tip Modules APTIC

* *5 : Découverte des Réseaux Sociaux*

**Résumé :**
Les réseaux sociaux sont ancrés dans notre quotidien. Avoir une vision globale de leur fonctionnement et savoir les différencier est essentiel pour avoir une utilisation éclairée de ces derniers. 

**Nombre d’heures :** 3h

**Publics :** Séniors / retraités

**Prérequis:** 
Maîtrise minimum du Français : lu , écrit, parlé. 
Avoir un smartphone permettant de télécharger une application

**Objectifs de l’atelier**

* Expliquer au tout venant ce qu’est un réseaux social
* Démontre l’intérêt d’un Réseau Social dans un cadre concret
* Sensibiliser sur les dérives de l’usage excessif des Réseau Social

  **Sommaire**: 

[[toc]]

:::

## 1. Définition d’un réseau social

Origine: media social

> Ce sont des applications web qui permettent la création et la publication de contenus générés par l'utilisateur et le développement de réseaux sociaux en ligne en connectant les profils des utilisateurs.

L’expression « réseau social » dans l'usage habituel renvoie généralement à celle de « médias sociaux », qui recouvre les différentes activités qui intègrent technologie, interaction sociale entre individus ou groupes d'individus, et la création de contenu

Les applications relient entre elles des identités virtuelles.

Autre définition

> L’ensemble des site internet ou applications permettant aux utilisateurs de se constituer un réseau d’amis ou de connaissances et fournissant des outils d’interaction, de présentation ou de communication.

## 2.Cartographie des réseaux sociaux

### 2.1. Vision d’ensemble des réseaux sociaux existant

Première infographie représentent les 20 réseaux sociaux les plus utilisés dans les monde (Arrêt des décomptes En Avril 2020)

<a href="https://fr.statista.com/infographie/11915/comparaison-utilisateurs-actifs-mensuels-reseaux-sociaux-services-messagerie-facebook-tencent/" title="Infographie: Facebook domine en maître sur les réseaux sociaux | Statista"><img src="https://cdn.statcdn.com/Infographic/images/normal/11915.jpeg" alt="Infographie: Facebook domine en maître sur les réseaux sociaux | Statista" width="100%" height="auto" style="width: 100%; height: auto !important; max-width:960px;-ms-interpolation-mode: bicubic;"/></a> Vous trouverez plus d'infographie sur <a
 href="https://fr.statista.com/graphique-du-jour/">Statista</a> 

![Infographie représentent les réseaux utilisés en France en 2020](/upload/chiffres-reseaux-sociaux-france-2020.jpg "Infographie représentent les réseaux utilisés en France en 2020")

### 2.2 Les usages des réseaux sociaux

![Classement des réseaux sociaux et messageries instantanées ayant le plus fort taux de pénétration en France en 2018 ](/upload/classement-des-réseaux-sociaux-et-messageries-instantanées-ayant-le-plus-fort-taux-de-pénétration-en-france-en-2018-.png "Classement des réseaux sociaux et messageries instantanées ayant le plus fort taux de pénétration en France en 2018 ")

![Les usages des Réseaux Sociaux (sources sondage Harris Interractive 2019)](/upload/usages-des-rs.png "Les usages des Réseaux Sociaux (sources sondage Harris Interractive 2019)")

L’usage des réseaux sociaux est très variée.

#### 2.2.1 Messagerie

Certains réseaux sociaux ont un usage principalement orienté autours de la discussion entre utilisateur et partage de messages.

Ils se présentent sous la forme systèmes de messagerie instantanée (Facebook Messenger, Skype, Whatsapp…)

#### 2.2.2 Usages commerciaux

Certains réseaux sociaux vont être utilisés dans un cadre commercial bien définit afin de gagner une clientèle, mettre en avant un produit ou se faire connaître d’un potentiel investisseur.

Les plus connus dans le domaine sont pinterest, myspace

#### 2. 2. 3 Divertissements

C’est la part la plus connue des réseaux sociaux.

On y retrouve des contenus très variés (musique, video, critique...)

Les sites ou applications concernés sont nombreuses (Tik-Tok, Youtube, Twitch…)

#### 2.2.4 Blogging

Sous partie de la catégorie divertissement, ces réseaux sociaux sont orientés autours de la création d’un personae public et du partage de contenu auprès d’un public cible ou d’une communauté

Anciennement on connaissait les blog (type skyblog ou overblog) aujourd’hui ce sont les sites comme weebly, joomla ou wordpress blog

#### 2.2.5  Usages professionnels

Usage de plus en plus répandu, utiliser les réseaux sociaux dans le cadre de sa vie professionnel est à présent favorisé avec des sites comme Linkedin ou Viadeo, qui sont orientés sur la mise en valeur des profils professionnels et des échanges autours de thématiques liées à l’emploi et aux compétences.

### 2.3  Les profils des utilisateurs des RS

* Les Young Addict Fans (YAF) : une tranche jeune de la population très connectée au réseaux sociaux. Ils représentent 20% des interrogés.
* Les communic’Actives : des femmes actives dans les médias sociaux, avec relativement peu d’engagement vis-à-vis des marques.
* Les Brand Opportunists : qui génèrent énormément d’engagement auprès des marques. Ils sont 28%.
* Les Social Ghosts : qui survolent les réseaux sociaux mais interagissent peu.
* Les Cold-Feet : qui utilisent très rarement les réseaux sociaux.

![Les personae utilisateurs des Réseaux Sociaux](/upload/profils-d-utilisateurs.png "Les personae utilisateurs des Réseaux Sociaux")

## 3. Risques

### 3.1  Dépendance

Les usagers des réseaux sociaux en particulier les profils les plus jeux sont particulièrement vulnérables à devenir **dépendants**: dans une phase de la vie où les contacts sociaux avec les pairs jouent un rôle important pour l’estime de soi et l’identification, les likes et demandes d’amis conduisent à passer de plus en plus de temps devant son écran.

Tout comme lors de la dépendance aux jeux, le corps libère des **endorphines** et l’exaltation ne peut être ressentie que pendant une fraction de seconde, lorsque l’alarme du message clignote et annonce des messages possibles de la part d’« amis » ou promet une évaluation positive de son propre message.

Cependant, dès que le smartphone est hors de vue pendant un certain temps, de nombreuses personnes commencent à se sentir mal à l’aise et craignent de passer à côté de choses importantes.

Exemple : playlist sur la dopamine de Arte

<iframe width="560" height="315" src="https://www.youtube.com/embed/IahJWpRGbWE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Les caractéristiques d’une addiction sont :

* Une **envie irrépressible** du comportement addictif ;
* Un **abandon d’autres activités** au profit du comportement ;
* Des **conséquences individuelles, familiales, sociales, professionnelles** ;
* Une **sensation de manque** ou de malaise en cas d’interruption du comportement.

Les causes d’une cyber-addiction

* Une quête identitaire (à travers les avatars des jeux vidéos) ;
* Un refuge face aux problèmes du quotidien (solitude, incompréhension et non reconnaissance) ;
* Une incapacité à gérer l’ennui ;
* Une forme de dépendance à l’autre ou un problème de confiance et d’estime de soi.

### 3.2  Cyber-harcelement ou atteinte à la réputation

La cyber-intimidation (ou cyber-harcèlement) se définit par des actes répétés d’agression psychologique commis par un individu, ou un groupe, sur une victime par l’intermédiaire d’e-mails, de sites Web, des réseaux sociaux, forums, blogs, messageries instantanées ou sms. Ce harcèlement se manifeste par des menaces, des insultes, l’envoi de messages choquants, de photos obscènes, des rumeurs, des tags sur des photos gênantes…\
Ces intimidations peuvent concerner un collègue de bureau, un camarade de classe, un ex-conjoint… Comme toute forme de harcèlement, la cyber-intimidation entraîne des conséquences à la fois physiques et psychologiques sur la victime : stress, isolement, comportement violent, dépression, tendances suicidaires, voire un passage à l’acte.

Les réseaux sociaux offrent de nombreuses possibilités de laisser une impression sur les nouveaux contacts, aussi bien positive que négative.

Plus de **75 % des responsables des ressources humaines** utilisent Facebook et les autres applications pour se faire une première impression des candidats. Ceux qui s’affichent publiquement avec des photos ou des messages évoquant ou montrant des drogues illégales ou une consommation excessive d’alcool réduisent leurs chances.

De même, les messages de haine, qui sont liés à votre propre nom, jettent une mauvaise impression sur vous. Cependant, tous les faux pas des médias sociaux ne sont pas toujours de votre faute. Pour les maîtres-chanteurs ou les ennemis personnels, il est facile d’afficher des messages nuisibles à la réputation sur le Net.

Les dangers des médias sociaux vont de la **diffamation** à ce qu’on appelle le **porno de la vengeance**. Bien que les plateformes établissent des règles de conduite et emploient généralement des modérateurs qui suppriment ces contenus, elles ne réagissent pas toujours rapidement. La teneur en photos ou messages houleux peut donc initialement se répandre de façon incontrôlée. Dans de tels cas, les victimes ne peuvent être aidées que par une documentation aussi précise que possible sur les personnes qui ont eu accès aux données pertinentes et en s’adressant à la police.

### 3.3 Relais d’informations fausses ou « désinformation »

Fake news désigne une fausse information, bénéficiant le plus souvent d'une large diffusion dans les médias, notamment sur Internet et les réseaux sociaux.

Ces fausses informations peuvent être propagées dans des buts différents. Certaines ont pour objectif de tromper le lecteur ou d'influencer son opinion sur un sujet particulier. D'autres sont fabriquées de toute pièce avec un titre accrocheur pour densifier le trafic et augmenter le nombre de visiteurs sur un site.

### 3.4  Enfermement dans des "bulles de filtre"

Vous l’avez sûrement remarqué, les réseaux sociaux ont tendance à vous présenter des articles d’actualité qui reflètent vos opinions.

Notre société a transféré une grande partie de la vie quotidienne sur Internet. Pour beaucoup, la communication et la recherche d'informations se fait presque exclusivement en ligne. Le fil d'actualités de Facebook fonctionne comme un magazine d'actualités, Google comme une encyclopédie et des messagers tels que WhatsApp ou Skype sont utilisés pour échanger des informations avec des amis, des collègues et des membres de la famille. Presque tout ce que nous voulons savoir est désormais accessible en ligne. Les fournisseurs d'accès à Internet le savent aussi : Google, Facebook, Netflix ou Instagram comprennent l'importance de leurs offres pour la société. Ils affinent donc constamment leurs algorithmes en termes de convivialité : ils ne nous montrent que les informations **supposées pertinentes** pour nous.

Il n'y a là rien de bien nouveau : les principaux fournisseurs de services sur Internet recueillent des données sur le comportement des utilisateurs sur leurs plateformes et promettent d'adapter encore mieux l'expérience utilisateur aux besoins des clients, souvent sans qu’eux-mêmes n’en manifestent le désir. La **collecte de données** a déjà été critiquée par de nombreux experts, mais principalement sous l'aspect, par ailleurs essentiel, de la protection des données. Le terme audacieux *data octopus* décrit comment Google, Facebook et consorts, collectent et analysent les données personnelles de leurs utilisateurs : combien de temps une personne passe-t-elle en ligne ? Où habite-t-elle ? Quels sont ses passe-temps ?

Bien entendu, toutes ces informations sont utilisées à des fins très intéressées par ces entreprises : Google et Facebook, par exemple, réalisent une grande partie de leur chiffre d'affaires grâce à la publicité personnalisée. Mais les données devraient également aider à **mieux adapter les offres à chacun des utilisateurs** : ceci implique de personnaliser non seulement la publicité, mais également les informations proposées.

Par conséquent, les services ne nous montrent que les actualités, les informations et les opinions qui correspondent à notre profil d'utilisateur. Cela peut sembler positif au premier abord : les fils d’actualité ne sont plus remplis d'articles qui ne nous intéressent pas, les messages populaires ne sont plus remplis de commentaires que l’on ne lira pas, et il n’est plus nécessaire de lutter dans des débats d'opinion qui n'aboutissent à aucun résultat. Toutefois, cela crée à long terme des problèmes qui ne deviennent évidents que lorsque l'on remet en question les **mécanismes de filtrage des médias sociaux.**

## 4. Bonnes pratiques

### 4.1 Astuce de protection de ses données personnelles

#### 4.1.1 Changer de navigateur Web

Vous pouvez utiliser un navigateur web qui collecte moins de données sur ses utilisateurs.

#### 4.1.2  Utiliser un gestionnaire de mot de passe

Ne laissez plus la gestion de vos identifiants et mots de passe à l’abandon sur votre navigateur web. Pourquoi ? Tout simplement parce que ces derniers sont très facilement accessibles, en clair, en quelques clics. Faites le test.

Sur Firefox, depuis le menu principal, entrez dans Outils, puis placez-vous sur la section Vie privée et Sécurité. Dans la partie consacrée aux Identifiants et mots de passe, un clic sur Identifiants enregistrés suffira à afficher en clair tous vos mots de passe de connexion.

Pour éviter cela, il convient d’utiliser un mot de passe principal qui servira de mot de passe de référence, pour activer la saisie automatique de vos identifiants et mots de passe lorsque vous cherchez à vous connecter sur un site.

Mais plutôt que de confier vos mots de passe à un navigateur web, mieux vaut opter pour un gestionnaire dédié à cette tâche. Relativement simples à mettre en place, ils sont capables d’importer une base d’identifiants et de mots de passe déjà existants (dans votre navigateur web par exemple), et vous permettent de retrouver tous vos mots de passe, sur tous vos appareils, grâce à la synchronisation.

Parmi les plus réputés, vous pouvez utiliser KeePass ou Bitwarden, deux gestionnaires de mots de passe open source. KeePass n’étant disponible que sur Windows, les autres utilisateurs préféreront se tourner vers Bitwarden qui dispose d’une application sur toutes les plateformes.

#### 4.1.3 Communiquer à l’aide de messageries chiffrées

Les moteurs de recherche et les cookies déposés sur votre ordinateur ne sont pas les seules menaces pour vos données personnelles. Les services de messagerie sont aussi de précieuses sources d’informations pour les entreprises qui les possèdent.

Et même si nombre d’entre elles assurent ne pas lire vos conversations, certaines comme Facebook Messenger ont été pris la main dans le sac à partager certaines données personnelles et parfois même à divulguer vos conversations à des entreprises tierces.

Pour éviter, ou tout du moins limiter les risques, d’être espionné dans vos échanges, mieux vaut bien choisir ses services de messagerie. Pour ce qui est des messageries instantanées, vous pouvez vous tourner vers Telegram et Signal, deux services sécurisés se chargeant de protéger vos conversations en les chiffrant. Ces deux applications fonctionnent aussi bien sur PC que sur smartphone et sont entièrement gratuites.

#### 4.1.4 Utiliser une connexion VPN

Pour protéger vos données privées, il est également possible de naviguer sur la toile en utilisant une connexion VPN. Pour accéder à Internet, votre ordinateur fait transiter toutes vos requêtes par un tunnel sécurisé jusqu’à un serveur distant, qui charge le contenu à votre place, avant de vous renvoyer automatiquement le résultat. Vous accédez ainsi à Internet par le biais d’un intermédiaire possédant une adresse IP différente de la vôtre, empêchant la possibilité de vous identifier. Toutes les données transitant entre le VPN et votre ordinateur ou votre smartphone sont sécurisées à l’aide d’un système de chiffrement. Cela sera particulièrement utile si vous devez, par exemple, vous connecter à un réseau Wi-Fi gratuit non sécurisé, ou peu sécurisé, où vos données pourraient être facilement interceptées par une âme mal intentionnée.

La plupart des fournisseurs de VPN proposent des serveurs répartis dans différents pays du monde. Vous pouvez alors accéder facilement à certains sites pouvant être bloqués dans votre région et évitez toute possibilité pour votre fournisseur d’accès de pouvoir accéder à votre historique de navigation. Pour une protection optimale, n’utilisez pas de VPN gratuit. A part quelques exceptions, une grande majorité de VPN gratuits ne se soucient guère de la protection de vos données privées.

Si vous souhaitez tout de même pouvoir tester la navigation à travers un VPN sans bourse délier, optez pour ProtonVPN. Ce service de VPN, rattaché à ProtonMail, propose de tester ses services par le biais d’un accès gratuit mais très limité : 3 pays de connexion, un seul appareil et une vitesse de connexion lente. Pour des performances optimales, il faudra obligatoirement passer à la caisse. ProtonVPN assure ne conserver aucun log des activités de ses utilisateurs et ne partage aucune donnée à des tierces parties.

En alternative, Windscribe est un service VPN ayant également été mis en avant pour le respect des données de ses utilisateurs en assurant ne conserver aucun log. La plateforme propose à ceux qui souhaiteraient tester ses services, une formule d’essai gratuite, limitée à 10 Go mensuel avec serveurs répartis sur dix pays.

### 4.2 Sortir de la bulle de filtres

Si l’on souhaite se libérer de sa bulle de filtres, il existe plusieurs options : la première étape consiste à remettre en question son propre comportement de navigation. Si l’on souhaite délibérément faire face à des opinions contraires aux siennes, c’est possible malgré la bulle Facebook et la personnalisation (supposée) de Google. De cette façon, les algorithmes des médias sociaux peuvent être consciemment influencés et formés : par exemple, suivre les pages Facebook de plusieurs partis (avec une mention *J’aime*) permet de recevoir un éventail plus large d'informations du spectre politique. De cette façon, chacun peut **créer sa propre diversité**.

De plus, le réseau offre des outils qui libèrent au moins la recherche de résultats personnalisés. Ainsi, chaque internaute est libre d'effectuer sa recherche dans **d'autres moteurs de recherche que Google**. Le moteur de recherche français [Qwant](https://www.ionos.fr/digitalguide/web-marketing/search-engine-marketing/vue-densemble-des-alternatives-a-google/), par exemple, annonce qu'aucune donnée personnelle concernant les utilisateurs n'est collectée ou évaluée. Ainsi, aucune recherche personnalisée n'a lieu et aucune bulle de filtre ne peut être créée.

### 4.3 Gérer son identité numérique et sa e-réputation en ligne

#### 4.2.1 L’identité numérique

L’identité numérique est l’ensemble des données et renseignements qui se rapportent spécifiquement à un individu sur Internet.

Elle est composée des :

\- Identité numérique et e-réputation

\- Notes éléments d’authentification : nom, prénoms, pseudonymes, adresse IP, etc.

\- Éléments de reconnaissance : photo, avatar, image, logo, etc.

\- Traces numériques : posts et/ou commentaires sur les réseaux sociaux, les blogs, etc.

Pour résumer, l’identité numérique est composée de tout ce que nous publions, ce que nous commentons et montrons de nous volontairement à chacune de nos apparitions sur Internet. Nous alimentons ainsi, petit à petit, toutes les facettes de notre identité numérique.

De même que nous rechercherons des informations sur un potentiel employeur, les recruteurs prennent l’habitude de googleliser et/ ou facebooker les noms (au moins), les adresses électroniques, etc. de leurs candidats potentiels. Ainsi, ils disposent d’une masse d’informations à la fois sur notre vie privée et notre vie publique.

*Comment gérer son identité numérique ?*

Tout ce qui se trouve sur Internet ne disparaît (presque) jamais.

Aussi est-il est important d’avoir le contrôle sur chaque élément que nous y mettons. La gestion de notre identité numérique se révèle être d’une importance capitale pour éviter de nous retrouver confronté à des situations aux conséquences indésirables.

Voici quelques conseils pour bien gérer notre identité numérique :

\- Définir des objectifs de la ou des cible(s) de nos publications sur Internet

\- Définir et modifier les paramètres de visibilités de nos contenus

\- Faire de la veille sur son nom

\- Éviter de publier des éléments qui pourraient être compromettant

\- Donner le minimum de d’information personnelles (nom, prénom, adresse ou lieu de résidence ...)

#### 4.2.2  La e-réputation

À la différence de l’identité numérique représentée par ce qui vient de nous-mêmes, l’e-réputation, c’est ce que les autres disent et publient sur nous.

L’e-réputation est « subie ». Nous ne pouvons donc pas la maîtriser directement.

Il existe cependant des astuces pour surveiller ce qui se dit sur nous sur Internet et, au besoin, réagir en apportant une réponse ou en demandant le retrait des éléments indésirables.

\- D’abord, il faut mettre en place une veille informatique pour surveiller tout ce qui se dit sur nous, à travers les outils comme Google Alerts et Talk Walker Alerts

\- Essayer d’enterrer les commentaires négatifs voire de contacter une autorité légale si les contenus portent atteinte à notre image ou à notre vie privée. Google a mis en ligne un formulaire permettant de faire une demande de suppression de données. (lien du formulaire de suppression des données de google :<https://www.google.com/webmasters/tools/legal-removal-request?complaint_type=rtbf&visit_id=637338584311930368-4007918605&rd=1>)

\- Ajuster nos paramètres de confidentialité afin que seuls nos amis voient par exemple une photo de nous en soirée et non les recruteurs. Possible sur Facebook.

\- Changer régulièrement nos mots de passe, afin d’éviter les risques de piratages de nos comptes.

Être régulièrement connecté et ne pas oublier de mettre nos liens de blog, Facebook, twitter, sur chaque compte.

\- Se valoriser tout en restant en accord avec nous-même et en ne mentant pas.

\- Séparer notre vie personnelle de notre vie privée ; par exemple, avoir un profil LinkedIn pour les enjeux professionnels et Facebook pour notre vie privée.

::: details
Quelques liens utiles pour gérer son e-reputation

Pour manager sa réputation en ligne 

<https://brandyourself.com/>

<http://youseeme.fr/>

Formulaire de demande de suppression d'informations personnelles 

<https://www.google.com/webmasters/tools/legal-removal-request?complaint_type=rtbf&visit_id=637338587605048342-227588402&rd=1>
::: 

::: tip Sources

**Sites Sources internet**

*<https://www.webmarketing-conseil.fr/liste-reseaux-sociaux/>*

*<https://fr.wikipedia.org/wiki/R%C3%A9seau_social#R%C3%A9seaux_sociaux_sur_Internet>*

*<https://fr.wikipedia.org/wiki/M%C3%A9dias_sociaux>*

*<https://www.e-marketing.fr/Definitions-Glossaire/Reseausocial-242949.htm>*

*<https://www.definitions-marketing.com/definition/reseaux-sociaux/>*

*<https://comarketing-news.fr/usages-reseaux-sociaux-france/>*

*<https://fr.statista.com/etude/31001/l-utilisation-des-reseaux-sociaux-en-france-dossier-statista/>*

*<https://www.blogdumoderateur.com/usages-des-reseaux-sociaux-2019/>*

*<https://www.oberlo.fr/blog/chiffres-reseaux-sociaux>*

*<https://www.talkwalker.com/fr/blog/statistiques-reseaux-sociaux-france>*

*<https://harris-interactive.fr/press/social-life-2019-les-social-personae-leurs-pratiques-portrait-et-relation-aux-marques/>*

*<https://www.mgc-prevention.fr/internet-les-reseaux-sociaux-quels-risques-sante/>*

*<https://www.ionos.fr/digitalguide/web-marketing/les-media-sociaux/dangers-reseaux-sociaux/>*

*<https://www.lesechos.fr/2017/11/pourquoi-addiction-et-fake-news-sont-ladn-des-reseaux-sociaux-188398>*

*<https://www.macsf.fr/Nos-publications/Nos-guides/guide-reseaux-sociaux>*

**Sources manuscrites**

*Guide d’utilisation et de bonnes pratiques des réseaux sociaux – Institut Paros Afrique de l’Ouest*

*Guide du bon usage des réseaux sociaux – Ministère français de la défense*
:::