---
title: 🔒 Protection des données
---
# Protection des données

::: tip Modules APTIC  

* *97 : Nettoyer son identité numérique*
* *98 : Effacer ses traces sur le Web, protéger ses données personnelles*
* **Résumé :** (3 lignes / Minuscule / 12)

Face à l’explosion des données produites (objets connectés, cookies ...) mais aussi la démocratisation des smartphones de nombreux acteurs qu’ils soient plus ou moins bien intentionnés traitent aujourd’hui ces dernières ou cherchent à le faire. Ce cours sert donc à se protéger face à cela et à vous aider à faire des choix conscients quant à l’utilisation qui est faite de vos données.

* **Nombre d’heures :** 3 heures
* **Publics :** Grand débutant / Débutant / intermédiaire
* **Prérequis:** Maîtrise minimum du Français : lu , écrit, parlé . Posséder un smartphone (pour les ateliers). Avoir suivi les modules Navigateur/Smartphone/
* **Objectifs de l’atelier :**

  * Connaître les différents niveaux de protection des données
  * Savoir gérer ses autorisations smartphone
  * Savoir gérer ses réseaux sociaux
  * Savoir configurer son navigateur
  * Savoir créer un mot de passe résistant

  :::
* **Sommaire**

[[toc]]

## **1. Les mots de passe**

Objectifs intermédiaires : comprendre le bruteforce et les méthodes de hacking.

Définition : 

> Un mot de passe est un mot ou une série de caractères utilisés comme moyen d'authentification pour prouver son identité lorsque l'on désire accéder à un lieu protégé, à une ressource (notamment informatique) ou à un service dont l'accès est limité et protégé.

Le mot de passe doit être tenu secret pour éviter qu'un tiers non autorisé puisse accéder à la ressource ou au service. C'est une méthode parmi d'autres pour vérifier qu'une personne correspond bien à l'identité déclarée. Il s'agit d'une preuve que l'on possède et que l'on communique au service chargé d'autoriser l'accès.

En informatique : Un mot de passe sert à ralentir une personne souhaitant s’introduire dans un système informatique.

Tous les mots de passe sont crackables dans l’absolu! Pour autant le but d’un hacker est de faire un maximum de victimes en un minimum de temps aussi si votre mot de passe est trop difficile à cracker il ira chercher une autre cible.

Pour rappel il convient de ne pas utiliser le même mot de passe sur tous ces sites afin de limiter l’impact d’un éventuel vol.

De plus il convient de ne pas y incorporer des éléments pouvant être devinés par un cracker comme une date de naissance, son prénom …

Enfin, un bon mot de passe est un mot de passe mémorisable ! Si vous devez changer régulièrement de mot de passe c’est qu’il y a un problème.

Les règles de création de mot de passe résistant sont :

* des chiffres
* des lettres
* des majuscules/minuscules
* mais surtout des caractères spéciaux(!, @, *, #) car ces derniers ont un encodage (une façon d’être lue par les machines) différent selon les ordianteurs, systèmes, navigateurs …

Votre mot de passe est-il sécurisé ?

Pour tester la résistance des mots de passe via une technique appellée bruteforce(technique constitant à tester toutes les combinaisons possibles) il y a le site [howesureismypassword.net](howesureismypassword.net)

![](/upload/howsecureismypassword.png "Howsecureismypassword")

Si le résultat apparaît:

* en rouge : à changer d’urgence
* en orange: correct
* en vert: quasi inviolable via cette méthode

## **2. Les autorisations**

Objectifs intermédiaires : comprendre la notion de finalité des données

La navigation se faisant aujourd’hui de plus en plus sur smartphone il convient donc tout naturellement d’appliquer les principes vus auparavant sur le téléphone. A ceux-ci se rajoute une particulière : les autorisations.

En effet lorsque vous installez une application un certain nombre d’autorisations vont vous être demandées. Certaines sont justifiées (ex : la localisation pour le GPS d’autres non par exemple vos contacts pour une application de météo).

On parle même d’offre Open Bar pour les applications les demandant toutes par défaut, chose qui est aujourd’hui très mal vu.

Pour les configurer :

* sur Android : rouages, taper « autorisation » dans la barre de paramètre de recherche en haut puis autorisations des applis

![](/upload/autorisationsandroid.png "Autorisations Android")

* sur Iphone : dans les paramètres il faudra cliquer sur chaque application manuellement puis dans le menu de l’application désactiver les autorisations non nécessaires.

![](/upload/autorisation-iphone.png "Autorisation Iphone")

## **3. Les antivirus**

Objectifs intermédiaires : connaître différents antivirus

Définition : 

> Logiciels qui scannent l’ordinateur en vue de la détection de programmes malveillants à partir d’une base de données de signatures de virus.

Pour rappel un virus est un logiciel qui s’autoréplique sans accord de l’utilisateur et provoque des dysfonctionnements ou des ralentissements de l’ordinateur.

C’est un logiciel indispensables dès lors qu’on connecte son ordinateur à Internet!

Il en existe des gratuits : Kapersky, Microsoft Security Essential, Avast

Et des payants : Avira, Bitdefender, Norton.

Les exercices : <https://learningapps.org/watch?v=p7jt460fc19>

## **4. Les navigateurs**

Objectifs intermédiaires : comprendre ce que sont les cookies, savoir effacer ses traces, installer une extension

Définition : 

> Logiciel permettant d’aller sur Internet retenant énormément d’informations lors de notre connexion (mots de passe, sites visités …).

Or ces informations peuvent être utilisées pour de la revente de données (cf. cours Big Data) ou par un hacker.

Tous en collectent par défaut mais certains moins que d’autres et peuvent être paramétrés.

Parmi les navigateurs il faut citer : Opera(appartenant à Apple), Chrome(appartenant à Google), Edge (anciennement Internet Explorer appartenant à Windows) ou encore Firefox. C’est ce dernier qui est le moins gourmand en matière de collecte de données.

De fait il convient de savoir comment à minima effacer ses traces.

Dans Firefox : les 3 barres en haut à gauche, options, Vie privée et sécurité, blocage strict + ne jamais conserver l’historique+ effacer l’historique

![](/upload/effacerhistoriquefirefox.png "Effacer l'historique Firefox")

Dans Chrome : les 3 barres en haut à gauche, options, vie privée et sécurité, effacer l’historique à faire **à chaque fois**.

![](/upload/historiquechrome.png "Effacer l'historique Chrome")

A noter qu’il existe des extensions pour bloquer la publicité (ex:Adblock), limiter les distractions visuelles(ex:Minimal) ou encore limiter la personnalisation des résultats via Ghostery

![](/upload/extensions.png "Extensions sur Firefox")

On peut aussi remarquer la mention de cookies dans les paramètres. Les cookies ce sont des traces de navigation lues par un site B si l’on vient d’un site A. Certains sont nécessaires au fonctionnement des sites (mots de passe, identifiants, permissions) mais la plupart servent juste à tracker les utilisateurs aussi il faut apprendre à les refuser.

![](/upload/cookies.png "Paramétrage des cookies")

## **5. Les moteurs de recherche**

Sites Internet permettant de trouver des résultats gratuitement en l’échange de nos données ou de notre travail (digital labor).

La revente des données est ici immédiate mais on peut limiter son stockage.

Parmi les plus connus on peut citer : Google, Bing, Yahoo mais aussi Qwant, DuckduckGo ou encore Ecosia.

Exercice pratique : le cas Google My Activity.

Note pour l’animateur : *Uniquement pour les titulaires d’un compte Google (téléphone sous Android ou adresse Gmail), à faire sur ordinateur de préférence et chacun son ordinateur au vue des données exposées.*

Sur la page <https://myactivity.google.com/> faites-les se connecter avec leurs identifiants. Ils devraient tomber sur la page suivante.

![](/upload/googlemyactivity.png "Google My activity")

Laissez-les dans un premier temps visualiser leurs données puis une fois l’excitation passée montrez-leur comment limiter la collecte (option : commandes relatives à l’activité, boutons bleus à décocher – à noter que ceux qui avaient la géolocalisation activée peuvent visualiser leurs données sous forme de cartes via carte des positions) et ensuite seulement comment supprimer celles stockées (option : supprimer l’activité, toute la période).

A noter que malheureusement cela ne vaut pas accord pour Google de ne plus collecter de données sur vous, juste de ne plus les stocker.

Toutefois il s’agit d’une avancée majeure dans la régulation des plateformes. A titre d’information Yahoo ou Apple ne proposent même pas ce genre d’outils.

## **6. La Messagerie**

Objectifs intermédiaires : connaître le phishing, les spams.

La messagerie est bien entendu une cible de choix des hackers notamment parce qu’elle fait appel à une faille majeur de sécurité : la crédulité des gens.

Tout d’abord il convient de différencier le spam qui consiste en la réception de courriers non désirés mais qui n’ont aucun autre impact que d’inonder nos boites mails du phishing qui elle est une technique d’arnaque consistant à se faire passer pour un courriel officiel et vous demandant vos identifiants ou autre dans le but de les voler ou de pouvoir atteindre un de vos contacts par ce biais.

Il peut arriver qu’un courriel sérieux arrive parfois en spam aussi pensez une fois par semaine à checker cette boite sans pour autant ouvrir les autres mails !

Pour savoir si votre boite mail a déjà été ou non piratée il existe le site : <https://haveibeenpwned.com/>

Rentrez-y votre adresse mail et constatez si elle a déjà été piratée ou non (en rouge si oui).

![](/upload/haveibeenpwned2.png "HaveIbeenPwned")

## **7. Les Réseaux Sociaux**

Objectifs intermédiaires : savoir paramétrer ses comptes, comprendre les conditions générales d’utilisation, la notion de consentement éclairé

Définition : 

> Sites ou applications permettant la création et la publication de contenus générés par l’utilisateur et le développement de réseaux en ligne en connectant les profils des utilisateurs.

Ces sites mettent par défaut nos informations en public, scannent nos contacts, revendent les données des utilisateurs.

Aussi à des fins de gestion de son e-réputation(réputation en ligne) comme de sa vie privée il convient de savoir les paramétrer pour éviter cela.

Par exemple : Sur Facebook Rouages, Paramètres, Confidentialité, Qui peut voir vos actualités/Comment les autres peuvent vous trouver (Public/Amis des Amis/*Amis uniquement*) mais aussi répondre Non à désirez-vous apparaître sur les moteurs de recherche.

![](/upload/parametresfacebook.png "Paramètres de confidentialité Facebook")

Autre exemple : Lire avec eux les [conditions d’utilisation d’Instagram expliquées à des enfants](https://www.businessinsider.fr/une-avocate-a-reecrit-les-conditions-dutilisations-dinstagram-comme-si-elles-etaient-expliquees-a-un-enfant-de-8-ans/) pour comprendre le caractère abusif de la collecte et de la revente de données.

::: details Anecdote : 

C’est dans une optique d’arbitrage quant à la collecte et la revente massive de données que l’Union Européenne a mis en place le RGPD, texte de loi voté en 2016 et appliqué en 2018 stipulant que tout site collectant des données doit :

* notifier l’internaute en cas de collecte des données
* lui permettre de dire quelles données il souhaite ou non donner
* protéger les données ainsi acquises

:::

Évaluation des acquis

Exercice :  <https://learningapps.org/watch?v=pvqsha8z219>