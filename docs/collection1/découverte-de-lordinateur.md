---
title: 👣 Premiers Pas sur l'Ordinateur
---
# Premiers Pas sur l'Ordinateur

::: tip Référentiel APTIC : 

* *None*
* **Résumé** 

Les ordinateurs sont des objets de plus en plus présents dans notre quotidien. Il est rare qu’on ne l’utilise pas de manière courante ou pour satisfaire des besoins spécifiques. Cette technologie, aussi différente soit-elle, nous donne accès à toutes sortes d’informations qui nous facilitent ou améliorent notre vie en général. Toutefois il n’est plus d’usage courant, aujourd’hui, d’essayer de comprendre l’intérieur de la machine. Cela s’avère toutefois nécessaire pour comprendre l’ensemble du processus de fonctionnement des ordinateurs. C’est ce que nous allons faire avec ce module.

* **Nombre d’heures du module** **:** 2h
* **Public :** Module « débutant »
* **Prérequis :** Maîtrise du français lu et parlé.
* **Objectifs de l’atelier :**

  * Découverte de l’ordinateur 
  * Citer les principaux éléments 
  * Identifier les fonctions des composants entre eux

:::

**Sommaire**

[[toc]]

## 1.Introduction

### 1.1 Qu’est-ce que c'est ?

> L’informatique est une « science » qui permet de traiter l’information de façon automatique. L’ordinateur est un appareil très puissant permettant de traiter les informations avec une très grande vitesse, un degré de précision élevée et permet de stocker toutes ces informations. Il est divisé en deux parties : la partie matérielle et la partie logicielle .
>
> → Larousse : « Science du traitement automatique et rationnel de l’information ».
>
> → Wikipédia : « L'informatique est le domaine d'activité scientifique, technique et industriel concernant le traitement automatique de l'information par des machines telles que : calculateur, système embarqué, ordinateur, console de jeux vidéo, robot, automate, etc. »

La partie matérielle représente la partie physique du système informatique.
Elle est divisée en deux parties : L’unité centrale et ce que l’on appelle les périphériques

### 1.2 D’abord quelques définitions

> Informatique : Traitement automatique de l’information.
>
> Hardware : Mot anglais désignant le matériel informatique (processeur, carte mère ...)
>
> Software : Mot anglais désignant les logiciels.
>
> PC : Mot anglais (Personal Computer) désignant l’ordinateur.
>
> Plantage : Le micro-ordinateur ne répond plus à aucune instruction. Dans ces cas-là, il faut redémarrer son ordinateur.

### 1.3 Quelques autres définitions

Le bureau de Windows

> Le bureau de Windows est le premier élément que l’on voit lorsque l’on allume son micro-ordinateur. La petite flèche blanche que vous pouvez voir sur l’image précédente correspond à la souris. Le bureau de Windows est complètement paramétrable.

Les icônes

> Un icône est un élément placé sur le bureau de Windows. Il est composé d’un dessin et d’un nom. Ces icônes permettent de lancer des programmes plus rapidement que par le menu Démarrer.

Les raccourcis

> Un raccourci est un petit fichier (de 1Ko à 2Ko) qui indique l'adresse d'un document ou d'un programme. Sous Windows, ils sont symbolisés par une petite flèche noire dans un carré blanc en bas à gauche de l'icône. (extension .lnk )

Le menu démarrer

> Le menu démarrer est un menu dans lequel on trouve tous les logiciels ou programmes non placés sur le bureau de Windows mais aussi, on trouve des petits programmes qui vont nous permettre de mieux configurer ou paramétrer notre micro-ordinateur.

### 1.4 Petite chronologie de l’informatique

1642 Blaise Pascal crée la Pascaline, première machine à calculer

1880-1930 Machines électromécaniques 

1937-45 Tous premiers ordinateurs 

1950-60 Premiers ordinateurs commerciaux, très gros et peu puissants 

1956 IBM commercialise le premier disque dur 

1967 IBM commercialise les disquettes et les lecteurs de disquettes 

1970 Intel invente le micro-processeur 1971 Xerox commercialise une imprimante Laser 

1975 Première calculatrice programmable de Texas Instruments 

1980 Vitesse des processeurs : 5 MHz 

1980 Premiers ordinateurs personnels, premiers logiciels de traitement de texte, premiers jeux-vidéos 

1981 Lancement en France du réseau Minitel 

1984 Sortie du premier Macintosh d'Apple 

1990 Vitesse des processeurs : 100 MHz 

1990 Création de Microsoft Windows 

1990 Ouverture d'Internet au grand public 

1990 Arrivée des CD-ROM 

1991 Lancement du projet Linux 

1996 Arrivée des DVD-ROM 

1997 L'ordinateur DeepBlue bat le champion d'échecs Garry Kasparov

1998 Arrivée des clés USB et du réseau Wifi

2000 Vitesse des processeurs : 2GHz 

2007 Invention du smartphone iPhone

2009 Commercialisation au grand public d'imprimantes 3D 

2010 Commercialisation de la tablette iPad 

2015 Vitesse des processeurs : 4GHz

### 1.5 Qu’est-ce qu’un ordinateur ?

> Un ordinateur est un ensemble de composants électroniques ayant des caractéristiques différentes, capables de faire fonctionner des programmes informatiques. Il est généralement composé d’une unité centrale, d’un écran, d’un clavier et d’une souris.

### 6.Une analogie avec le corps humain

Dans le but de mieux comprendre l’intérieur d’un ordinateur, il est possible de procéder à une analogie connue. En effet, un ordinateur est composé selon une structure analogique à celle du corps humain.

| Ordinateur                       | Rôle                     | Humain                |
| -------------------------------- | ------------------------ | --------------------- |
| Unité centrale                   | Entrée principale        | Corps humain          |
| Carte mère                       | Structure principale     | Moelle épinière       |
| Micro-processeur                 | Capacités de calcul      | Cerveau               |
| Disque dur                       | Stockage d'information   | Mémoire à long-terme  |
| Mémoire RAM                      | Stockage d’information   | Mémoire à court-terme |
| Alimentation électrique/Batterie | Alimentation énergétique | Système digestif      |

::: details Aparté pour le formateur: 
Penser à faire en même temps analogie et présentation technique
:::

## 2.La présentation des éléments

![](/upload/lescomposants.png "Les composants de l'ordinateur")

Tous les ordinateurs ont le même matériel de base. Le microprocesseur est leur «cerveau». Il gère les informations qui entrent et sortent de l’ordinateur. La mémoire stocke les programmes et les données pendant leur utilisation. Les programmes et les données sont conservés de façon permanente sur des dispositifs de stockage. La plupart des ordinateurs sont ainsi équipés d’un disque dur qui stocke les données sur un disque métallique, dans l’ordinateur. Voyons tout cela dans le détail.

Commençons !

### 2.1 Le boîtier

En partant de la boîte (avant et arrière) que nous avons à l’association ICI, expliquer le détail de tous les composants :

Focus BOÎTIER AVANT

![](/upload/boitieravant.png "Exemple de boitier avant")

• L'interrupteur d'alimentation "Power" permet de mettre le P.C. sous tension. En fin de travail, l'arrêt se faisant par logiciel sous un système d’exploitation classique (commandes Démarrer / Arrêter), il est préférable d'attendre le message "vous pouvez éteindre votre ordinateur" pour les boîtiers de type AT avant d'appuyer sur celui-ci. Mais si 1'ordinateur refuse de s'arrêter, pour les boîtiers de type ATX, il faut maintenir appuyer l'interrupteur d'alimentation plus de 10 secondes pour voir 1'ordinateur s'arrêter. 

• Le bouton de réinitialisation "Reset" ou démarrage à chaud, quand il existe, est très utile lorsque 1'ordinateur ne répond plus aux commandes. II permet de relancer 1'ordinateur sans l'éteindre complètement. 

::: danger ATTENTION : 
Dans ces deux cas, les données inscrites en mémoire qui n'auront pas été sauvegardées seront irrémédiablement perdues. 
:::

![](/upload/composition-de-la-tour.png "Composition de la tour")

Lien vers la cyberbase de la cité des sciences, on y trouve une très bonne vidéo interactive (sous flash) présentant l'intérieur d'un ordinateur. 

Focus BOÎTIER ARRIÈRE : L’alimentation électrique est reliée directement à une prise de courant (secteur) 

![](/upload/boitier-arriere.png "Composants du boitier arrière (connectique)")

• Un bouton 0 – 1 permet de couper l’arrivée de courant.

• Selon l’âge de votre ordinateur, plusieurs prises permettront de le relier à un écran : un branchement DVI (blanc rectangulaire) pour brancher les écrans modernes et un VGA (bleu rectangulaire) pour les anciens écrans. Et plus récemment l’HDMI qui a la même norme que pour nos téléviseurs !

• Les ports USB permettant de brancher divers périphériques (souris, clavier, clé USB…). Ce sont actuellement les ports les plus utilisés !

• Une prise spéciale permet de se connecter à Internet par câble : c’est une prise RJ45. Aujourd’hui la plupart des ordinateurs peuvent se connecter sans fil.

• Les prises son permettent de brancher des enceintes, caissons de basses, microphones…

• La grille d’aération est très importante car elle permet d’évacuer la chaleur générée par les composants de l’ordinateur. Veillez à ne pas obstruer cette sortie.

Bien entendu, ces branchements peuvent varier d’un ordinateur à l’autre selon son ancienneté. On retrouvera presque toujours les ports USB qui sont devenus incontournables.

### 2.2 L’écran

Un écran d'ordinateur est un périphérique de sortie vidéo d'ordinateur. Il affiche les images générées par la carte graphique de l'ordinateur.

![](/upload/ecran.jpg "Exemple d'écran")

Focus
• **La taille de l'écran** : les moniteurs ont des dimensions usuelles de 14, 15, 17 et 20 pouces, (1 pouce = 2,54 cm). Cette taille représente la longueur de la diagonale de l'écran. 
• **La résolution** est le nombre de points que peut afficher l'ordinateur à l'écran. Ce nombre est compris entre 640 X 480 points (640 points en longueur et 480 points en largeur) et 1600 X 1200 points. Lorsque l'on parle de la résolution d'un écran (dans une publicité, par exemple), on donne toujours la résolution maximale qui est prise en charge.

### 2.3 L'alimentation :

L'alimentation permet de fournir du courant électrique à tous les composants de l'unité centrale. Elle est aujourd'hui plus importante qu'hier : en effet avec la montée en fréquence des processeurs et cartes graphiques, la demande en courant s'accroît au fur et à mesure des années. Un processeur fonctionnant à 3 GHz consomme près de 70 A, et les nouveaux processeurs peuvent consommer plus de 120 Watt ! Une alimentation délivre 3 tensions principalement : 12V, 5V et 3,3V. Elle délivre aussi des tensions négatives : -12V, -5V, et -3,3V qui ne délivrent pas beaucoup de puissance.

![](/upload/alimentation.png "Exemple d'alimentation")

Il existe des alimentations dont la ventilation est thermorégulée : pour faire le moins de bruit possible, la vitesse du ventilateur varie en fonction de la température de l'alimentation. Plus elle chauffera, plus il tournera vite et inversement. La puissance idéale se situe aujourd'hui à 450W. Ce niveau de puissance est largement suffisant pour tout type d'usages. L'avantage de prendre une alimentation très puissante est qu'elle fera généralement moins de bruit qu'une petite alimentation utilisée à son maximum, la durée de vie en sera d'ailleurs améliorée.

### 2.4 Le refroidissement :

Aujourd'hui, un bon ventilateur sur son processeur ne suffit plus. Il faut évacuer la chaleur produite par le processeur et la carte graphique, de plus en plus puissants et devant dissiper de plus en plus de chaleur. Le ventilateur de l'alimentation ne suffit donc pas à faire circuler un courant d'air suffisamment frais pour assurer un fonctionnement optimal des composants du PC. De plus, si on dispose de 2 disques durs ou plus dans sa machine, il faut absolument penser à les ventiler. Un ventilateur est alors obligatoire si on ne veut pas tout simplement faire griller son processeur ou ses composants !

### 2.5 La carte mère

La carte mère (Mainboard ou Motherboard) est l'un des principaux composants du PC. Elle se présente sous la forme d'un circuit imprimé sur lequel sont présents divers composants. En fait, son rôle est de lier tous les composants du PC, de la mémoire aux cartes d'extensions.

![](/upload/cartemere.png "Carte Mere")

Une carte mère comporte toujours les éléments suivants : 

• Un chipset 

• Un BIOS

 • Une horloge interne gérée par une pile lorsque le PC est éteint

 • Un bus système (chemin reliant le processeur à la mémoire vive sur la carte mère) 

On différencie généralement les cartes-mères par leur : 
• facteur d'encombrement 

• Chipset 

• Support de processeur (appelé socket) 

• Fonctionnalités intégrées (fait partie du chipset).

### 2.6 Le processeur

Un processeur (aussi appelé microprocesseur ou CPU pour Central Processing Unit) est le cœur de l'ordinateur. Ce composant a été inventé par Intel (avec le modèle 4004) en 1971. Il est chargé de traiter les informations et d'exécuter les instructions. Il ne sait communiquer qu'avec le reste de l'ordinateur via le langage binaire. Le processeur est rythmé par une horloge (quartz) cadencée plus ou moins rapidement (on parle alors de fréquence). A chaque impulsion d'horloge (signal électrique passant du niveau bas au niveau haut en cas de front montant), le processeur lit l'instruction stockée généralement dans un registre d'instruction et exécute l'instruction.

![](/upload/processeur.png "Exemple de processeur")

### 2.7 La mémoire

La mémoire est un composant de base de l'ordinateur, sans lequel tout fonctionnement devient impossible. Son rôle est de stocker les données avant et pendant leur traitement par le processeur. Ces données sont d'apparence binaire et mémorisées sous forme d'impulsions électriques (une impulsion est égale à 1, aucune impulsion est égale à 0). Plusieurs types de mémoires sont utilisés, différentiables par leur technologie (DRAM, SRAM, ...), leur forme (SIMM, DIMM, ...) ou encore leur fonctionnement (RAM, ROM,). 

![](/upload/ram.jpg "Exemple de barrettes de RAM")

Les différents types de mémoire : On distingue deux grands types de mémoire : 

• La mémoire vive (ou RAM pour Random Access Memory) : cette mémoire perd ses données si elles ne sont pas rafraîchies régulièrement, le contenu de cette mémoire est dynamique. 
• La mémoire morte (ou ROM pour Read Only Memory) : cette mémoire ne perd pas ses données (sauf par des techniques de réécriture, comme le flashage pour les mémoires flash), même si elle n'est pas rafraîchie.

### 2.8 La carte graphique

La carte graphique est l'un des rares périphériques reconnus par le PC dès l'initialisation de la machine. Elle permet de convertir des données numériques brutes en données pouvant être affichées sur un périphérique destiné à cet usage (écran, vidéo projecteur, etc...). Son rôle ne se limite cependant pas à ça puisqu'elle décharge de plus en plus le processeur central des calculs complexes 3D et ce au moyen de diverses techniques.

### 2.9 La carte son

La carte son a un rôle simple dans l'ordinateur : produire du son, même si ses rôles ne s'arrêtent pas là. Elle permet de gérer tout un tas d'effets qui auparavant étaient traités par le processeur central, ce qui le décharge d'autant plus. Le choix d'une carte son conditionne l'usage que l'on doit en faire.

Le son intégré : Beaucoup de cartes mères intègrent maintenant des cartes son intégrées. Les meilleures cartes son intégrées ne sont pas au niveau de celles sur port PCI. Cependant, certaines chipset permettent de décoder le son 7.1.

### 2.10 Le disque dur

Le disque dur est l'organe du PC servant à conserver les données de manière permanente, même lorsque le PC est hors tension, contrairement à la mémoire vive, qui s'efface à chaque redémarrage de l'ordinateur, c'est la raison pour laquelle on parle de mémoire de masse.

Un disque dur est constitué de plusieurs disques rigides en métal, verre ou en céramique appelés plateaux et empilés les uns sur les autres avec une très faible distance d'écart. Les plateaux tournent autour d'un axe (entre 4000 et 15000 tours par minute) dans le sens inverse des aiguilles d'une montre.

La lecture et l'écriture se font grâce à des têtes de lecture/écriture situées de part et d'autre de chacun des plateaux et fixées sur un axe. Ces têtes sont en fait des électroaimants qui se baissent et se soulèvent (elles ne sont qu'à 15 microns de la surface, séparées par une couche d'air provoquée par la rotation des plateaux) pour pouvoir lire l'information ou l'écrire.

::: details Focus
En informatique, la grandeur de base est le bit (binary digit). Pour simplifier les grandeurs, on utilise des multiples de l'octet (un ensemble de 8 bits), à ne pas confondre avec la définition anglaise de l'octet qui s'écrit Byte (avec le B majuscule). 
On a ainsi :
 • Le kilo-octet (Ko) : 1 Ko équivaut à 1024 octets. • Le méga-octet (Mo) : 1 Mo = 1024 Ko. • Le giga-octet (Go) : 1 Go = 1024 Mo. • Le téraoctet (To) : 1 To = 1024 Go.

:::

::: details Note: 

Sa capacité de stockage peut varier. Aujourd’hui, si vous achetez un ordinateur neuf, votre disque dur aura entre 250 et 500 Go voir 1 To de capacité de stockage. Vous pouvez brancher plusieurs disques durs sur un même ordinateur. 

:::

### 2.11 Les nappes

La nappe IDE permet de relier la carte mère aux disques durs ou au lecteur/graveur. De forme aplatie, large et de couleurs grise, elle va bientôt se faire remplacer par les connecteurs SATA.

![](/upload/nappes.png "Exemple de nappes")

### 2.12 Le clavier

Le clavier et la souris sont les éléments primordiaux qui vous permettent de donner des instructions à votre ordinateur. Le clavier permet de modifier ou entrer du texte et des chiffres. La souris permet d’utiliser les différents éléments présents à l’écran. 

### 2.13 La souris

Une souris est un dispositif de pointage relatif manuel pour ordinateur ; elle est composée d'un petit boîtier fait pour tenir sous la main, sur lequel se trouvent un ou plusieurs (généralement 2) boutons. La souris a été inventée en 1963. Elle a été améliorée dès 1979 grâce à l'adjonction d'une boule et de capteurs. Dans les premières souris l'utilisateur déplaçait l'appareil. La friction de la boule contre la table permettait le mouvement du pointeur sur l'écran. Depuis, les souris ont utilisé des mécanismes optiques ou à inertie pour détecter les mouvements.

::: details Focus CD/DVD-ROM
Un CD-ROM (abréviation de Compact Disc - Read Only Memory) ou cédérom est un disque optique utilisé pour stocker des données sous forme numérique destinées à être lues par un ordinateur ou une console de jeu compatible.

Le DVD-ROM est un compact Disc pouvant stocker 4.7 Go par face et par couche avec un maximum de deux couches par face. C'est le remplaçant du CD-ROM. L'intérêt du lecteur de  DVD  est sa retrocompatibilité  avec les CD.

:::

## 3. Allumage de l'ordinateur et systèmes d'exploitation

### 3.1 Initiation au démarrage de l’ordinateur

Afin de mettre en route votre ordinateur, il va falloir allumer votre écran et votre unité centrale.

![](/upload/demarrerlordi.png "Démarrer l'ordinateur")

Pour allumer un appareil électronique, il suffit d’appuyer sur le bouton représenté ci-dessus. Sachez qu’il n’y a pas d’ordre pour allumer l’unité centrale et l’écran. Vous retrouverez ce même pictogramme sur d’autres appareils comme un téléviseur, un lecteur DVD, votre téléphone etc.

::: details Note
Assurez-vous que votre ordinateur soit branché. Si vous utilisez un ordinateur de bureau, vous ne pourrez pas l'allumer sans l'avoir branché. Les portatifs par contre peuvent fonctionner sur une batterie, mais vous devez les brancher pour éviter tout problème de batterie faible ou d'autres types d'ennuis au moment de démarrer l'appareil.

:::

**Focus:**
Localiser le bouton d'allumage de l'ordinateur.

Ce dernier est généralement représenté par un cercle contenant un trait vertical. 

Sa position peut varier d'un ordinateur à un autre, mais vous le trouverez généralement à l'un de ces endroits :

 • Ordinateur portable : sur le côté gauche ou droit ou encore sur la façade avant de la coque. Parfois, le bouton d'allumage peut se situer juste au-dessus du clavier ou sur l'espace qui se situe au-dessus ou en dessous du clavier,

 • Ordinateur de bureau : devant ou derrière l'unité centrale. Il s'agit du boîtier en forme de caisson auquel le moniteur de votre ordinateur est relié. Le bouton d'allumage de certains i Mac est situé à l'arrière de l'écran ou sur le clavier.

### 3.2 Le système d’exploitation : Séquence de lancement de l’ordinateur

Lorsque l’unité centrale est allumée, votre ordinateur démarre. Sachez que l’écran est indépendant et que l’ordinateur démarrera même si l’écran n’est pas allumé. Mais si nous voulons voir quelque chose, il vaut mieux tout allumer en même temps. Ne faites pas cas des inscriptions « barbares » qui peuvent apparaître au tout début, elles ne nous concernent pas. Après quelques secondes votre système d’exploitation se charge.

> Le système d'exploitation est l'environnement de travail qui apparaît à l'écran. C'est l'interface entre l'utilisateur, vous et la machine. Les versions les plus connues se nomment (2000, XP, Vista, Windows 7, Windows 10). Il existe des concurrents comme Apple avec Mac OS mais aussi Linux.

Pour faire simple, le système d’exploitation est ce qui fait marcher votre machine et affiche tout ce qu’il se passe à l’écran ! Le chargement peut prendre jusqu’à plusieurs minutes selon la puissance de votre ordinateur.

Windows est le système que nous allons étudier, car installé sur la majorité des ordinateurs (le principal concurrent étant Apple et ses Macs). Au fil du temps, de nouvelles versions apparaissent.

Windows

Windows est le nom du système d'exploitation que nous étudions actuellement. Il existe plusieurs versions de Windows : Windows 98, Windows 2000, Windows XP, Windows Vista, Seven, 8, 10... Windows, (fenêtres en anglais, et vous n'allez pas tarder à comprendre pourquoi) est installé dans la majeure partie des ordinateurs que vous achetez dans le commerce.

Microsoft est une multinationale qui développe entre autres les systèmes Windows. Fondée par Bill Gates en 1975 et située à Redmond aux États-Unis, Microsoft est actuellement l'éditeur qui domine largement le marché des systèmes d'exploitation et de l'informatique avec plus de 50 milliards de dollars de revenus par an.

![](/upload/avantagesinconvénientswindows.jpg "Avantages Inconvénients Windows")

Linux

Linux est un système d'exploitation complet et libre, qui peut être utilisé en lieu et place de systèmes d'exploitation commercialisés, tels que Windows, de Microsoft. Il est accompagné de nombreux logiciels libres complémentaires, offrant un système complet aux utilisateurs.
Linux est disponible en plusieurs versions, téléchargeables gratuitement sur le net, et nommées "distributions", telles que Debian ou Ubuntu.
Lien de téléchargement : <https://www.linux.org/pages/download/>

![](/upload/avantagesinconvénientslinux.jpg "Linux OS Avantages Inconvénients")

Mac OS

Mac OS est un système d’exploitation à interface graphique développé par la marque APPLE pour équiper ses ordinateurs personnels depuis 1984.

![](/upload/avantagesinconvénientsmac.jpg "Avantages Inconvénients Mac OS")

A titre d’information, nous vous donnons les chiffres d’utilisation des systèmes d’exploitations dans le monde :

![](/upload/chiffresos2018.png "Principaux OS en matière d'utilisation 2018")

### 3.3 Le bureau

#### 3.3.1 Aspect général

À la fin du chargement, plusieurs cas de figure peuvent se présenter à vous :

Arrivée directement sur votre bureau

Dans beaucoup de cas de figure, à la fin du chargement, vous arrivez directement sur le bureau Windows. Vous pouvez alors sans plus attendre commencer à travailler, écouter de la musique, naviguer sur Internet, exécuter des logiciels etc. 

![](/upload/bureauwindows10.jpg "Bureau Windows 10")

Comme vous pourrez le voir, les différents éléments du  bureau Windows sont représentés par de petites images qu’on appelle des icônes. Sur l’image ci-dessus, le bureau Windows se compose donc de quatre icônes.

Pour ouvrir un élément placé sur le bureau Windows, placez le curseur de votre souris sur l’élément (un document, un raccourci d’un programme, une image, un dossier, …) avec lequel vous souhaitez interagir puis double-cliquez sur le bouton gauche de votre souris sans bouger.

![](/upload/bureau-ubuntu.jpg "Bureau Ubuntu")


Comme vous pouvez le voir ici, les différents éléments du  bureau sont représentés par de des icônes présentées sur la gauche de l’écran. Sur l’image ci-dessus, le bureau se compose donc de neuf icônes de base.

![](/upload/bureaudebian.jpg "Bureau Debian")

#### 3.3.2 Les icônes

C’est un symbole graphique qui représente une fonction ou une application logicielle particulière que l'on peut sélectionner et activer à partir d'un dispositif tel qu'une souris. L'icône peut aussi représenter un fichier ou un document, dans les interfaces graphiques.

![](/upload/exemplesdiconescourantes.png "Les principales icônes et leurs fonctions")

#### 3. Le menu démarrer

C’est le menu principal de l’interface graphique de la série des systèmes d’exploitation Microsoft Windows. Il a été conçu comme le point de départ pour commencer un travail ou lancer des programmes.

![](/upload/menusdemarrer.png "Exemples de menu Démarrer")

Comment éteindre son ordinateur

Vous êtes maintenant arrivé sur votre système Windows. La première chose que l’on voit est le bureau, avec un fond d’écran que nous allons pouvoir personnaliser. Différents éléments sont présents à l’écran.

Pour le moment nous allons simplement apprendre à éteindre proprement notre ordinateur.
Pour éteindre Windows, nous allons tout d’abord repérer le bouton « fenêtre » en bas à gauche de l’écran, c’est ce que l’on appelle le menu démarrer. Cliquez une fois dessus avec le bouton gauche de la souris.

Sur Windows 10, vous verrez « Alimentation », cliquez-dessus et cliquez enfin sur «Arrêter ». Sur les anciennes versions de Windows, vous aurez directement le menu « Arrêter ».

Laissez ensuite faire l’ordinateur. Vous pouvez dès à présent éteindre l’écran. L’unité centrale s’éteindra d’elle-même.

![](/upload/arreterlordi.jpg "Arrêter l'ordinateur sous Windows 10")

Sous Linux Ubuntu, le bouton d'arrêt est représenté par le logo arrêt/démarrage, il se trouve généralement dans la barre des tâches. Cliquez dessus une fois avec le bouton gauche de la souris. Un menu apparaît au centre de l'écran, confirmez en cliquant à nouveau sur "arrêter". Votre ordinateur s'éteindra automatiquement, il vous suffit d'éteindre l'écran.

::: tip Sources

<https://cours-informatique-gratuit.fr/>

<http://www.cndp.fr/crdp-dijon/IMG/pdf_dicob2i.pdf>

<http://www.dicofr.com/cgi-bin/n.pl/dicodossier/jo-16-03-99-d/>

<http://www.education-et-numerique.org/vocabulaire-informatique-de-base/>
:::

::: details Pour aller plus loin
Débuter en informatique pour les nuls

Découvrir l’informatique pour les nuls
<http://cybertrocingre.free.fr/wa_files/initiation_informatique.pdf>

Site: <https://www.vulgarisation-informatique.com/>

Site: <http://www.memoclic.com/test-de-connaissance/> =>permet d'avoir des informations supplémentaires sur des logiciels et usages spécifiques + tester ses connaissances générales du numérique

Site: <http://pros-souris.fr/> (possibilité de voir certaines formations spécifiques sur YouTube)

:::

::: details LEXIQUE

Carte graphique : La carte graphique est le constituant de votre ordinateur qui fixe les capacités d’affichage de votre écran : nombre de documents simultanément visibles, précision des traits, qualité des couleurs, vitesse des animations. Elle a un rôle déterminant pour certains usages : dessin, simulation en trois dimensions, jeux etc.

Carte mère : La carte mère est l’élément qui, comme les fondations d’un bâtiment, sert de base à l’assemblage d’un ordinateur. Elle détermine les performances maximales de tout ordinateur qui sera construit sur elle. Une carte mère défectueuse peut être remplacée, nécessitant le désassemblage puis le réassemblage de tout l’ordinateur.

Carte son : La carte son est le composant de votre ordinateur qui en fixe les capacités de restitution et d’enregistrement sonore : stéréophonie, son de qualité home cinéma, prise de son… Ses possibilités ne sont pleinement exploitées que si vous utilisez des périphériques \[enceintes, micro, écouteurs] de qualité équivalente.

Clavier : Le clavier permet de taper des textes mais également de donner des instructions à votre ordinateur. Le clavier est relié à l'unité centrale par un câble (USB le plus souvent). Il peut également être sans fil.

Clé USB : C’est une mémoire, on peut la transporter dans une poche. Malgré son nom de clé, une clé USB n’ouvre rien. C’est un petit accessoire que l’on branche sur l’ordinateur qui sert à garder en mémoire les documents que vous pouvez transférer sur un autre ordinateur en branchant la clé sur celui-ci.

Cookie : Fichier écrit sur le PC de l’internaute par le serveur web distant, permettant de garder en mémoire un contexte de connexion (produits commandés, préférences, etc.)

Copyright : Droit exclusif que se réserve un auteur ou son représentant d’exploiter commercialement pendant un nombre déterminé d’années son œuvre littéraire, artistique ou scientifique.

Corbeille : Dossier dans lequel les fichiers sont stockés lorsqu’ils sont supprimés par l’utilisateur. Sous Windows ou Mac OS, il est possible de récupérer les fichiers tant que la corbeille n’a pas été vidée. La corbeille est disponible sous Windows depuis Windows 95.

Courrier électronique : Un courrier électronique est un message que vous envoyez à une personne ou que vous recevez par internet. Il ne parvient pas directement à l’ordinateur du destinataire : il est déposé dans sa ou ses boîtes aux lettres \[BAL] internet. Le destinataire peut consulter ses messages depuis n’importe quel ordinateur relié à internet.

Créer un fichier : Créer un fichier, c’est demander au système de lui allouer un espace sur le disque de votre ordinateur, lui donner un nom et un emplacement dans votre système de rangement \[dossiers, bureau]. Vous créez un fichier à partir d’une application pour qu’il renferme un document. Mais vous créez également un fichier lorsque vous le copiez.

Curseur : Le curseur est un symbole graphique \[souvent une barre verticale clignotante] visualisant sur l’écran l’endroit où porteront les actions que vous souhaitez avoir avec le clavier \[taper du texte, enlever du texte, etc.]. Pour modifier un texte, vous devez préalablement placer le curseur à l’endroit voulu à l’aide de la souris.

Défragmenter : Le contenu des fichiers stockés sur le disque dur de l’ordinateur peut être éparpillé, fragmenté. Si vous consultez un fichier, l’ordinateur doit en retrouver les fragments. Son fonctionnement est ralenti. Défragmenter est l’action de réorganisation du disque dur où l’ordinateur réunit en un seul bloc les fragments de chaque fichier.

Dégroupage : Modalité technico-économique qui permet d’ouvrir la boucle locale de télécommunications à la concurrence. Le dégroupage consiste à mettre à disposition d’un opérateur alternatif moyennant finance des paires de cuivre nues reliées à ses propres équipements de transmission. En France, le dégroupage a eu pour effet d’accentuer la concurrence et a entraîné une diminution des prix de l’accès internet par ADSL.

Dégroupage partiel : Deux informations sont transportées via le réseau : la voix et les données. Celles-ci sont dirigées conjointement vers la ligne téléphonique, permettant de conserver la disponibilité tout en continuant à surfer en Haut Débit sur le Web. La notion de dégroupage partiel permet à un opérateur de bénéficier de l’accès à la bande de fréquence "haute" (fréquences non vocales), servant au transport des données (web). Cette solution moins onéreuse que le dégroupage total est un compromis permettant aux opérateurs de disposer de la connexion ADSL, alors que la téléphonie constitue la partie exclusive de France Télécom.

Dégroupage total : Comme l’indique son nom, le dégroupage total implique pour l’opérateur (FAI) la possession de la totalité de la bande de fréquence. Plus aucune relation contractuelle n’est alors établie entre France Télécom et la personne ayant souscrit un abonnement.

Disque Dur : Le disque dur est la partie vitale de l’ordinateur où sont enregistrées les informations. Une part minime est réservée au fonctionnement interne de l’ordinateur, le reste est laissé à votre disposition. La destruction du disque entraîne la perte irrémédiable des données si elles n’ont pas été sauvegardées ailleurs.

Dossier : Un dossier contient des documents \[lettre, facture, photographie] et des sous-dossiers. Lorsqu’on souhaite stocker un document dans l’ordinateur, on doit le placer dans un « dossier ». L’utilisateur crée des dossiers à sa convenance et y place les documents selon sa propre logique de rangement.

Ecran : Appelé aussi « moniteur », ressemble à une télévision. Sert à diffuser les textes, images, programmes produits par l'ordinateur. Un écran peut intégrer des hauts parleurs. Un écran a un bouton d'alimentation afin de l'éteindre lorsque votre ordinateur n'est pas en marche.

E-mail :Courrier informatisé qu’un utilisateur saisit, envoie ou consulte en différé, par l’intermédiaire d’un réseau. Le réseau le plus utilisé de nos jours dans ce cadre est Internet. Un courrier électronique est le plus souvent composé de texte auquel peuvent être joints d’autres documents informatisés (texte, image, son, vidéo, programme, etc.).

Émoticônes : Souriants ou grimaçants il vous faut pencher la tête vers la gauche pour voir apparaître deux yeux et une bouche. Les émoticônes sont souvent des clins d’œils ou des images de visages simplifiés. Les logiciels de messagerie instantanée proposent presque tous d’inclure des émoticônes. C’est le cas par exemple de MSN Messenger qui propose beaucoup d’émoticônes et de clins d’œil gratuits.

Enregistrer : Au moment où vous utilisez votre ordinateur \[par ex. rédiger une lettre], s’affiche à l’écran le résultat de vos actions, au fur et à mesure, comme si l’ordinateur les enregistrait. Cet enregistrement n’est pas définitif. Pour qu’il le devienne vous devez demander à « enregistrer » votre document dans un fichier et lui donner un nom.

Environnement : En informatique, « environnement » est un terme qui évoque les caractéristiques de votre installation informatique. Ce terme peut prêter à confusion car on parle également d’environnement d’utilisation de l’informatique : familial, professionnel, éducatif, ludique… On parle aussi d’environnement Windows, Mac OS, Linux…

FAI : Fournisseur d’Accès à Internet : Entreprise proposant de connecter un ordinateur ou un réseau de machines à Internet.

FAQ : Une FAQ est une aide à l’utilisation d’un outil informatique. Elle s’attache à résoudre les difficultés les plus fréquemment rencontrées par les utilisateurs de cet outil. Elle se présente comme une liste de questions et de réponses ou de problèmes et de solutions.

Fenêtre :Une fenêtre est une zone de travail délimitée par un rectangle sur l’écran, à travers laquelle on peut manipuler un dossier ou un document. Tout ce que montre l’ordinateur apparaît dans des fenêtres. Plusieurs fenêtres peuvent se superposer. On peut les déplacer, en changer les dimensions, les fermer ou les ouvrir.

Fenêtre active : Lorsque plusieurs fenêtres apparaissent à l’écran, vos actions n’auront d’effet que sur la fenêtre que vous aurez sélectionnée en cliquant dessus. La « fenêtre active » est la zone de l’écran où sont répercutées vos actions : taper du texte, lancer une vidéo… Les autres restent présentes et peuvent afficher des animations.

Feuille de calcul : Équipé de l’outil adéquat \[tableur], vous pouvez utiliser votre ordinateur pour qu’il effectue des calculs sur des tableaux informatiques. À l’image des feuilles de papier parfois grandes utilisées pour faire manuellement ces calculs, ces tableaux informatiques sont appelés « feuilles de calcul ».

Fibre Optique : La fibre optique est une technologie qui révolutionne l’Internet Haut Débit. Grâce à cette nouvelle technologie qui tente à remplacer l’ADSL, on pourrait atteindre des débits de l’ordre de 100Mb en Download et 100Mb en Upload (débit symétrique).

Fichier : Contrairement à l’usage courant où « fichier » désigne des documents en fiches, en informatique tout est fichier. Chaque document que l’on crée donne lieu à son propre fichier informatique dans lequel il est contenu. On peut renommer un fichier, en modifier le contenu, le classer et même le supprimer.

Freeware : Un freeware est un logiciel que vous avez le droit d’utiliser gratuitement. On le confond souvent avec un shareware que vous ne pouvez utiliser gratuitement que pendant une durée limitée ; au-delà, vous devez l’acheter. Freeware et shareware prévoient des conditions restrictives d’utilisation \[licence].

Logiciel: Un ordinateur est équipé de programmes qui pilotent son fonctionnement et vous permettent de réaliser des tâches : rédiger, dessiner, envoyer des messages… C’est l’ensemble de ces programmes que l’on appelle le logiciel de l’ordinateur. On parle parfois d’un logiciel. Le mot est alors synonyme de « programme ».

Login : Nom d’utilisateur qui permet d’identifier un utilisateur qui se connecte sur un ordinateur ou un site internet. Le log-in est associé à un mot de passe (password).

Loi informatique et libertés
Cette loi fut votée suite à la révélation du projet SAFARI, en 1974, qui visait à créer un identifiant unique pour tous les Français et à interconnecter les fichiers nominatifs de l’administration. Elle encadre et régule la collecte, le traitement et la conservation des données dites "personnelles" ou "nominatives". Elle instaure également une commission chargée de veiller au respect de cette loi : la CNIL. La loi définit également les droits reconnus aux personnes de consulter, de modifier ou de supprimer les informations nominatives les concernant sur un fichier.

Mémoire: Un ordinateur conserve et manipule en « mémoire » les informations qu’on lui confie. Il possède deux types de mémoires : une mémoire permanente où l’information est stockée, dormante, et une mémoire temporaire, dite mémoire vive, où l’information n’est présente qu’au moment de son utilisation : consultation, modification, impression…

Mémoire cache : La mémoire cache sert à stocker les données auxquelles un système fait le plus souvent appel, permettant ainsi de réduire les états d’attente du microprocesseur. L’utilisation de mémoire cache est un moyen astucieux pour optimiser les temps de chargement et désengorger le réseau.

Mémoire vidéo : La mémoire vidéo est une mémoire vive ayant la particularité de posséder deux bus de données. L’un d’eux est destiné aux données entrantes, l’autre aux données sortantes. Cette particularité utilisée sur les cartes vidéo permet de charger une nouvelle image (données entrantes) tout en affichant une image déjà stockée (données sortantes).

Menu : En informatique, on parle de « menu » dès l’instant où s’affiche à l’écran une liste d’options parmi lesquelles vous devez choisir. Vous indiquez l’option retenue en cliquant avec la souris ou en utilisant le clavier. En général, le menu déroulant disparaît lorsque vous avez fait votre choix d’option.

Mettre en veille Lorsque vous mettez votre ordinateur « en veille », il consomme moins d’électricité sans être éteint. L’ordinateur mémorise où vous en étiez dans votre travail et le restitue rapidement lorsque vous le réveillez. La mise en veille « prolongée » provoque l’extinction de l’ordinateur, stoppant toute consommation d’énergie.

Micro SD : Standard de carte mémoire flash dérivé du format SD autorisant des capacités de stockage allant de 128 Mo à 4 Go pour un format de 15 mm x 11 mm x 1 mm.

Micro-processeur : Le microprocesseur est le cœur d’un micro-ordinateur. Il s’agit d’un circuit intégré, c’est-à-dire d’une plaquette de silicium supportant un grand nombre de composants, d’un type particulier.

Modem : Indispensable pour accéder à internet, le modem est l’appareil qui permet à votre ordinateur de se brancher à un réseau de communication initialement prévu pour un autre usage : réseau téléphonique, réseau de télévision câblée. On le branche d’un côté sur la prise de ce réseau, de l’autre sur l’ordinateur.

Moniteur : « Moniteur » est le terme technique et commercial employé pour désigner l’écran, en tant qu’élément indépendant de votre ordinateur de bureau. On en distingue deux types : les écrans traditionnels \[cathodiques ou CRT] et les écrans plats \[LCD, plasma].

Mot de passe : Un mot de passe est un code secret qui protège l’accès à des équipements, à des services ou à des parties de votre ordinateur. Le simple accès à un ordinateur peut être protégé par un mot de passe. Dans certains cas, vous êtes libre de le choisir, dans d’autres, il est choisi pour vous par la technique.

Moteur de recherche : Un moteur de recherche est un outil qui permet de trouver des informations sur internet. Il en fournit une liste aussi complète que possible, le plus souvent sans classement logique pour l’utilisateur. On peut interroger le moteur de recherche en tapant les mots qui décrivent l’information que l’on cherche.

Multimédia : Le multimédia \[multi-médias] est la mise en œuvre simultanée de plusieurs médias : texte, photo, son… Un ordinateur est dit multimédia s’il vous permet d’écrire des textes, de dessiner, d’écouter de la musique et de regarder des films. Un document est dit multimédia s’il combine au moins deux types de ces éléments.

Navigateur : « Navigateur » est le nom de l’outil qui sert à parcourir internet et à visiter les sites Web. On connaît plutôt ces outils sous leurs noms propres tels que Internet Explorer, Firefox ou Safari. La plupart des sites peuvent être consultés avec n’importe quel navigateur récent. Ils seront cependant plus ou moins bien affichés.

Newsletter : Une newsletter est une lettre d’information expédiée par mail à travers internet. Pour la recevoir vous devez, au minimum, disposer d’une adresse de courrier électronique. Pour la consulter, vous devez avoir accès à un ordinateur relié à internet.

Nom de fichier : Tout fichier informatique possède un nom. Si tous les fichiers classés dans le même dossier ont obligatoirement un nom différent, des fichiers classés dans des dossiers différents peuvent porter le même nom. Quand vous créez un fichier avec une application, celle-ci ajoute automatiquement une marque \[extension] au nom que vous lui donnez.

Octet : Unité de mesure de la quantité de données informatique. Octet comme octuple, c’est un ensemble de 8 bits.

Ordinateur : C’est le nom donné à la machine informatique en état de marche, avec tous les outils dont on l’aura dotée. Un ordinateur est souvent vendu dans plusieurs configurations possibles, adaptées à différents usages : travail de bureau, jeux, conception graphique, montage vidéo… Vous pourrez faire évoluer votre configuration.

Ordinateur portable (ou laptop) : est un ordinateur personnel qui, grâce à un poids et un encombrement limités, peut être transporté très facilement. L'ordinateur portable possède une batterie qui lui permet d'être autonome jusqu'à plusieurs heures sans être alimenté en courant électrique.

Ouvrir un document : « Ouvrir un document » est l’opération qui consiste à afficher le contenu d’un document, d’un fichier, pour le consulter, le modifier ou l’imprimer. Vous ne pouvez « ouvrir » un document que si votre ordinateur est équipé d’un outil \[application] capable, au minimum, d’en afficher le contenu.

Ouvrir une session : Lorsque vous démarrez l’ordinateur, celui-ci peut vous demander de vous identifier ou de choisir le compte que vous souhaitez utiliser. Dès l’instant où vous vous identifiez, vous « ouvrez une session ». Si vous fermez la session, vous devrez à nouveau vous identifier, même si vous n’avez pas éteint l’ordinateur.

Périphérique : « Périphérique » est un terme général qui désigne un équipement extérieur, branché sur votre ordinateur. L’écran, le clavier et la souris sont les « périphériques » les plus connus. Une imprimante, un scanner, un modem, sont également des périphériques, qui étendent les possibilités de votre ordinateur.

Pièce jointe : Une pièce jointe est un fichier informatique que l’on associe à un message internet afin de l’expédier par courrier électronique. Ainsi, on peut envoyer le message « ci-joint la facture » et y associer le fichier de la facture. Quand le destinataire consulte le message, il voit qu’il contient une pièce jointe.

Pilote : Ensemble de fichiers système permettant l’identification et l’utilisation des périphériques connectés à un ordinateur. Un pilote est non seulement spécifique à un équipement mais également au système utilisé dans l’ordinateur. Parvenir à brancher un nouvel équipement \[appareil, imprimante…] sur l’ordinateur ne suffit pas à le rendre utilisable. Il faut disposer d’un programme dans l’ordinateur permettant de contrôler l’équipement : le pilote.

Pirate : Un « pirate » informatique est une personne qui tente de prendre le contrôle de votre ordinateur par effraction. S’il parvient à ses fins, on dit que l’ordinateur est « piraté », compromis. Un pirate peut être un inconnu ou un utilisateur « autorisé » qui tente d’accéder à des informations qui ne lui appartiennent pas.

Pixel : Mot anglais provenant de la concaténation du mot Picture et du mot élément". Il s’agit du plus petit élément d’une surface d’affichage auquel on puisse associer individuellement une couleur.

Planté : En jargon informatique, « planté » signifie « hors de contrôle de l’utilisateur ». Un outil informatique \[matériel, logiciel] « planté » peut avoir plusieurs comportements : s’arrêter et ne répondre à aucune sollicitation, accepter des commandes mais en exécuter d’autres, avoir une activité autonome…

Processeur : Le processeur d’un ordinateur est l’équivalent du moteur d’un véhicule. Il conditionne les performances de l’ordinateur. Sa puissance est indiquée en GHz : 2GHz valent mieux que 1GHz. Pour une utilisation courante, tous les modèles de processeurs actuellement disponibles conviennent.

Programme : Un programme est un outil informatique installé sur le disque dur de votre ordinateur. « Programme » est un terme général qui désigne aussi bien une application, un utilitaire, un pilote, qu’une partie du système d’exploitation. Un programme est contenu dans un ou plusieurs fichiers rangés dans des dossiers qui leur sont réservés.

Raccourci : Un raccourci est un symbole à l’écran qui renvoie vers un fichier. Il prend généralement la forme d’une icône indiquant qu’il s’agit d’un raccourci et non directement du fichier. Vous pouvez créer des raccourcis vers tout type de fichier \[document, application, pilote]. Effacer un raccourci n’efface pas le fichier auquel il renvoie.

Raccourci clavier : Les menus affichant les actions possibles indiquent souvent ce qu’on appelle le « raccourci clavier » correspondant. Un tel raccourci est un moyen d’obtenir le même résultat qu’avec la souris à partir du seul clavier. Il consiste le plus souvent à appuyer simultanément sur plusieurs touches \[par ex : la touche Ctrl et la touche « S »].

RAM : Pour fonctionner votre ordinateur doit être équipé de RAM \[ou mémoire vive]. La quantité de RAM nécessaire dépend de l’usage que vous faites de votre ordinateur : réaliser un catalogue illustré de 50 pages demande plus de RAM que rédiger un courrier car une image demande beaucoup plus d’espace mémoire que du texte.

Redémarrer :Redémarrer votre système informatique consiste à arrêter le système puis à le relancer sans éteindre l’ordinateur. Une fois votre demande de redémarrage prise en compte, arrêt et relance s’enchaînent automatiquement. Redémarrer l’ordinateur signifie l’arrêter, attendre son extinction, puis le relancer.

Renommer : Tout fichier ou dossier stocké dans votre ordinateur possède un nom. Le « renommer » consiste à changer ce nom. Renommer un fichier ou un dossier ne modifie pas son contenu. Vous êtes libre de renommer les fichiers et dossiers que vous créez ou que l’on vous transmet, par exemple dans un but d’une meilleure organisation de votre classement.

Réseau : Un réseau relie vos équipements informatiques entre eux. Depuis votre ordinateur, vous accédez aux informations stockées sur un autre ordinateur. Plusieurs ordinateurs organisés en réseau peuvent utiliser la même imprimante. On parle aussi de réseau internet, d’« intranet », d’« extranet » ou encore de réseau Ethernet pour un réseau local.

Réseau social : Communauté d’individus ou d’organisations reliés de manière directe ou indirecte entre eux, en fonction de centres d’intérêts, de points de vue ou encore de besoins communs. Sur internet de très nombreux sites permettent la création de ce type de réseaux. Les plus connus étant Facebook, LinkedIn, Myspace ou encore Flickr pour les photographies.

Souris : Vous permet d'interagir avec l'ordinateur, d'ouvrir des programmes et d'animer des éléments. Une souris peut avoir un ou deux boutons qui ont des effets différents que vous cliquiez sur le bouton droit ou gauche. Le clic le plus utilisé étant le gauche. Une souris peut être connecté à votre ordinateur par câble ou sans fil.

Système d'exploitation :logiciel le plus important d'un ordinateur. Il permet tout d'abord le démarrage de l'ordinateur et est indispensable à la mise en œuvre des autres programmes présents sur le PC. Il regroupe un ensemble de programmes qui permettent l'utilisation de l'ordinateur et de ses périphériques (écran, imprimante, clavier, etc.). Sans système d'exploitation, l'ordinateur n'est pas capable de fonctionner. En anglais on parle d'Operating System ou OS.

Télécharger : Lorsqu’on fait venir sur son ordinateur pour l’enregistrer un fichier présent sur internet, on le télécharge. Ceci revient à effectuer une copie sur votre ordinateur du fichier mis à votre disposition à distance. Vous pouvez télécharger tout ce qui peut être rangé dans un fichier : une déclaration fiscale, une photo, un texte, un film…

Unité centrale : Appelée aussi « tour », l'unité centrale est le boîtier contenant tout le matériel électronique permettant à l'ordinateur de fonctionner. Le clavier, la souris, l'écran y sont reliés. C'est la partie d'un ordinateur qui effectue des calculs. C'est le cerveau de l'ordinateur, les informations y sont stockées et traitées. 

URL : Ce sigle désigne le moyen d’identifier un document consultable par le Web. Pour faire connaître un document ou un site existant sur le Web, il faut en donner l’URL. Toute personne peut indiquer cette adresse à son ordinateur et accéder à la consultation du document ou du site.

USB : Protocole de transfert de données entre un ordinateur et ses périphériques. Le port USB est utilisé pour brancher des appareils photo numériques, caméscopes numériques, claviers, souris, webcams, scanners, imprimantes…

Utilitaire : Un « utilitaire » est un programme qui accroît votre confort ou votre plaisir dans l’utilisation de votre ordinateur. Ajouter un utilitaire à votre ordinateur ne vous permet pas de consulter ou de créer de nouveaux types de documents, il ne vous permet que d’adapter son utilisation. En cela, il se distingue d’une application.

Virus : Un virus informatique est un programme malfaisant venu de l’extérieur, dissimulé à l’intérieur d’un courrier électronique, d’un document ou d’un autre programme. Il est capable de modifier les informations conservées dans votre ordinateur. Il peut se propager d’un ordinateur à l’autre par transfert d’informations infectées.

Web : Le « Web » est composé de milliards de sources d’informations réparties dans le monde et liées entre elles. C’est la partie la plus connue du réseau internet \[les autres étant le courrier, le transfert de fichiers…]. Le mot Web est l’abréviation de « World Wide Web ». Les moteurs de recherche vous aident à vous y retrouver.

Webcam : Caméra fournissant des images en direct à un site Web, on parle aussi de Livecam ou de Netcam.

Wifi : Le Wifi permet d’utiliser internet depuis son ordinateur sans avoir à le relier par un câble. Le Wifi privé, similaire au téléphone sans fil domestique, prolonge votre installation internet à partir d’une base. Le Wifi public, à l’image du téléphone mobile, vous permet de vous relier à internet dans la rue ou les lieux publics.

Wiki : Un Wiki est un site web dynamique dont tout visiteur peut modifier les pages à volonté. Il permet donc de communiquer ses idées rapidement.

WWW : Le World Wide Web (littéralement la toile mondiale) est communément appelé le Web et abrégé en "WWW" ou "W3". Le Web actuel est constitué de pages web, physiquement stockées dans des serveurs web (des ordinateurs connectés à Internet), structurées en sites web et reliées entre elles par des liens hypertextes.

:::