---
title: 🎉 Sites de divertissements culturels
---
# **Les sites de divertissement culturel**

::: tip Modules APTIC

* *Aucun ?*

• **Résumé :**

Internet a permis une démocratisation culturelle sans précédent dans l'Histoire de l'Humanité. Passée une période où le piratage était la norme de nombreuses offres légales ont émergé suivant des modèles économiques propres que nous allons étudier aujourd'hui que cela soit dans le cadre de la musique, la vidéo, la littérature, les JV …

• **Nombre d'heures :** 2 heures

• **Publics :** Débutant / intermédiaire / confirmé

• **Prérequis:** Maîtrise minimum du Français : lu , écrit, parlé.

Avoir suivi les modules Navigateur, Recherche sur Internet, Cloud.

• **Objectifs de l'atelier**

* Apprendre à utiliser plusieurs sites de référence : Youtube, Netflix, Musicme …
* Appréhender les différents modèles économiques présents : abonnements, freemium, publicité
* Comprendre la notion d'économie de l'attention

:::

**Sommaire :**

[[toc]]

## 1. Regarder des vidéos en ligne

Objectifs intermédiaires : Comprendre les modèles économiques de la vidéo en ligne.

### 1.1 Youtube (disponible sur PC, smartphone, smartTV)

Ce site de streaming vidéo, lancé en 2005 et racheté par Google en 2007, propose de visionner ""gratuitement"" des vidéos, se rémunérant sur les publicités placées avant, pendant et parfois à l'intérieur de celles-ci (on parlera alors de vidéos sponsorisées ex :NordVPN).

Il n'est pas obligé de posséder un compte pour y accéder, toutefois certaines options ne sont disponibles qu'avec un compte.

L'écran d'accueil se présente de la sorte : 

![](/upload/captureyoutube1.jpg "Page d'accueil Youtube")

A noter que la Bibliothèque et les Abonnements ne sont disponibles qu'avec un compte.

Les recommandations apparaîtront dès lors que vous aurez regarder des vidéos dans la page d'accueil.

Dans les paramètres vous pouvez configurer :

![](/upload/captureyoutube2.jpg "Paramètres Youtube")

A noter que, par défaut, l'accès à du contenu 18+ est assujetti à la possession d'un compte d'une personne âgée de plus de 18 ans.

Les vidéos sont constituées de la sorte :

![](/upload/captureyoutube3.jpg "Anatomie d'une vidéo Youtube")

Quand on clique dessus on atterrit alors sur :

![](/upload/captureyoutube4.jpg "Anatomie d'une vidéo Youtube 2")

La vidéo possède ses propres options :

![](/upload/captureyoutube5.jpg "Options vidéo Youtube")

A noter que l'option sous-titres va bientôt disparaître.

Quand vous faites une recherche sachez aussi que vous pouvez filtrer vos résultats en cliquant sur le bouton Filtrer. Vous aurez alors les options suivantes :

![](/upload/filtresrch.jpg "Filtres Youtube")

### 1.2 Netflix (disponible sur PC, smartphone, smartTV)

Site **uniquement** disponible **via un abonnement** il vous permet de regarder des films en ligne.

Parmi les sites similaires on peut citer Amazon Prime ou Disney +.

Une fois votre abonnement pris, rentrez votre adresse mail et votre mot de passe.

Puis créez vous un profil (vous pouvez cocher l'option Enfants dans ce cas le contenu 12+ ne seront pas affichés).

![](/upload/netflixentrée.jpg "Création Profil Netflix")

Une fois le profil créé vous serez amené à la page d'accueil :

![](/upload/capturenetflix1.jpg "Page d'accueil de Netflix")

On retrouve une sélection thématique plus détaillée en descendant vers le bas :

![](/upload/netflix4.jpg)

Lancez (enfin) le film en cliquant sur son image.

Ici le modèle économique de Netflix repose sur les abonnements payés par les abonnés pour payer des droits de diffusion des films durant une période donnée. Netflix est le service disposant des films les plus connus, de séries exclusives et du catalogue animé le plus fourni là où Amazon Prime va par exemple proposer des films moins connus et des documentaires.

::: danger Important : L'économie de l'attention

Comme vous avez pu le remarquer des sites comme Youtube ou Netflix avec leurs suggestions/recommandations, l'activation par défaut de la lecture automatique pour Youtube ou le saut d'introduction pour Netflix, font tout pour vous faire rester dessus un maximum de temps.

C'est une pratique délibérément voulue appelée économie de l'attention.

Avec l'explosion des contenus la rareté vient en effet aujourd'hui de notre temps de cerveau disponible appelée plus couramment attention.

Afin de limiter son impact nous invitons à limiter le temps d'exposition aux écrans ou encore installer l'extension Minimal pour Firefox.

:::

## 2. Écouter de la musique en ligne

### 2.1 Soundcloud (disponible sur PC et smartphone)

Ce site, lancé en 2007, est un peu l'équivalent de Youtube pour la musique à ceci près qu'il se focalise désormais sur la musique indé (donc hors du circuit des gros labels de musique). Il dispose de fonctionnalités uniquement déblocables au moyen d'un abonnement (écoute hors ligne, création de playlists, sauvegarde de musiques dans une bibliothèque …) mais le site est du reste gratuit. On parle de modèle freemium dans ce cas c'est-à-dire que l'accès à une partie de celui-ci est soumis à un paiement financier.

![](/upload/capturesoundcloud1.jpg "Page d'accueil de Soundcloud")

![](/upload/tendancessoundcloud.jpg "Tendances Soundcloud")

Accessible en scrollant vers le bas à la page d'accueil

Une fois votre recherche effectuée vous tomberez sur cela :

![](/upload/capturesoundcloud2.jpg "Décomposition d'un résultat Soundcloud")

### 2.2 Musicme (disponible sur PC et smartphone)

Ce site fondé en 2006 propose quant à lui plutôt les musiques dites mainstreams (issues des labels) selon là encore un modèle freemium. Sans abonnement vous pourrez écouter des titres à qualité réduite mais pas les télécharger, faire de playlists, écouter hors ligne ou écouter certains titres.

![](/upload/capturemusicme1.jpg "Page d'accueil MusicMe")

Une fois votre recherche effectuée vous tomberez sur ceci :

![](/upload/capturemusicme2.jpg "Résultats Recherche Musicme")

A noter qu’il existe un 3ème site de référence en matière de streaming musical à savoir Deezer mais qu’il est désormais sous abonnement uniquement.

## **3. Littérature en ligne**

### 3.1 La bibliothèque numérique de Tv5Monde

Sur ce site vous trouverez des classiques de la littérature libres de droit du 13ème au 20ème siècle en libre accès.

![](/upload/capturebib1.jpg "Page d'accueil Bibliothèque Numérique TV5Monde")

Une fois la recherche effectuée, cliquez sur le livre qui correspond puis sur Télécharger. Vous aurez alors le choix entre le format Epub (pour tablettes) ou Pdf.

![](/upload/livrebibnum.jpg "Sélection d'un livre ")

### 3.2 LitteratureAudio.com

Ce site fondé par l'association *Des livres à lire et à entendre* en 2007 a pour but de faciliter l'accès à tous (en particulier des non-voyants/malvoyants) à la littérature en proposant gratuitement des lectures de livres par des bénévoles à télécharger au format MP3.

![](/upload/capturelittaudio1.jpg "Page d'accueil LitteratureAudio.com")

Une fois votre livre audio sélectionné cliquez sur Télécharger le mp3 ou clic droit enregistrer sous.

![](/upload/litteratureaudio2.jpg "Exemple de livre à télécharger")

**4. Les jeux vidéo**

### 4.1 Itch.io

Ce site, fondé en 2013, concurrent du logiciel Steam, a pour but de favoriser les créateurs de jeu vidéo indépendants. Ces derniers mettent à disposition, gratuitement ou non leurs créations, jouables en ligne ou via téléchargement, sous Windows/Mac ou Linux.

Uniquement en anglais il dispose néanmoins d'un catalogue intéressant et éclectique de 180000 jeux. Seules certaines options dont l'achat de jeux nécessitent un compte.

![](/upload/captureitchio1.jpg "Page d'accueil Itch.io")

Un jeu vidéo sur Itch.io se compose de la sorte :

![](/upload/captureitchio2.jpg "Anatomie d'un jeu vidéo sur Itch.io")

En passant la souris sur l’image, une ou plusieurs icônes apparaîtront pour indiquer la ou les plateformes compatibles du jeu. 

Les jeux directement jouables en ligne auront l’indication *Play in Browser* (jouer dans le Navigateur) ou *Web*. 

Dans ce cas le jeu se lancera directement en cliquant sur l’image. Dans le cas contraire il vous faudra le télécharger, s’il est compatible.

### 4.2 Jeux.fr

Beaucoup plus connu, ce site propose une centaine de jeux vidéos en ligne, gratuits, en français et adaptés pour la plupart aux enfants.

Son modèle économique est basée sur la publicité placée avant les jeux mais aussi la collecte des cookies (les traces de connexion que nous laissons un peu partout sur Internet) aussi pensez à bien les refuser avant d’entrer sur le site.

Un compte n’est nécessaire que pour la sauvegarde des résultats.

Son écran d’accueil est le suivant :

![](/upload/capturejeuxfr1.jpg "Page d'accueil Jeux.fr")

Une fois le jeu trouvé cliquez dessus, vous aurez alors l’interface suivante :

![](/upload/capturejeuxfr2.jpg)

Cliquez sur *Play* pour lancer le jeu

::: tip Sources

<https://www.archimedia.fr/quels-sont-les-meilleurs-divertissements-en-ligne/>

<https://www.youtube.com/watch?v=rMV1WaWGb3I>

:::