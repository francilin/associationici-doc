---
title: 📧 Boite Mail
---
# Découverte de la boîte mail

::: tip Modules APTIC

* *12 : Internet fonctionnement des mails*
* *13 : Fonctionnement des clients webmail*
* *42 : Créer et paramétrer un compte Google*
* *48 : Envoyer, recevoir, gérer ses mails*
* **Résumé**

*A faire*

* **Nombre d'heures:** 3h
* **Publics :** Grand débutant/débutant
* **Prérequis:**

Maîtrise minimum du Français : lu , écrit, parlé .

* **Objectifs de l'atelier**  :

  * Créer une boite mail
  * Savoir envoyer et répondre à un mail
  * Savoir envoyer un mail avec une pièce jointe

:::

  **Sommaire**  :

[[toc]] 

## **1. Rappels historiques autour de la boite mail et de l'e-mail**

### **1.1 Préhistoire**

On considère souvent que l'histoire de l'email (ou courrier électronique) débute en 1965, à une époque ou Internet n'existait pas encore. C'est en effet durant cette année que furent mis en place les premiers échanges de messages entre utilisateurs sur des réseaux privés.

L'un des premiers systèmes ayant autorisé l'échange de messages fut le Compatible Time-Sharing System (CTSS) de la fameuse Institut de Technologies du Massachusetts (MIT), bien que cette paternité lui soit aussi revendiquée par la société System Development Corporation (SDC) et son propre Time-Sharing System (Système de Temps Partagé) créé pour le Q32, un ordinateur spécialement fabriqué par IBM pour l'Armée de l'Air américaine.

Cela étant, le courrier électronique ne naît véritablement qu'à partir de la création du réseau ARPAnet, l'ancêtre d'Internet. Et c'est à l'automne 1971 qu'un ingénieur du nom de Ray (Raymond Samuel) Tomlinson, travaillant chez Bolt Beranek and Newman Technologies (société employée par le Ministère américain de la Défense pour le développement du réseau ARPA), s'envoya à lui-même le premier email de l'Histoire.

Auparavant, les messages ne pouvaient être envoyés qu'aux utilisateurs d'un même domaine et consultés, le plus souvent, sur la même machine que celle qui servait à écrire et déposer les messages.

### **1.2 Genèse d'une révolution**

Ray Tomlinson conçu une application spécifique à l'envoi de messages, SNDMSG (Send Message), ainsi qu'une application dédiée à la lecture de ces derniers, READMAIL. Ces applications autorisaient la lecture de messages par différents utilisateurs mais sur une seule et même machine. L'idée de Ray Tomlinson fut d'ajouter à ces applications un protocole d'envoi et de réception de fichiers à travers le réseau ARPAnet, le CPYNET.

Après l'écriture de quelques 200 lignes de code et la création de deux boîtes électroniques sur deux machines côte à côte, Ray Tomlinson devait encore trouver un moyen pour que le programme différencie facilement un message local d'un message réseau. C'est alors qu'il eut l'idée de dissocier nom d'utilisateur et nom d'hôte avec le seul caractère qui n'était utilisé dans aucun nom propre ni, et surtout, dans aucun nom d'entreprise – qui, par la suite, pouvait servir de préfixe au nom de domaine : le symbole @ (arobase).

Ray Tomlinson parvient ainsi à s'envoyer le premier « netmail » de test avec pour seul contenu « QWERTYUIOP », soit la première ligne de caractère du clavier anglophone.

Le premier véritable email envoyé à des utilisateurs le fut par Ray Tomlinson pour annoncer justement la naissance de son application et en expliquer son fonctionnement aux employés de BBN Technologies.

### **1.3 Succès**

L'email connu un tel succès qu'il devint vite inenvisageable, pour les utilisateurs du réseau ARPAnet, de s'en passer. En conséquence, le logiciel obtint très vite le qualificatif de « killer app » (ou « application-qui-tue ») du réseau ARPAnet, et les développeurs s'attachèrent soit à améliorer le programme et son protocole de transfert, soit à développer leurs propres solutions.

Dès 1973, une étude menée par l'ARPA dévoilait que 75% du trafic de son réseau était généré par l'échange d'emails.

C'est en 1975 que l'email va se voir adjoindre un véritable client de messagerie avec la création de MSG par John Vittal, alors ingénieur à l'Institut des Sciences de l'Information, dans l'Université de Californie du Sud. Son programme, considéré comme l'ancêtre des clients de messagerie modernes comme Outlook ou Thunderbird, permet à lui seul de rassembler les fonctions de lecture, d'envoi, de transfert des mails, d'adjonction de pièces jointes et la notion de « corbeille » pour les messages supprimés, le tout dans une interface simplifiée.

### **1.4 Le premier SPAM**

En 1978, Gray Thuerk, un employé de Digital Equipment Corporation (sous-traitant pour le Ministère américain de la Défense), souhaite faire connaître l'un des nouveaux produits de sa société aux ingénieurs du réseau ARPAnet. C'est dans le but de ce simplifier la tâche d'envoi du même mail pour chaque personne que Gray Thuerk récupérera les adresses mail de toutes les 393 personnes, connectées à l'époque sur le réseau, pour leur transmettre ce qui est aujourd'hui considéré comme le premier spam de l'histoire. Son action fut critiquée par l'ARPA, jugeant l'annonce commerciale inappropriée avec l'utilisation qui devait être faite du réseau, à l'origine prévue pour la recherche et le développement technologique.

### **1.5 Les spams**

Le terme de spam a été assimilé aux courriers indésirables à la suite d'un célèbre sketch des Monthy Python qui fut rediffusé en 1975 sur la BBC (sa première diffusion datant du 15 décembre 1970).

Mais le spam n'est pas un mot purement inventé par les Monthy Python. Il s'agit d'un véritable aliment en conserve qui fut fabriqué par l'américain Hormel Foods Corporation pendant la Seconde Guerre mondiale et distribué aux soldats de l'armée. Son nom est l'acronyme de « Shoulder of Pork And haM », ou, en français, « épaule de porc et jambon ».

Relayé par la mauvaise presse qu'en faisait les soldats, le mauvais goût du produit fut par la suite notoirement connu du public anglo-saxon.

C'est à partir de cette culture populaire et du fameux sketch des Monthy Python que furent désignés comme spam les messages non-désirés, à caractère commercial, qui ont fini par « envahir » les boîtes électroniques.

Aujourd'hui le spam constitue entre 55% et 95% des échanges de mails réalisés dans le monde (selon les données statistiques de la société Softscan)…

### **1.6 Quelques dates clés :**

**Automne 1971**. Naissance du courrier électronique. Ray Tomlinson, ingénieur de la société BBN, aménage le programme SNDMSG/READMAIL. Il se crée 2 boîtes aux lettres électroniques sur 2 ordinateurs situés côte-à-côte et réussit à envoyer un message d'un ordinateur à un autre. On ne l'appelle pas encore e-mail mais Netmail (pour Network Mail).

**1975**. John Vittal conçoit *MSG*, le premier logiciel de messagerie à inclure une interface personnalisable, des fonctions d'envoi, de réponse, de renvoi et de classement des messages. *MSG* est considéré comme le premier logiciel de messagerie moderne.

**3 mai 1978**. Gary Thuerk, commercial de la société informatique DEC, envoie le premier spam.

**1988**. Steve Dorner développe la première version d'Eudora. Ce programme est le premier logiciel de messagerie grand public.

**1992**. Le premier e-mail WYSIWYG (autrement dit formaté) voit le jour chez CompuServe : polices de caractère, couleurs et émoticônes font pour la première fois leur apparition. La même année, chez Microsoft, apparaît un logiciel : Outlook pour MS-DOS.

**1993**. AOL établit des passerelles entre son système d'e-mail propriétaire et celui de l'Internet. L'e-mail Internet devient *de facto* standard.

**1994**. Jim Clark et Marc Andreessen créent MCC qui va rapidement devenir Netscape. En décembre, *Nescape Navigator*, diffusé gratuitement, inclut un module de courrier électronique : Netscape Mail. 

**1996**. Le 4 juillet, jour de la fête nationale américaine, Jack Smith et Sabeer Bhatia lancent, avec 300 000 $ d'investissement, un service de courrier électronique gratuit sur le Web baptisé Hotmail. En quelques mois, le cap des 100 000 utilisateurs sera dépassé

**1997**. Microsoft livre Outlook Express. Le logiciel de messagerie, qui deviendra rapidement leader dans sa catégorie, est associé à Internet Explorer 4. De son, côté, Yahoo! acquiert Four11, qui édite un webmail gratuit (RocketMail) et un annuaire d'adresses e-mail. En France, 3 pionniers créent Caramail.

**1998**. Microsoft rachète Hotmail le 31 décembre 1997. La transaction est évaluée à 400 millions de dollars. Il y a alors 9 millions d'abonnés. Fin 1998, Hotmail atteindra 30 millions d'abonnés. Lire l'histoire de Hotmail

**Avril 2004**. Google annonce le lancement de sa messagerie gratuite Gmail, d'une capacité de 1 Go. Les concurrents embraieront rapidement. Gmail est le premier grand webmail à mettre en oeuvre les technologies AJAX, qui permettent de doter l'interface de fonctions se rapprochant de celles d'un logiciel de messagerie. Lire l'histoire de Gmail

**Décembre 2004**. Un mois après le lancement du navigateur Firefox, la fondation Mozilla lance la première version du logiciel de courrier électronique Thunderbird, qui vise à contrer la domination d'Outlook Express. Voir l'histoire de Thunderbird

**2007**. Yahoo! est le premier grand acteur à annoncer une capacité de stockage illimitée pour son service Yahoo Mail

**Mai 2013**. Microsoft rebaptise sa messagerie Outlook.com et abandonne définitivement la marque Hotmail.

## 2. Gérer une boite mail au quotidien

### **2.1 Rappel de définitions :**

* Qu'est-ce qu'un e-mail ?

Le courrier électronique ou « e-mail » en anglais, est une des fonctionnalités les plus répandues sur Internet. L' e-mail permet l'envoi et la réception de messages, d'un interlocuteur vers un autre. Pour échanger par e-mail, chaque interlocuteur doit disposer dune adresse électronique (ou adresse e-mail).

* Qu'est ce qu'une boite mail ?

Une boite mail est une boite aux lettres électronique pour le courrier électronique. Dans le cadre d'une messagerie électronique, c'est un espace de mémoire réservé à quelqu'un, dans lequel sont conservés les messages qui lui sont destinés et les messages qu'il envoie.

* Qu'est ce qu'une messagerie électronique ?

Une messagerie électronique est un logiciel dont le but est de recevoir, de classer et d'envoyer vos courriers électroniques (e-mails)

### **2.2 Le paysage économique de la messagerie en ligne**

![](/upload/paysageboitesmail.png "Paysage Boite Mail en 2019")

### **2.3 Comment se procurer une adresse e-mail ?**

On peut créer une adresse électronique (ou adresse e-mail) auprès de votre fournisseur d'accès Internet, ou auprès de structures spécialisées dans le service d'échange d'e-mails tels que hotmail, gmail, laposte, etc.

Une inscription en ligne et une création de compte est nécessaire pour avoir l'usage des services proposés par les opérateurs. La plupart du temps cette inscription est gratuite et l'usage des services de base également.

Pour être valide, une adresse e-mail doit respecter certaines règles, elle se compose :

* d' **un identifiant** ,
* d' **une arobase** (@)
* d' **un nom de domaine**  :

Elle se composera alors ainsi : identifiant@nomdedomaine.fr (ou .com,…).

Par exemple : dupont@orange.fr  ou encore dupont@hotmail.fr

Une adresse e-mail s'écrit toujours en minuscule, sans accent et sans espace, peut contenir des chiffres, et comporte obligatoirement une arobase (@). Vous pouvez uniquement choisir votre identifiant ; le nom de domaine est automatiquement généré selon la structure de messagerie choisie.

Étape 1: créer une boite mail

Ici nous allons voir ensemble comment créer un adresse mail.

Nous proposons de créer une adresse mail sur le site de laposte.net, car il permet d'avoir des identifiants pour se connecter aux différents service publics et option coffre-fort numérique

Lien vers le site <https://www.laposte.net/accueil>

![](/upload/maillaposte1.png "Page d'accueil La Poste Mail")

**L'inscription (pas à pas)**

![](/upload/maillaposte2.png "Forumulaire d'inscription La Poste Mail")

1. informations personnelles à fournir :

(uniquement les champs obligatoires *)

\* civilité, identité, naissance, cp

2. le choix du compte (adresse à créer) :

\* nom.prenom (ou prenom.nom) 

\* mot de passe (relativement sécurisé)

3. premier test de sécurité (CAPTCHA) :

![](/upload/captcha.png "Exemple de Captcha")

![](/upload/maillaposte3.png "Formulaire d'inscription La Poste Mail 2")

1. Cocher le stricte minimum

lu et accepté :

‣ les CGU et

‣ la charte informatique et liberté

2. Valider l'étape

![](/upload/maillaposte4.png "Formulaire d'inscription La Poste Mail 3")

**2ème test de sécurité**

La *validation d'étape finale* de création nécessite un code de vérification, fourni par La Poste.

Pour la valider l'adresse créée via :

\* une adresse email secondaire

ou

\* un numéro de téléphone mobile

![](/upload/maillaposte5.jpg "Procédure sécurité n°2")

![](/upload/maillaposte6.png "Finalisation création de compte 1")

La Poste envoie un code de vérification au téléphone ou email fourni

\* inscrire le code pour finaliser la création du compte

**Finalisation de la création du compte**

![](/upload/maillaposte7.png "Finalisation création de compte 2")

![](/upload/maillaposte8.jpg "Finalisation création de compte 3")

::: details Tuto création mail Hotmail

 (à rédiger)

[https://www.generation-nt.com/creer-adresse-messagerie-hotmail-fr-live-msn-mail-email-inscription-compte-windows-live-id-astuce-1083611-1.html ](https://www.generation-nt.com/creer-adresse-messagerie-hotmail-fr-live-msn-mail-email-inscription-compte-windows-live-id-astuce-1083611-1.html)

:::



::: details Création d'adresse mail sur Gmail

Sur Google.fr cliquez sur l'icône en haut à droite (Connexion):

![](/upload/gmail000.jpg "Création Compte Gmail 0")

Puis sur:

![](/upload/gmail0.jpg "Création Compte Gmail 1")

Remplissez ensuite les champs suivants:

![](/upload/gmail1.jpg "Création Compte Gmail 2")

Un numéro de téléphone, une adresse mail ou un captcha peuvent vous être demandés pour valider la procédure comme montré ci-dessous:

![](/upload/validationcomptegmail.jpg)

**:::**

### **2.4 Comment envoyer et recevoir un e-mail ?**

Dans cette partie, il s'agit principalement de guider vos apprenant à comprendre la manière d'envoyer un mail et de faire l'exercice plusieurs fois avec eux.

Une fois votre adresse mail créée connectez-vous en y rentrant sur l'interface de connexion votre adresse mail puis votre mot de passe.

Vous atterirez alors sur l'espace central qui comporte plusieurs options:

* En 1 ce sont les boîtes: boîte de reception, messages envoyés, spam(où atterissent les messages indésirables), corbeille.
* En 2 la visualisation de la boîte mail principale avec en gras les messages non lus.

![](/upload/accueilgmail.png)

**Comment envoyer un message ?**

Cliquez sur la commande Créer un message dans la barre d'outils en haut à gauche. Saisissez l'adresse email de votre correspondant dans la petite zone en face de A : (par exemple, [monsieur.dupont@gmail.com](mailto:monsieur.dupont@gmail.com)).

![](/upload/nouveaumessage.png "Ecrire un message Gmail")

Vous pouvez envoyer votre message à plusieurs destinataires en séparant les adresses email par un point-virgule. Vous pouvez aussi mettre des destinataires en copie carbone (Cc) ou en copie carbone invisible (Cci). Dans ce dernier cas, toutes les adresses inscrites dans ce champs seront cachées.

Remplissez ensuite le champ Objet pour renseigner le destinataire sur la teneur de votre message. Ne l'oubliez pas : vous-même n'appréciez sans doute pas de recevoir un message sans savoir de quoi il retourne ! Il vous reste à rédiger le corps de votre message, puis à cliquer sur Envoyer.

Voici les champs à remplir lors de l'envoi d'un e-mail :

* **De** : c'est votre adresse électronique, la plupart du temps vous n'aurez pas à remplir ce champ car il est généralement défini par le client de messagerie selon vos préférences
* **A** : ce champ correspond à l'adresse électronique du destinataire
* **Objet** : il s'agit du titre que votre destinataire verra lorsqu'il voudra lire le courrier
* **Cc** : cela permet d'envoyer un e-mail à de nombreuses personnes en écrivant leurs adresses respectives séparées par des virgules
* **CCi**: il s'agit d'une simple *Copie Carbone* (Cc) à la différence près que le destinataire ne voit pas dans l'en-tête la liste des personnes en *copie cachée*
* **Corps** : c'est le message de votre courrier

![](/upload/compoemail.png "Composition d'un e-mail sous Gmail")

### 2.5 Inclure une pièce jointe

Comment joindre un document à un email ?

Les pièces jointes permettent d'envoyer aussi bien une photo, qu'un texte ou une vidéo.

Après avoir précisé l'adresse un ou des destinataires, cliquez sur l'icône ne forme d'épingle puis aller chercher votre fichier sur le bureau, dans vos documents, sur votre clé USB ou disque dur externe.

![](/upload/pj1.png "Insertion Pièce Jointe Gmail")

Une fois trouvé, il suffit juste de cliquer sur Ouvrir/Choisir pour mettre le fichier en pièce jointe.

![](/upload/pj2.png "Pièce Jointe Insérée")

### 2.6  Créer une signature

Lorsque l'on se crée un adresse mail professionnelle, il est important d'avoir un signature résumant notre identité et nom principaux moyens de contact.

Tuto création signature sur gmail:

Sur l'Espace central cliquez tout d'abord sur l'icône en forme de rouage (Paramètres) puis sur Afficher tous les Paramètres.

![](/upload/signaturegmail1b.png "Signature Gmail Etape 1")

![](/upload/signaturegmail2b.png "Signature Gmail Etape 2")

Vous tomberez alors sur l'ensemble des Paramètres de la boite Mail. Dans la rubrique Général, en scrollant vers le bas avec la souris, vous pouvez voir l'option Signature, cliquez ensuite sur le bouton Créer la signature.

![](/upload/signaturegmail3b.png "Signature Gmail Etape 3")

Une fenêtre va alors apparaître vous demandant de nommer cette signature, cliquez sur Créer.

![](/upload/signaturegmail4b.png "Signature Gmail Etape 4")

Une fois cela fait, remplissez votre signature dans le cadre qui vient d'apparaître, par exemple avec votre NOM Prénom Adresse Mail Numéro de Téléphone. Vous pouvez choisir d'inclure par défaut ou non la signature à l'écriture de tout nouveau mail en cliquant sur l'option "Valeurs par défaut de la signature" puis le nom donné à votre signature.

![](/upload/signaturegmail5b.png "Signature Gmail Etape 5")

N'oubliez pas d'enregistrer vos modifications en scrollant vers le bas puis en cliquant sur le bouton Enregistrer les modifications !

![](/upload/signaturegmail6b.png "Signature Gmail Etape 6")

::: details Tuto création signature hotmail:

<http://www.pcastuces.com/pratique/astuces/3178.html>

:::

::: warning Intermédiaires et Avancés

### 2.7 Créer et gérer des dossiers

Pourquoi créer des dossier?

Créer des dossiers nous permet de nous organiser dans la gestion de notre boite mail et de trier les mails que l'on souhaite conserver ou pas, ceux qui sont important ou non, en fonction d'un critères que l'on aura choisi.

Pour créer un dossier de rangement sur Gmail, cliquez sur le bouton Libellés dans votre barre d'actions de votre boîte de réception, et cliquez sur Créer un libellé.

![](/upload/libellégmail.png "Creation dossier Gmail")

Donnez un nom pertinent à votre dossier pour mieux vous y retrouver si vous comptez en créer plusieurs. J'ai renommé ce dossier Amis en guise d'exemple.

![](/upload/libellesgmail3.png)

Il ne vous reste plus qu'à remplir votre nouveau dossier avec les mails que vous voulez ranger.

### 2.8 Faire un filtre

Lorsque l'on souhaite qu'un nouvel e-mail se « range » directement dans un sous-dossier/libellé, au lieu qu'il ne se place dans la boîte de réception, il faut pour cela créer un filtre.

::: details Création Filtre sur Gmail

Il faut aller dans la barre de recherche, cliquer sur le bouton à droite en forme de flèche, indiquer par exemple dans *Contient les mots* le mot "emploi", le bouton Créer un filtre sera alors accessible. Ensuite Il faut cocher Appliquer le libellé et indiquer le sous-dossier dans lequel le contenu sera transféré puis cliquer sur le bouton bleu Créer le filtre. Vous pouvez aussi rétro-appliquer ce filtre en cochant l'option *Appliquer également le filtre à* 

![](/upload/options-filtre.jpg "Options Filtre Gmail")

::: details Création Filtre sur Hotmail

à rédiger

:::

:::

### 2.9 Rechercher un e-mail

![](/upload/recherchemail.png "Recherche Email Gmail")

Une barre de recherche permet de rechercher dans ses e-mails, reçus ou envoyés, un ou plusieurs e-mails.

A partir de mots-clés, de dates, du nom d'un ou plusieurs émetteurs ou destinataires, selon qu'il s'agit d'un mail reçu ou envoyé, vous pouvez ainsi retrouver des mails (même supprimés, s'ils n'ont pas été définitivement supprimés, depuis la corbeille).

Il est également possible de faire une recherche plus précise, à partir de certains critères (plusieurs, qui permettent d'affiner les résultats), comme par exemple : le destinataire, la date, s'il y avait une PJ etc. 

![](/upload/rechercheavancée.png "Recherche Avancée Gmail")

## **3. Les usages professionnels de la boite mail**

### **3.1 Écrire un e-mail de façon professionnelle**

Lorsque l'on répond à une offre d'emploi, il s'agit d'écrire un e-mail de façon professionnelle, c'est-à-dire en y mettant certaines formes.

Il a tout d'abord les règles de politesses qui stipulent que l'on commence un e-mail avec un « Madame Unetelle, », « Monsieur Untel, », «Messieurs Untels, », etc, et qu'on le termine avec une formule du type « Cordialement » + sa signature = prénom et nom, au minimum.

Mais au-delà des formules pour introduire un e-mail et pour le « refermer » poliment, il est attendu dans le corps du texte d'un e-mail professionnel, différentes parties, différents moments :

* Une formule de politesse pour introduire l'e-mail: soit « Madame Unetelle », « Monsieur Untel, », ou encore « Bonjour Madame Unetelle, », « Bonjour Monsieur Untel, »… : nommer la personne à qui on envoie l'e-mail montre par ailleurs que l'on sait à qui l'on s'adresse : on n'envoie pas l'e-mail au hasard (d'où, si possible, chercher à savoir à qui l'on adresse sa candidature, par exemple ; est-ce qu'il s'agit d'un.e responsable des ressources humaines ? Est-ce qu'il s'agit d'un.e directeur/rice d'entreprise….?).
* Suit un moment où l'on explique la raison pour laquelle on contacte la personne, il s'agit d'aller en somme à l'essentiel : « Je me permets de vous contacter au sujet de votre offre d'emploi / votre offre n° / votre offre de « intitulé du poste » / votre offre de médiateur numérique à pourvoir au sein de votre entreprise (par exemple)... »
* Suit une phrase pour notifier au destinataire de l'e-mail ce qui peut l'intéresser plus particulièrement, à savoir, votre candidature, le fait qu'en PJ, il va trouver votre CV et votre LM, qui correspondent à l'offre de poste à pourvoir au sein de l'entreprise/la structure qui va prochainement recruter : « Vivement intéressé par les missions de ce poste, je vous fais parvenir mon CV et ma LM ».
* Une phrase de politesse et pour témoigner de sa disponibilité : « Je vous souhaite une bonne réception de ma candidature et me tiens disponible pour un prochain entretien »…
* Une phrase pour refermer l'e-mail envoyé « Bien cordialement », « Cordialement »…
* Signature

Ne pas oublier de bien renseigner l’adresse e-mail du destinataire / attention aux fautes de frappe que l’on pourrait faire et qui feraient que la personne ne recevrait pas l’e-mail

Ne pas oublier de mettre un « objet » : s’il s’agit d’une candidature à une offre, par exemple « Candidature  au poste de « intitulé de poste » \[par ex. médiateur numérique] / offre d’emploi n° + (prénom et) nom » : mettre un objet indique la raison pour laquelle on contacte la personne : un e-mail sans objet de la part de quelqu’un qu’on ne connaît pas aura moins de chance d’être ouvert...


Ne pas oublier de bien insérer la ou les pièces-jointes : le CV et (éventuellement) la lettre de motivation

### **3.2 Écrire un e-mail de motivation**

Un e-mail de motivation peut s'apparenter à une lettre de motivation. Selon le degré de détail, un e-mail de motivation peut même se substituer à une lettre de motvation, que l'on insère généralement en pièce-jointe. Selon Pôle Emploi, cependant, l'e-mail de motivation ne doit pas être un simple copié-collé d'une lettre de motivation. Il vaut mieux préférer l'envoi d'un e-mail de motvation avec en PJ une lettre de motivation… Il faut qu'il soit « concis, efficace et dynamique » ; l'e-mail de motivation, à la différence de la LM n'est pas forcément lu au même moment et avec la même attention de la part du recruteur.

⇒ le but de l'e-mail de motivation = donner envie d'ouvrir les pièces-jointes et donner envie de rencontrer la personne (cela dépend du contenu des pièces-jointes).

⇒ « publicité de vous-même », pitch

Mots-clés qui structurent le contenu de l'e-mail de motivation :

* Formation
* Expériences
* Compétences
* Personnalité

Un e-mail de motivation ne doit pas être trop détaillé, et donc submerger le destinataire d'informations sur les expériences professionnelles, les formations suivies, etc.

**5 astuces pour écrire un bon e-mail de motivation selon Pôle Emploi :**

1 - Soignez la forme

La première information vue par le recruteur est votre adresse e-mail. Pour ne pas donner une mauvaise image de vous, utilisez une adresse sérieuse construite à partir de votre nom et prénom.

Évitez les adresses fantaisistes du type Polo64@email.com, même si vous êtes fiers de venir des Pyrénées-Atlantiques !

Vous envoyez des pièces jointes ? Donnez-leurs des titres précis (ex : CV_Paul_Dubois) et choisissez un format lisible sur tous les supports (privilégiez les PDF).

2- Restez poli

Le mail ne nécessite pas le même formalisme que la lettre de motivation. Les formules de politesse (« Veuillez agréer… ») peuvent être allégées mais elles doivent être présentes, par exemple en terminant par « Cordialement ». Dans le doute, commencez toujours votre mail par un titre de civilité (Madame, Monsieur) suivi du nom de la personne si vous le connaissez.

La formule « Bonjour » peut se justifier pour une candidature dans un milieu start-up, créatif ou associatif. Bien entendu, on oublie les polices de caractères alambiquées, l'utilisation de la couleur dans le texte et les smileys.

3- Accrochez le recruteur

L'objet du mail doit être explicite. Si vous répondez à une offre, le titre et éventuellement la référence de l'annonce doivent apparaître dans l'objet. Dans le cadre d'une candidature spontanée, optez pour un titre percutant mettant votre spécificité en valeur, par exemple : « Candidature : assistante 9 ans d'exp. en ressources humaines » \[= 60 caractères]. Pour s'afficher en entier dans la boite mail de votre interlocuteur, l'objet doit être très court (moins de 60 caractères).

4- Soyez bref et percutant

Un e-mail de motivation n'est pas un copié-collé de votre lettre de motivation. Il est là pour inciter le recruteur à en savoir plus sur vous. Le mail ne doit pas excéder 10 lignes, découpées en paragraphes. Il doit être construit autour de phrases courtes qui mettent en avant vos particularités (expériences notables, points forts, motivation pour le poste). Multipliez l'usage des mots-clés qui font référence à votre métier, à l'entreprise auprès de laquelle vous postulez et/ou tirés de l'annonce.

5- Maîtrisez le temps

Concis, percutant, efficace : un mail de motivation, même court, nécessite de consacrer du temps pour sa rédaction, sa relecture et sa correction. Une fois peaufiné, il ne reste plus qu'à l'envoyer (sans oublier de joindre CV et lettre de motivation !). Là encore, le timing est important : poster un mail de candidature en pleine nuit donnera l'image d'un candidat qui ne sait pas gérer son temps. On évite, aussi, d'envoyer son mail le vendredi ou le week-end pour ne pas le noyer dans le flot de courriers du lundi matin.

Le meilleur moment ? Il dépend des habitudes de travail de votre interlocuteur et du secteur d'activité visé : matinée, milieu d'après-midi… Mais gardez à l'esprit, au final, qu'une bonne candidature trouvera toujours son destinataire ! »

::: tip Sources

* Sur l'histoire de la boite mail

<https://www.arobase.org/>

<https://cours-informatique-gratuit.fr/dictionnaire/messagerie-electronique/>

<https://fr.wiktionary.org/wiki/boite_mail>

<https://www.demainlemail.com/email_alinto/historique/>

* Sur les parts de marchés d'utilisation des messageries

<https://www.leptidigital.fr/webmarketing/parts-de-marche-messageries-boites-mail-11774/>

<https://www.radicati.com/?p=15792>

<https://www.arobase.org/actu/chiffres-email.html>

* 5 astuces issues de Pole Emploi

<https://www.pole-emploi.fr/candidat/vos-recherches/preparer-votre-candidature/5-astuces-pour-ecrire-un-e-mail.html>

:::