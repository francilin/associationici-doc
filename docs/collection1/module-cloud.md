---
title: ☁️ Le Cloud
---
# Le Cloud : Démystifier le nuage

::: tip Modules APTIC

* *16 : Panorama des outils de cloud*
* *24 : Principes et fonctionnement du cloud*
* *51 : Sauvegarder en ligne (dans le cloud)*

• **Résumé :**

L’idée de cet atelier est de tacler la conception du cloud comme un service dématérialisé, écologique et économique pour rappeler les réalités matérielles, énergétiques et politiques qui se cachent derrière. 

Ce sera aussi l’occasion de voir concrètement son fonctionnement et les différents outils qui existent.

• **Nombre d’heures :** 2 heures

• **Publics :** Débutant / intermédiaire / confirmé / GEN

• **Prérequis:** Maîtrise minimum du Français : lu , écrit, parlé. 

Avoir suivi les modules Navigateur de fichier+Navigation Internet.

• **Objectifs de l’atelier**

* Comprendre les principes et fonctionnement du cloud
* Connaître différents outils de cloud
* Savoir gérer ses fichiers dans le cloud
* Connaître l’utilisation professionnelle possible du cloud
* Connaître les impacts du cloud

:::

**Sommaire :**

[[toc]]

## **1. Définition et Histoire**

Objectifs intermédiaires : Resituer chronologiquement le cloud.

Définition:

> Le cloud (ou nuage dans la langue de Molière) est un service de gestion de fichiers ou données à distance, accessible à la demande, en permanence.

Il repose sur l’utilisation de serveurs c’est-à-dire d’ordinateurs un peu spéciaux (plats, sans écran), allumés en permanence, pour fournir le service.

![](/upload/cloud1.jpg "Illustration d'un serveur")

Le cloud est donc un service défini par 4 caractéristiques :

* à la demande,
* pour fonctionner il fait appel à une mutualisation des services,
* ses services sont mesurables,
* et il y a une élasticité des ressources c’est-à-dire qu’on peut augmenter la puissance ou la capacité de stockage à la demande.

Le terme cloud vient des années 90 où il était courant de représenter Internet sous forme d’un nuage dans les schémas. De là est né le mythe d’un Espace virtuel immatériel et énigmatique.

![](/upload/cloud4.png "Illustration fantasmée  du cloud vs Réprésentation réelle")

Or il repose au contraire sur une réalité très concrète qui est que le cloud c’est juste l’ordinateur de quelqu’un d’autre.

Économiquement parlant (car il s’agit avant tout aujourd’hui d’un modèle économique) le service s’est démocratisé en 2002 avec le développement d’Amazon.

Pour faire face aux surplus de connexions Internet pour les fêtes de fin d’année ces derniers avaient en effet massivement investi dans des serveurs.

Or comment les rentabiliser une fois les fêtes terminées ?

C’est là que les ingénieurs d’Amazon eurent l’idée de les louer à d’autres développeurs web.

Le cloud à vocation personnelle s’est quant à lui développé massivement en 2007 avec la fondation par des étudiants de Dropbox.

Le cloud est à l’origine d’une autre invention : le streaming. Invention qui s’est démocratisée via Youtube (2005), Deezer(2007), Netflix(2007) ...

::: warning Intermédiaires et confirmés

## **2. Les caractéristiques**

Si techniquement le cloud n’est pas très compliqué il a tout de même quelques ‘spécificités’:

Il existe donc 3 types de cloud :

* le coud public : pour les particuliers, un opérateur privé leur loue un ‘espace’
* le cloud privé : appartenant à des entreprises
* le cloud hybride : l’entreprise fait appel à un prestataire extérieur pour faire face à un afflux temporaire

Il existe 2 modes de fonctionnement : le synchrone (les fichiers sont gérés en temps réels-utilisation personnelle plutôt) et l’asynchrone(les fichiers ont un temps d’upload et de mise à jour-utilisation professionnelle plutôt). Chaque type a ses avantages et inconvénients !

Enfin il existe 3 usages de cloud :

* IAAS Infrastructure as a Service : l’hébergeur (c’est-à-dire l’entreprise qui offre le service de cloud) fournit des instances OS principalement. L’IAAS est destiné pour les exploitants informatiques.
* PAAS Plateform as a Service : l’hébergeur fournit des instances OS+les composants logiciels intégrés. Destiné pour les développeurs web ou développeurs d’applications.
* SAAS Software as a Service Fournit des applications prêtes à l’emploi s’exécutant sur l’infrastructure. Ex : messagerie en ligne. Destiné aux consommateurs.

Au final la question du cloud est avant tout une question d’usage plus que de technicité !
:::

## **3. Les hébergeurs personnels/professionnels et leurs différences**

Comme dit dans l’étape 2 le cloud fonctionne via des prestataires privés offrant des espaces de calcul/stockage … de fichiers ou données contre de l’argent (ou nos données car si c’est gratuit c’est que c’est toi le produit!).

Certains de ces prestataires ont pour vocation de toucher le grand public (on parle d’hébergeur personnel) et d’autres non (les hébergeurs professionnels).

On peut citer en hébergeur personnel :

DropBox, Google Drive(Google), OneDrive(Microsoft), Apple iCloud pour la gestion de fichiers

Youtube(Google), Dailymotion, Netflix, Disney+, Amazon Prime(Amazon) pour le streaming vidéo

Deezer, Soundclood, Spotify pour le streaming musical

Niveau hébergeur professionnel on peut citer :

\-Office 365(Microsoft)

\-Nextcloud(service de cloud européen basé sur du logiciel libre)

Leurs différences :

![](/upload/tableaudifférence.jpg "Tableau comparatif offres de cloud")

A travers l’analyse de ce tableau on peut voir le développement du modèle freemium où l’on propose quelques Go de stockage gratuits avant de faire payer l’utilisateur mais aussi que la plupart des services sont américains avec des serveurs aux USA ce qui n’est pas sans risque en matière de protection des données. Le contrôle des serveurs signifie un service payant et un serveur local !

## **4. Les principales fonctionnalités**

Là encore le cloud n’est pas très compliqué et se rapproche énormément de l’explorateur de fichiers.

![](/upload/cloud4.jpg "Principales fonctionnalités du cloud")

On y retrouve un ensemble de dossiers ; on visualise des icônes de fichiers et on peut rechercher des fichiers. Je vous renvoie donc sur le module Explorateur de fichiers pour plus de détail.

Toutefois 2 options supplémentaires apparaissent avec le cloud : l’option Charger (ou Upload) qui permet comme son nom l’indique de charger un fichier (ou un dossier) depuis votre ordinateur sur le cloud et l’option Nouveau qui permet de créer directement un fichier sur le cloud sans passer par l’ordinateur.

L’avantage du cloud aussi est de pouvoir ‘facilement’ partager des fichiers en ligne avec d’autres personnes, du moment que celles-ci utilisent les mêmes services.

Pour cela sélectionnez un fichier et cliquer sur le bouton Partager (généralement un + ou une flèche) puis entrez l’adresse de la personne avec qui vous souhaitez le partager. Selon les services vous pourrez permettre à cette personne de modifier ou non le document, y faire des commentaires, juste le lire, le protéger avec un mdp ...

Et c’est tout !

Bien sûr dans les clouds professionnels on peut y rajouter des options comme un calendrier partagé, un calendrier des activités sur le cloud pour savoir qui a fait quoi et quand … mais le principe même du cloud est sa simplicité.

## **5. L’impact du cloud**

Le Cloud pose tout d’abord problème en matière de confidentialité des données transmises dessus. En effet, comme dit auparavant, la plupart des services sont américains ce qui sous-entend un envoi des données sur le sol américain ou dans des locaux américains. Et de fait un accès par le gouvernement américain.

Sans tomber dans les théories du complot cet accès a été confirmé par les révélations d’Edward Snowden mettant en lumière la collaboration des entreprises numériques avec les services de renseignement américains suite aux attentats du 11 septembre et des lois votées après (notamment via le logiciel PRISM).

![](/upload/cloud5.jpg "Le programme Prism")

De plus suite au récent Cloud Act voté en 2018 par Donald Trump permettant l’accès aux autorités aux données hébergées sur le cloud des personnes suspectées de crime cette position est désormais ouvertement affirmée et ce malgré le vote par l’Union Européenne de lois de protection comme le RGPD.

Cf. Cours sur l’Histoire des Lois de l’Informatique.

Ainsi l’utilisation du cloud n’est pas neutre et il est recommandé à toute entreprise ou particulier ayant des données sensibles de mettre en place son propre serveur local.

L’autre principal défaut du cloud est son coût écologique désastreux.

En effet, comme dit précédemment le cloud est un service qui par définition fonctionne en permanence.

La simple présence disons d’une photo de chat consomme de l’électricité car devant être accessible en permanence, même quand vous ne l’utilisez pas.

Or qui dit ordinateur en fonctionnement dit surchauffe et donc refroidissement nécessaire (50 % de la facture d’électricité d’un data center partirait dedans). Ce refroidissement est aujourd’hui un peu limité par l’installation de serveurs dans les pays nordiques (free cooling) mais là encore la solution n’est pas parfaite.

Le streaming, technologie qui comme dit est dérivée du cloud, est un non-sens absolu d’un point de vue écologique : on télécharge (et donc consomme de l’énergie pour) un fichier placé dans un répertoire temporaire qui sera supprimé à l’extinction de l’ordinateur, de la box ou de l’application. C’est donc un gâchis similaire aux couverts en plastique par exemple.

De plus d’après Sandivine (société spécialisée dans les équipements réseau) le streaming vidéo a capté 56 % du trafic web mondial dont 15% rien que pour Netflix !

Or ce trafic web il repose sur des câbles physiques et donc derrière de la matière première dont des métaux dits rares dont l’exploitation est extrêmement polluante et problématique. Cf.article

De plus ce trafic se fait au détriment d’autres usages d’Internet.

Que faire alors à son niveau ?

* Déjà limiter les fichiers sur le cloud aux fichiers non sensibles, sans donnée personnelle, utilisés régulièrement par plusieurs personnes.
* Supprimer les fichiers déjà sur le cloud ne répondant pas à cette règle.
* Privilégier les services locaux, quitte à payer.
* Supprimer ses vieux mails avec pièce jointe et limiter ceux-ci au maximum désormais.
* Limiter sa consommation des services streaming : pour Netflix privilégier l’achat ou la location de films (ou à défaut des services moins polluants), pour Youtube limiter la qualité des vidéos, pour Deezer/Spotify le téléchargement direct de musique (légal de préférence).
* De plus là encore privilégier l’usage collectif à l’usage personnel des écrans.

Exercice: <https://learningapps.org/watch?v=p4yb1kafj19>

::: tip Sources

<https://www.sciencesetavenir.fr/high-tech/informatique/numerique-et-ecologie-les-data-centers-des-gouffres-energetiques_121838>

<https://www.greenpeace.fr/il-est-temps-de-renouveler-internet/>

<https://www.cnetfrance.fr/news/netflix-represente-15-de-la-bande-passante-mondiale-39874513.htm>

:::