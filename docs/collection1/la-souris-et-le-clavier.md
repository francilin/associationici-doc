---
title: "🖱️⌨️  La Souris et le Clavier "
---
# La souris et le clavier

::: tip APTIC?

* **Résumé**

Cet atelier a pour but d'apprendre aux participants les principes de base de l'utilisation d'un clavier et d'une souris.

* **Nombre d'heures:** 3h
* **Publics :** Grands débutants/débutant
* **Prérequis:**

Maîtrise minimum du Français : lu , écrit, parlé .

* **Objectifs de l'atelier :**

  * Savoir déplacer le curseur de la souris, connaître les boutons, se familiariser avec les différents clic
  * Connaître les différentes zones du clavier, commencer la maîtrise du clavier, taper un texte en utilisant les différents types de caractères.

:::

**Sommaire :**

[[toc]]

## 1. Maîtrise de la souris

### 1. Présentation de la souris

La souris est un outils indispensable pour utiliser un ordinateur.

Elle sélectionne, déplace, manipule des éléments présents dans votre ordinateur.

Elle est constituée de deux boutons :

![](/upload/souris1.jpg)

* Le clic gauche : le clic simple, permet de sélectionner un objet.
* double-clic : 2 appuis bref sur le bouton, permet d'ouvrir un élément que vous avez sélectionné.

C'est le clic le plus utilisé.

* **Le clic droit:**

il sert à afficher le menu contextuel. Il est différent en fonction de l'endroit ou on se trouve.

* **La molette:**

Elle permet de se déplacer dans les pages internet verticalement.

*Lien pour s'entraîner à manipuler la souris:*

<http://carrefour-numerique.cite-sciences.fr/ressources/flash/anims/cyberbase05/souris_home.swf>

### 2. Les différents pointeurs de la souris:

*Ici vous pouvez présenter les principaux pointeurs de souris*

![](/upload/souris2.png)

### 3. Comment utiliser la souris ?

*Les principales notions à voir :*

**Pointer** : placer le pointeur de la souris sur ou à côté d'un élément de texte ou d'un objet.

**Cliquer** : placer le pointeur de la souris à l'endroit voulu, appuyer et relâcher le bouton gauche ou droit de la souris.

**Double-cliquer** : placer le pointeur de la souris à l'endroit voulu, appuyer et relâcher deux fois de suite rapidement le bouton gauche de la souris.

**Faire glisser** : placer le pointeur de la souris à l'endroit voulu, cliquer le bouton gauche de la souris, le maintenir enfoncé en dirigeant la souris à l'emplacement désiré et relâcher. L'action de glisser permet d'étendre une sélection ou de déplacer un objet.

**Positionner le curseur** : pour positionner le curseur à un emplacement, effectuer un simple clic à l'endroit voulu. Le curseur représente le point d'insertion dans un document. Ne pas confondre le curseur avec le pointeur. En effet, le pointeur de la souris est l'élément qui se déplace sur l'écran et qui change d'apparence en fonction de l'endroit où il est positionné. Lorsque le curseur est positionné sur une cellule cette dernière est sélectionnée. Elle est encadrée ce qui indique l'emplacement où l'action va être exécutée. Lorsque le curseur est positionné à l'intérieur d'une cellule il est représenté par une petite barre qui clignote comme dans Word. Pour modifier le contenu d'une cellule il n'est pas nécessaire de tout ressaisir. Il suffit de positionner le curseur à l'emplacement voulu à l'intérieur de la cellule.

**Passer la souris** : l'action de passer la souris sur les icônes sans cliquer permet de faire apparaître une info-bulle d'information qui explique la commande. Depuis la version 2007 d'Excel, pour certaines fonctions, cette action permet également d'obtenir un aperçu du résultat qui sera obtenu en cliquant.

**La roulette :** la roulette de la souris permet de faire défiler les documents ou les informations dans une fenêtre sans utiliser les ascenseurs. Elle permet également d'augmenter ou de diminuer le zoom. En combinant la touche Ctrl avec la roulette de la souris l'affichage sera agrandi ou diminué suivant le sens dans lequel elle sera actionnée.

### 4. Jeux pour apprendre à se servir de la souris

*Ici une sélection de jeux permettant aux apprenants à se servir de la souris et apprendre les bases de sa maîtrise.*

Lien: <http://lasouris.weebly.com/exercices-simples.html>*(jeux à faire le quadroku)*

Lien: <http://www.bartbonte.com/meandthekey/>

Lien: <http://www.ferryhalim.com/orisinal/> compilation de mini jeux

Lien: <http://valnys.free.fr/amdunet/speed.swf>*(jeux flash basé sur la rapidité)*

## 2. Présentation et maîtrise du clavier

Le clavier sert à donner des instructions à l'ordinateur. Il nous sert d'interface avec la machine qu'est l'ordinateur et nous permet d'y entrer des commandes plus ou moins complexes.

::: details Remarque :

Ne pas hésiter à montrer un vrai clavier

:::

La disposition des touches du clavier nous vient de nos anciennes machines à écrire mécaniques (contraintes de fabrication et fonctionnement). Et cette disposition de ces mêmes touches dépend aussi du pays… En France, on parle de clavier « Azerty », dans les pays anglophones, on parle de clavier « Qwerty » (disposition des touches de la première rangée en haut).

Le clavier d'ordinateur portable peut posséder ou non le pavé numérique. La présence de celui-ci dépend de la taille de l'écran

![](/upload/clavier.png)

### 1. Présentation générale du clavier

La répartition des touches n'est pas anodine, elles sont regroupées par thème.

**En bleu :** Les touches les plus utilisées, notamment pour la frappe de documents : lettres, chiffres, signes de ponctuation, majuscules, minuscules…

**En jaune clair :** La touche Entrée présente en deux endroits, pour plus de commodité.

**En jaune plus foncé :** Les touches de fonction F1 à F12 (dépendantes du logiciel utilisé).

**En violet :** Le « pavé numérique », pour une saisie facilitée pour les chiffres et les symboles de calcul.

Attention ! Pour cela, le pavé numérique doit être activé (touche Verr.Num en haut à gauche de ce pavé).

**En vert :** Les flèches de direction, pour déplacer le curseur vers le haut, bas, droite, gauche.

**En rose :** Les touches « Windows » qui permettent de lancer des menus Windows directement.

**En rose :** Une touche permettant d'appeler le menu contextuel dans le cas général. Équivalent à un clic droit de la souris.

**En blanc :** Trois touches en haut « Système » ; six touches permettant l'accès à certaines fonctionnalités bien pratiques ; enfin la touche Echap ou Esc en haut à gauche est synonyme d'abandon en règle générale.

**Les voyants** : En général, ils indiquent :

* le mode majuscule activé,
* le mode numérique activé,

### 2. Les touches du clavier et leur fonctionnalité

* **Les lettres :**

Dans cette zone qui est la zone principale et centrale du clavier, nous retrouvons les 26 lettres de notre alphabet. Cette zone permet de saisir tous les caractères non accentués.

* **Les touches Entrée :**

Un clavier possédant un pavé numérique possède 2 touches « Entrée ». Une première située tout à droite de la zone des lettres et qui correspond à la plus grosse touche du clavier. La seconde est positionnée au niveau du pavé numérique.

Les fonctions principales de la touche « Entrée » sont :

* Passer à la ligne lorsque vous écrivez un texte dans un traitement de texte,
* Exécuter une action,
* Valider la saisie d'un formulaire sans avoir à cliquer sur le bouton adéquate.
* **Les touches de modification :**

Dans cette zone, nous trouvons 4 touches :

* La touche «**Verrouillage Majuscule**»

Aussi nommée caps lock, sur laquelle on retrouve souvent une image représentant un cadenas.

Si vous appuyez une fois sur cette touche, vous passez en mode majuscule.

Toutes les lettres que l'on tape seront écrites à l'écran en majuscule. Lorsque l'on appuie sur cette touche, une petite LED s'allume sur le clavier.

Il faut appuyer à nouveau sur cette touche pour repasser en minuscule, et éteindre la petite LED.

* La touche « **Majuscule »**

Les 2 touches « Majuscule », aussi appelées touches «**maj»**  ou « **shift »** , se situent de chaque côté du clavier. Il s'agit des touches sur lesquelles est dessinée une grosse flèche,

Cette touche permet également de saisir des caractères en majuscule. Mais contrairement à la touche «**Verrouillage Majuscule**», il faut maintenir enfoncée cette touche et appuyer en même temps sur la touche que l'on souhaite mettre en majuscule.

::: details Remarque :

*Afin de rendre la manipulation plus compréhensible, ne pas hésiter à la faire exécuter par les apprenants après l'explication*

:::

* La touche **« Alt Gr »**

Cette touche permet d'obtenir des caractères spéciaux

Qu'est ce qu'un caractère spécial ? 

(ajouter capture/image)

Ce sont les touches qui se situent au-dessus et à droite de la zone des lettres. Sur ces touches vous pouvez remarquer plusieurs caractères sur une même touche. Prenons par exemple la touche qui contient le chiffre « 4 ». Sur cette même touche, nous pouvons voir les caractères « ' », « { » et le chiffre « 4 ». C'est ici que les touches de modification vont avoir toute leur utilité.Pour obtenir le caractère « ' », il faut être en minuscule et appuyer sur la touche contenant le chiffre « 4 ».Pour obtenir le caractère « { », il faut appuyer maintenir la touche « Alt GR » enfoncée puis avec votre autre main ou un autre doigt, appuyez sur la touche contenant le chiffre « 4 ».Enfin pour obtenir le chiffre « 4 », il faut être en majuscule, soit en utilisant la touche « Verrouillage Majuscule » ou en maintenant enfoncée la touche « Majuscule » tout en appuyant sur la touche qui contient le chiffre « 4 », de la même façon que précédemment avec la touche « Alt GR« .
Pour l'utilisation des touches « Majuscule » et « Alt GR », la bonne pratique est d'abord d'appuyer sur l'une de ces deux touches, restez appuyer, puis ensuite appuyer sur la touche qui contient le caractère que l'on souhaite afficher.
_(Remarque : Afin de rendre la manipulation plus compréhensible, ne pas hésiter à la faire exécuter par les apprenants après l'explication)

**L'accent circonflexe :** L'accent circonflexe « **^** » est un des accents les plus compliqué à trouver sur le clavier.
Voici comment faire pour le mettre sur une voyelle : _(Remarque : ici nous prenons l'exemple de la lettre « **e** »)_Pressez la touche avec l'accent circonflexe,Relâchez la touche,Presser la touche e du clavier

**Pour l'accent tréma :** réitérer ces dernières étapes en étant en majuscule.
*Remarque : Dans cette zone vous pouvez remarquer tout à gauche une touche avec un petit caractère « 2 ». Cette dernière n'a pas trop d'utilité à part lorsqu'il faut des terminologies mathématiques ou des mètres carrés* 

**La Ponctuation**

Cette zone comprend deux types de touches :

* Les touches avec plusieurs caractères sur lesquelles on retrouve des caractères de ponctuation (par exempte le point, les deux points …)

Pour obtenir les ponctuations, il faut retenir que :

* les caractères du bas de la touche s'obtiennent quand on est en minuscule.
* les caractères du haut de la touche s'obtiennent lorsque l'on passe en majuscule soit par « Verrouillage Majuscule » ou la touche « Majuscule ».
* Une touche d'espacement, communément appelée « Espace »

La touche « Espace » (grande barre en bas du clavier) sert comme son nom l'indique à positionner un espace entre deux mots.

* **Les touches de suppression**

Les touches de suppression servent à effacer un texte lorsque l'on fait une erreur. Il existe sur le clavier deux touches pour corriger un texte :

* Une touche appelée « **Backspace** », représenté par une flèche qui se dirige vers la gauche, située juste au dessus de la touche « Entrée« . Lorsque vous appuyez sur celle-ci, le caractère situé à gauche du curseur est effacé.
* Une touche **« Suppr »** qui lorsqu'on appuie dessus supprime le caractère qui se trouve à droite du curseur.
* **Les touches de déplacement**

On retrouve sur le clavier différentes touches vous permettant de se déplacer dans un document ou dans des pages Internet.

On y trouve :

* Les flèches haut, bas, gauche et droite : il s'agit des touches de direction
* Les touches « **PageUp»** et « **PageDn»** permettant de naviguer de page par page. Ce sont les deux grosses flèches Haut et bas. Elles sont pratiques pour faire défiler un document ou une page Internet.
* Une touche permettant de revenir en début de ligne ou d'un document. Il s'agit la flèche oblique qui pointe vers la gauche.
* Puis une touche permettant d'accéder à la fin de la ligne ou d'un document. Il s'agit de la touche « **Fin** ».

Les flèches sont souvent utilisées lorsque l'on écrit un document sur un traitement de texte comme Word. Elles permettent de naviguer de lettre en lettre aussi de gauche à droite que de bas en haut. Sur Internet, vous pouvez utiliser les flèche haut et bas pour faire défiler la page que vous consultez. les autres sont plus anecdotiques.

Les autres touches sont moins utilisées, elles servent principalement lorsque l'on écrit des texte pour se déplacer plus rapidement dans un document.

* **Le pavé numérique**

Si il y a un pavé numérique sur le clavier, il est possible alors de saisir directement les chiffres.

Le pavé numérique contient également les caractères « **.** », « **+** », « **–** », « **\*** » et « / » utiles par exemple lorsque l'on utilise l'application Calculatrice.

La touche **« Verr num »** permet de verrouiller ou déverrouiller le pavé numérique.

Dans le cas où le pavé numérique est désactivé, on peut avoir accès aux fonctions mentionnées en bas de chaque touche. Par exemple, la touche « **.** » devient une touche « **Suppr** ».

::: details Remarques :

Certains ordinateurs ont, au démarrage, le pavé numérique déverrouillé par défaut. N'oubliez pas de le verrouiller pour saisir votre mot de passe.

:::

* **La touche Tabulation**

Cette touche, aussi nommée touche tab, vous permet de mettre en forme vos documents.

Dans un traitement de texte comme word ou openoffice, elle permet de placer un espace assez conséquent au début des paragraphes.

::: Remarques

Elle possède une autre utilité lorsque vous saisissez un formulaire, par exemple sur Internet. Lorsque vous appuyez sur cette touche, vous passez au champ de saisie suivant.

:::

* **La touche Echap**

Cette touche est utilisée pour annuler une tâche en cours ou quitter une fenêtre.

* **Les touches Windows**

Sur les claviers standards on retrouve 2 touches « **Windows** », une de chaque côté de la barre d'espace.

Ces deux touches servent à afficher le menu Windows du bouton « **Démarrer** ».

Elles sont utilisées également dans le cadre des raccourcis clavier.

Par exemple, si vous appuyez simultanément sur les touches « **Windows** » et la lettre « **L** », votre ordinateur se verrouille.

* **La touche contextuelle**

Cette touche se trouve en bas à droite de votre clavier. Son action est équivalente au clic droit de votre souris.

Par exemple, si vous sélectionnez une icône sur votre bureau et que vous appuyez sur cette touche, un menu contextuel apparaît. Celui-ci est identique au menu que vous obtenez en effectuant un clic droit sur cette même icône.

* **Les touches de raccourcis**

Il existe sur un clavier, trois touches de raccourcis :

* 2 touches « **Ctrl»** , appelées «  **Contrôle »**
* 1 touche « **Alt»**

Comme leur nom l'indique, elles servent uniquement dans le cadre de raccourcis clavier. Un raccourci clavier est tout simplement une combinaison de touches.

Ces raccourcis clavier ont pour objet de nous permettre de travailler beaucoup plus facilement et rapidement.

L'utilisation des raccourcis clavier consiste à appuyer simultanément sur deux touches du clavier.

Par exemple, pour utiliser le raccourci « **Ctrl+C»** qui effectue une copie, pressez sur la première touche du raccourci (Ctrl) puis tout en la maintenant enfoncée appuyez sur la seconde (C).

::: details Les raccourcis les plus courants

**CTRL+A :** pour sélectionner l'ensemble d'une page **CTRL+X :** pour couper la sélection
**CTRL+C**  : pour copier la sélection
**CTRL+V :** pour coller la sélection **CTRL + ALT+ SUPPR :** pouraccéder au gestionnaire des tâches pour arrêter un programme qui ne répond plus et résoudre le plantage **ALT + TAB**  : basculer entre les différentes applications ouvertes

:::

* **Les touches de fonction**

::: details Remarque:

Ces touches ne sont pas indispensables au quotidien.

:::

Certaines servent pour effectuer des raccourcis. Par exemple, la touche « **Alt** » + la touche « **F4** » ferme l'application en cours.

Sur Internet, la touche « **F5** » permet de rafraîchir la page Internet, c'est-à-dire, recharger la page pour la mettre à jour.

* **Mode insertion ou ré-écriture**

Cette touche « **inser** » peut être utilisée dans certain traitement de texte.

Lorsque l'on appuie dessus, la saisie de nouveaux caractères remplace les précédents.

::: details Remarque: 

Cette touche est très peu utilisée au quotidien

:::

* **Les « Autres » touches du clavier**

Cette zone comprend trois touches :

* La touche **«Impr écran»** :

Elle permet de faire une capture d'écran et de copier cette dernière dans un logiciel comme Paint

* La touche **« Arrêt défil»:**

Un cas d'utilisation pour la touche Arrêt défil est de pouvoir naviguer dans un tableur (Excel par exemple) sans que le cellule sélectionnée change

* La touche **« Pause»:**

Elle peut être utilisée dans certains jeux pour mettre en pause une partie ou pour mettre en pause une vidéo.

::: details Remarque:

Ces deux touches sont utiles pour les développeurs informatiques. Elles ne sont pas utiles pour un usage classique d'un ordinateur.

:::

### 3. Mise en application des des jeux et exercices

Les jeux pour apprendre à se servir du clavier:

<http://www.pctap.net/pctap3/pctap.php>

(Inscription en ligne, enregistrement **gratuit**. Une fois que vous aurez amélioré votre niveau, environ 85 % de précision (équivalent à peu de fautes de frappe) et une vitesse de frappe de 15 mots par minute (ou 90 caractères par minute), PC Tap vous proposera d'acheter la version avancée du logiciel.)

<https://www.ticken.fr/Jeux-Dactylographie.html>

(jeux disponibles: la cueillette d'oeuf et le requin)

::: tip Sources

<http://ordi-senior.fr/Le_clavier.php>

<https://www.toutimages.com/clavier.htm>

<https://www.lecoinretraite.fr/maitrise-du-clavier/>

<https://cours-informatique-gratuit.fr/cours/apprendre-a-taper-sur-un-clavier-partie-3>

<https://cours-informatique-gratuit.fr/wp-content/uploads/2014/05/position-mains-dactylo.jpg>

:::