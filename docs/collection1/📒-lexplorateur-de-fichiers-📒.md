---
title: 📒 L'Explorateur de fichiers
---
# L'Explorateur de fichiers

::: tip Modules APTIC

* *50 : Organiser, explorer et  partager des contenus numériques*
* **Format :** 2H (Débutant) à 3H (Grand Débutant)
* **Publics** : Grand Débutant/Débutant
* **Résumé :** Présenter l'explorateur & son rôle dans l'ordinateur. Découverte des options de l'Explorateur
* **Objectifs de la formation :**

  * Comprendre ce qu'est un Explorateur de fichier, un Fichier &amp; un Dossier
  * Savoir naviguer dans une arborescence de dossier
  * Utiliser l'Explorateur de fichier
  * Créer un fichier/dossier
  * Modifier un fichier/dossier (renommer, supprimer, ect...)
  * Utiliser les bibliothèques
* **Prérequis :**

  * Hardware
  * Clavier & souris
  * Bureau

:::

**Sommaire:**

[[toc]] 

## **1. Présentation de l'Explorateur de Fichiers**

### **1. Introduction**

**Définitions**

**Fichier**
Un fichier est ce que vous produisez avec l'ordinateur : un courrier, une feuille de calcul, la fiche d'un contact, etc. Si ces éléments étaient en papier, vous les rangeriez dans un dossier en carton. En informatique, c'est pareil : vous créez des dossiers dans lesquels vous classez vos fichiers. Notez qu'il existe différentes sortes de fichiers : les fichiers systèmes sont ceux qui servent à faire fonctionner l'ordinateur. Ils se trouvent hors de vos dossiers de travail habituels et vous ne devez pas y toucher. Les fichiers exécutables démarrent en double-cliquant dessus ; ce sont ceux de vos applications, et aussi les fichiers d'installation.

**Dossier**
Un dossier est un réceptacle qui contient divers éléments : des fichiers et des sous-dossiers.

**Sous-dossier**
Pour éviter la pagaille dans un dossier en carton, vous classez vos documents dans des chemises en papier épais que vous placez dans le dossier. C'est pareil avec l'ordinateur : un dossier peut contenir autant de sous-dossiers que vous le désirez. Dans le Bureau de Windows comme avec un vrai bureau, c'est vous qui rangez.

**L'Explorateur de fichiers**
 Egalement appelé le Poste de travail, l'Explorateur de fichiers permet d'accéder à l'ensemble des fichiers d'un ordinateur. Il permet également d'accéder à tous les lecteurs et périphériques branchés : disque dur, disque dur externe, lecteur CD/DVD, clé usb, carte SD, smartphone…

**L'Explorateur permet de gérer l'ensemble de ses fichiers : les déplacer, les copier, les supprimer, les renommer, les rechercher, les trier et les organiser.**

Enfin il existe plusieurs façon d'ouvrir l'Explorateur:

\-En cliquant sur la Barre des Tâches

\-En Cliquant sur Ce PC depuis le bureau

![](/upload/explorateur1.jpg "Accéder à l'explorateur")

### **2. Découverte de l'Explorateur**

Nous allons nous basé sur l'Explorateur sur Windows celui ci étant le plus largement utilisé avant de revenir sur les légères différentes sur les autres Systèmes d'Exploitations.

Pour faciliter les explications, l'explorateur Windows a été découpés en plusieurs zones.

#### **a. Le Panneau latéral**

Le panneau latéral est composé de raccourcis qui permettent d'atteindre des endroits stratégiques sur votre ordinateur.
 Les raccourcis du panneau latéral sont classés dans **5 catégories différentes** :

* **Favoris** : cette catégorie est personnalisable et **contient des raccourcis très utiles**. Un seul clic sur le raccourci « Bureau » affiche par exemple directement tout le contenu de votre bureau.
* **Bibliothèques** : cette catégorie **réunit l'ensemble de vos fichiers personnels** (documents, images, musique, vidéos)
* **Groupe résidentiel** : permet d'afficher les ordinateurs au sein de votre groupe résidentiel. Cette fonction est très utile pour accéder aux fichiers d'un autre ordinateur connecté sur Windows.
* **Ordinateur** : cette catégorie contient les raccourcis de **tous les lecteurs** (disques durs, CD-Rom, DVD-Rom, Blue-Ray, cartes mémoires SD, iPhone, lecteurs MP3, …) **branchés à votre ordinateur**.
* **Réseau** : ce raccourci permet d'afficher les périphériques connectés au même réseau que votre ordinateur. Comme le groupe résidentiel, cette fonction est utile pour accéder aux fichiers d'un autre ordinateur. Contrairement au groupe résidentiel, vous pouvez également accéder à des ordinateurs qui fonctionnent avec Mac OS ou Linux via la fonction Réseau.

#### **b. La Zone centrale**

Cette zone affiche le contenu courant, autrement dit elle affiche le contenu du dossier dans lequel vous êtes avec tous fichier dedans.

#### **c. La Barre d'adresse**

La barre d'adresse est **comme un GPS** :elle indique toujours votre emplacement actuel. Autrement dit elle affiche l'arborescence du dossier actuel.
 On peut aussi s'en servir pour se déplacer dans l'arborescence en appuyant sur le dossier **Thomas** je vais sortir du dossier **Mes Documents** et aller dans le dossier **Thomas.**

#### **d. Les flèches de navigation**

Les flèches de navigation permettent de **revenir au dossier où vous étiez ou d'avancer au dossier d'où vous venez**. En fait, les flèches de navigation fonctionnent exactement comme les boutons « suivant » et « précédent » d'un lecteur MP3.

#### **e. La barre de recherche**

La barre de recherche permet comme son nom l'indique de rechercher du contenu dans le dossier.

**Attention:** bien que similaire à la barre de recherche du menu démarrer de Windows(Voir fiche sur le bureau) celle-ci cherche seulement dans le dossier courant (et donc l'ensemble des dossiers et sous-dossiers que le dossier courant contient).

#### **f. La Barre d'outils**

La barre d'outils permet de **gérer les dossiers et les fichiers de votre ordinateur**.

La barre d'outils permet notamment:

* Créer un nouveau dossier
* Renommer des fichiers et des dossiers
* Supprimer des fichiers et des dossiers
* Déplacer des fichiers et des dossiers
* Dupliquer des fichiers et des dossiers
* Modifier l'affichage de la zone centrale

## **2. Utiliser l'Explorateur**

### **1. Opérations de Base ~ Créer, Modfier & Supprimer**

#### **a. Modifier l'affichage**

Il est possible de modifier l'affichage visuel que windows utilise pour afficher les fichiers.

Pour ce faire vous avez deux choix :

\-En allant dans l'onglet **Affichage** du **Ruban** vu précedemment :

![](/upload/ruban1.png "Modifier l'affichage")

\-Cliquez avec le bouton droit de la souris sur le bureau et vous arriverez à ce menu:

![](/upload/ruban2.png "Modifier l'affichage 2ème option")

Vous pouvez maintenant décider du format d'affichage de vos fichiers.
 En icônes plus ou moins grande, utile pour avoir un aperçu visuel d'un fichier (comme d'une image par exemple).
 Les modes détails, liste &amp; mosaiques sont pratique pour différencier les fichiers et avoir leurs détails(comme leurs formats ou leurs tailles par exemple).

#### **b. L'option «Trier par»**

A rédiger

![](/upload/trierpar.png "Trier par")

#### **c. Créer un fichier**

1-Cliquez avec le bouton droit de la souris sur le bureau et vous arriverez à ce menu:

![](/upload/creer-nouveau-dossier-windows.png "Creation Nouveau Dossier")

2- Dans le menu , choisissez Nouveau; Dossier.
 Un dossier nommé Nouveau dossier est créé.

3- Saisissez un nom pour ce dossier (par exemple : Compta, ou Photos de vacances, ou ce que vous voudrez). Essayer de donner un titre aux dossiers qui rende le contenu à l'intérieur clair.

Les caractères suivants ne peuvent pas être utilisés

![](/upload/erreurnomdefichier.jpeg "Erreur nom de fichier")

#### **d. Modifier un fichier**

##### **Renommer**

Si vous souhaitez modifier le nom d'un fichier ou d'un dossier, cliquez sur le fichier ou le dossier à renommer pour le sélectionner. Il doit apparaître sous fond bleu.
 Appuyez ensuite une fois sur la touche **F2** de votre clavier (elle se trouve tout en haut vers la gauche du clavier). Enfin, **saisissez le titre souhaité** pour le fichier ou le dossier.
 L'ancien titre disparaîtra automatiquement dès que vous commencerez à écrire.

![](/upload/renommer.png "Renommer un fichier")

##### **Déplacer des fichiers**

Pour déplacer des éléments dans un autre dossier , commencez par sélectionner les fichiers et/ou dossiers à déplacer (rappel : les fichiers/dossiers sélectionnés doivent apparaître sous fond bleu). Dans la barre d'outils de l'explorateur Windows, cliquez ensuite sur le bouton **Organiser** puis sur **Couper**.

Ouvrez à présent le dossier dans lequel vous souhaitez déplacer les éléments sélectionnés précédemment. Cliquez à nouveau sur le bouton **Organiser** de la barre d'outils puis cliquez sur **Coller**.

Si le bouton **Coller** est grisé, c'est que vous n'avez pas cliqué sur le bouton **Couper** avant.
 Rappel: A la différence de **Copier** , **Couper** ne fait pas de copie du fichier mais déplace le fichier jusqu'au **Coller.**

![](/upload/déplacerdesfichiers.png "Déplacer des fichiers")

#### **e. Supprimer un fichier**

Il existe deux façons de supprimer un fichier:

* **Dans l'Explorateur de fichiers,**  cliquez sur le bouton droit sur le dossier ou le fichier à supprimer et, dans le menu, choisissez Supprimer. Windows affiche un panneau vous demandant de confirmer la suppression.
* **Dans le panneau Supprimer le fichier** , cliquez sur le bouton Oui. Le dossier ou le fichier est envoyé dans la Corbeille.

![](/upload/suppressionfichier.png "Suppression Fichiers")

Par précaution contre les mauvaises manipulations, tout les fichiers supprimer sont envoyer à la **Corbeille.**
Une sorte de dossiers de sauvegarde pour éventuellement récupérer ces fichiers ou les supprimer définitivement.

**Sur le Bureau,** double-cliquez sur l'icône Corbeille. La fenêtre de la Corbeille s'ouvre.
 Vous pouvez ensuite exécutez les différentes actions:

* **Vider la Corbeille**

Cliquez sur le bouton Vider la Corbeille, à droite dans le ruban. Il vous est demandé confirmation car cette opération est irréversible. Après avoir cliqué sur Oui, le contenu de la Corbeille est définitivement effacé.

* **Récupérer manuellement un élément**
  Faites-le glisser de la Corbeille vers le Bureau ou jusqu'à un dossier.
* **Replacer automatiquement un élément**  là où il était
  Sélectionnez un ou plusieurs éléments puis cliquez sur l'icône Restaurer les éléments sélectionnés. Le ou les éléments sont aussitôt déplacés là où ils se trouvaient au moment où ils avaient été supprimés.

Pour afficher en haut de la liste les derniers éléments supprimés, et les localiser ainsi plus facilement, affichez la Corbeille en mode Liste.

![](/upload/corbeillerestauration.jpg "Restaurer un fichier depuis la corbeille")

Puis cliquez sur l'icône de droite, tout en bas à gauche, puis cliquez sur l'en-tête de la colonne **Date de suppression**. Les fichiers seront ensuite triés par ordre chronologique.

### **2. La Recherche**

Située en haut à droite de la fenêtre de l'explorateur, la zone de recherche intitulée **Rechercher dans** se révèle particulièrement efficace. Elle offre la possibilité d'effectuer des recherches directement dans un dossier (Ce PC, Documents ect...) ainsi que ses éventuels sous-dossiers.
 Sélectionnez un dossier, puis saisissez un mot-clé dans le champ de recherche pour lancer une requête. Les résultats sont délivrés à une vitesse impressionnante. Il est possible d'affiner une recherche en jouant avec les options du menu **Outils de recherche** dans le ruban. Elles permettent de cibler et restreindre les recherches par type de fichiers (images, contacts, emails...), leur date de modification ou encore leur taille. Une fonction vraiment pratique pour trouver rapidement des contenus...

![](/upload/recherchefichiers.jpg "Recherche Fichiers")

### **3. Le Ruban**

Attardons nous ensuite sur le ruban (en haut de l'en-tête de notre dossier)qui est composée de 4 onglets:

#### **a. L'onglet Fichier**

Avec l'onglet Fichier, vous pouvez ouvrir une nouvelle fenêtre ou modifier les options des dossiers de recherche

![](/upload/rubanfichiers.png "Ruban Fichiers")

Dans la partie haute de droite de l'Explorateur se situent les dossiers récents en cours de consultation. Notez également la présence d'une barre d'adresse et d'un cadre de recherche, qui permet de naviguer instantanément d'un répertoire à l'autre.

#### **b. L'onglet Accueil**

Avec l'onglet Accueil, vous pouvez sélectionner, afficher & modifier les propriétés d'un fichier ou même créer un nouveau répertoire (ou dossier).

Notamment parmi les options de modification on a :

##### **Presse-papiers**

\-Épingler.
 Mettre l'élément sélectionner en accès rapide(équivalent à le mettre en favoris, il sera affiché directement sur la Barre des tâches).

\-Copier/Coller

Fait une copie du fichier. Puis il faudra le coller (sur le dossier courant ou ailleurs)

##### **Organiser**

\-Déplacer Vers

Similaire à coller mais sans faire de copie.

\-Copier vers

Fait une copie & l'envoie ailleurs dans l'arborescence de votre choix

\-Supprimer

Envoie le fichier dans la corbeille

##### **Renommer**

Permet de modifier le nom de l'élément.

##### **Nouveau**

Permet de créer un nouveau dossier

##### **Ouvrir**

\-Propriétés.

Afficher et modifier les propriétés d'un fichier. Permet de prendre connaissance de données comme la taille, le format la dates de modifications ou même les autorisations du fichier

##### **Sélectionner**

\-Sélectionner tout.
 Sélectionne l'intégralité des fichiers présents dans le dossier courant

\-Aucun

Déselectionne tout les fichiers surligné par le curseur.

\-Inverser la sélection

Déselectionne tout les fichiers surligné par le curseur &amp; sélectionne à la place les élements précedemment non sélectionner.

#### **c. L'onglet Partage**

Avec l'onglet Partage, vous pouvez utiliser des applications pour partager des fichiers et des photos directement à partir de l'Explorateur de fichiers.

![](/upload/partagefichiers2.png "Option Partage de fichiers")

Vous pouvez notamment zipper (compresser un fichier), graver le dossier sur un disque, le télécopier, et choisir un utilisateur pouvant accéder a votre dossier partagé, ou cesser de partager.
 Sélectionnez les fichiers que vous souhaitez partager, accédez à l'onglet Partager, sélectionnez le bouton Partager, puis dans la liste, choisissez l'application concernée.

#### **d. L'onglet Affichage**

Avec l'onglet Affichage, vous pouvez modifier l'affichage de vos photos mais également activer ou non les volets de visualisation et de détails de vos fichiers.

![](/upload/windows-10-afficher-cacher-bibliotheques-lexplorateur-de-fichiers-1.jpg "Réafficher le volet de navigation dans la bibliothèque")

A noter que les bibliothèques ne s'affichent plus dans l'Explorateur de fichiers mais vous pouvez les ajouter manuellement .