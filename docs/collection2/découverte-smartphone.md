---
title: "📱 Smartphone : Fonctionnalités de base et Applications clés"
---
# Smartphone : Fonctionnalités de base et Applications clés

::: tip Modules APTIC

* *38 : Le smartphone : principes de fonctionnement*
* *74 : Smartphones et Tablettes sous Android*
* *76 : Smartphone : les applications clés*
* *78 : Smartphone : Fonctionnalités de base*
* *79 : Télécharger une application sur le store*
* *80 : Smartphone : réglage et configuration de l’appareil*



* Résumé :

Le smartphone a dépassé depuis 2018 l'ordinateur en matière d'accès à Internet.

Cependant malgré son apparente simplicité ce dernier peut comporter quelques paternes obscures qu'il convient de maîtriser.

* Nombre d'heures : 3 heures
* Public : Grand débutants
* Prérequis: Maîtrise minimum du Français : lu , écrit, parlé . 

  Posséder un smartphone (pour les ateliers). 

  Le lieu doit disposer d'un wifi local ou l'animateur d'un portable pouvant partager sa connexion*, l'application Zoom/un ordinateur et un vidéoprojecteur pour l'animateur ?*
* Objectifs de l'atelier

  * maîtriser les fonctionnalités de base d'un smartphone
  * savoir télécharger une application
  * connaître les applications-clef
:::

**Sommaire**

[[toc]] 

## 1. Les fonctionnalités de base

Objectifs intermédiaires : connaître le vocabulaire spécifique, configurer une sonnerie, un fond d'écran …

Un smartphone ou téléphone intelligent en français est un téléphone mobile disposant de fonctionnalités similaires à un ordinateur portable en plus de fonctionnalités comme l'appareil photo ou l'écran tactile.

::: details Rappel:

Le système d'exploitation mobile ou OS​
> Un système d'exploitation mobile est un système d'exploitation conçu pour fonctionner sur un appareil mobile. Ce type de système d'exploitation se concentre entre autres sur la gestion de la connectivité sans fil et celle des différents types d'interface.

:::

Vous avez démarré votre smartphone après avoir rentré le code PIN (mot de passe spécifique) vous atterrissez alors sur l'écran d'accueil appelé HomeScreen. Il permet d'accéder aux applications (logiciels pour smartphone), aux paramètres ou autres.

Sur cet écran peuvent être placés des raccourcis vers des applications, des dossiers ou des Widgets (des gadgets permettant d'obtenir la météo ou l'heure par exemple). Vous y trouverez aussi en haut une barre de notifications.

![](/upload/drawer.png "Drawer")

Selon les versions il est possible d'accéder à plusieurs tableaux virtuels en faisant glisser son doigt vers la droite ou la gauche.

Pour supprimer ou déplacer un élément sur le HomeScreen, il suffit de maintenir un appui long sur l'élément pour faire apparaître un menu contextuel et/ou des icônes d'actions possibles.

Pour revenir à l'Homescreen depuis n'importe quelle application il suffit de presser le bouton Home (bouton physique ou au milieu de l'appareil virtuel selon les versions).

![](/upload/homescreen2.png "Homescreen2")

L'écran de verrouillage ensuite, il apparaît quand votre appareil est en veille. En général il suffit de faire glisser son doigt pour le débloquer mais selon les options de sécurité paramétrées il est aussi possible de devoir rentrer votre code PIN ou un schéma de verrouillage. (Se configure dans Paramètres/Verrouiller l'écran)

L'écran contenant toutes les applications installées est appelé « drawer ». Il convient alors de distinguer deux types d'applications :

\-celles système intégrées d'office dans le portable, elles ne sont pas supprimables et sont appelées natives.

\-celles utilisateurs installées via un market (Play Store, Apple Store ...) ou manuellement qui peuvent être supprimées.

Pour y accéder il suffit de cliquer sur l'icône Applications qui ressemble à ceci 

![](/upload/iconeapplication.png)

![](/upload/menudesapplications.png "Drawer")

Pour démarrer une application il suffit de cliquer sur son icône.

Pour naviguer dans le drawer balayez avec votre doigt à droite ou à gauche.

*Leur faire déplacer une application du drawer sur le homescreen.*

* Les paramètres :

L'écran des paramètres est ce qui permet de définir toutes les options système et personnalisation de l'appareil. Des fonds d'écrans aux sonneries, du réseau aux options de sécurité, gestion des applications et mémoire, fonctionnalités spécifiques du terminal : c'est ici que tout se paramètre. Son raccourci est le :

![](/upload/parametressmartphone.png "Paramètres du smartphone")

*Vous pourrez aussi retrouver certaines fonctionnalités en tapant deux fois vers le bas du homescreen !*

\-Il y a les paramètres Réseaux : Wifi, Bluetooth (pour échanger localement des fichiers), Utilisation des données (pour connaître sa consommation 4G), Mode avion(utile pour recharger plus vite ou rechercher un réseau), partage de connexion Internet, VPN, Réseaux mobiles(réseaux préférés …)

\-Ceux de l'Appareil :

Écran d'accueil, Options de verrouillage d'écran, son, affichage, gestion des applications, visualisation du stockage interne et externe, de la batterie …

\-Ceux personnels :

Localisation, Chiffrage du Téléphone, Gestion des Comptes, Langues, …

\-Et enfin ceux Système :

Date, Heure, Accessibilité …

*Certains paramètres sont à manipuler avec précaution car ils pourraient avoir de lourdes conséquences sur le comportement de l'appareil.*

Enfin vous aurez sûrement envie de personnaliser un peu votre appareil. Voyons donc comment changer le fond d'écran et la sonnerie :

\-pour le fond d'écran Allez dans Paramètres, Affichage, Fond d'écran puis choisissez la photo souhaitée via l'appareil photo, la galerie ou une sélection de fonds préinstallés, recadrer l'image au besoin à l'aide de vos doigts puis cliquez sur ok. Vous pouvez aussi directement définir une image comme fond d'écran depuis la galerie.

![](/upload/changerfonddecransmartphone.png "Changer le fond d'écran du smartphone")

\-pour la sonnerie : Allez dans Paramètres, Son et notification et sélectionner la sonnerie de la carte SIM dans le répertoire ringtones.

*Les laissez personnaliser leur appareil*

## 2. Applications natives

Définition:
>C’est une application qui est conçue et développée spécifiquement pour un système d’exploitation. Par exemple les applications développées pour Iphone ne fonctionnera pas sur Android ou vice-versa. 
Une application native ne peut être distribuée que par l'intermédiaire des plateformes d'applications (voir Android Market et App Store) 
Lorsque vous achetez un smartphone sous Androïd,vous retrouverez des applications déjà installés sur ce dernier. ​

On en distingue de plusieurs types : 

* Les "applications" ou "outils" système :​
  Dans ce pack sont compris les applications relatives au bon fonctionnement de votre
  appareil et de sa prise en main (ex : horloge, contact, agenda…)
* Les applications du constructeur : 
  Dans cette catégorie on retrouve toutes les applications installées appartenant à la
  société fabriquant votre smartphone. ​C'est ce qui explique pourquoi on retrouve
  certaines applications quasi identiques en double dans le smartphone 
* Le pack d'applications google – liées au système d’explotation​. Ce pack est installé d'office sur votre smartphone. Vous y retrouverez toutes les
  applications appartenant à l'entreprise Google. 

### 1.Les messages

L'application SMS/MMS permet comme son nom l'indique d'envoyer des messages courts (SMS) ou des messages avec photos intégrées (MMS) à un ou des destinataire(s).

Pour écrire un message appuyer sur le bouton +, entrez le nom ou le numéro de votre contact puis rédigez votre message. A noter que si vous utilisez un caractère spécial ou envoyez une photo le nombre de caractères disponibles sera limité.

Parmi les autres options intéressantes il convient de noter la possibilité de bloquer les messages d'un destinataire ou de rechercher dans les messages (bien que cette fonction soit assez limitée).

Pour rappel enfin il est nécessaire d'avoir un abonnement téléphonique pour envoyer des messages via cette application (sans Internet à la différence de Whatsapp par exemple).

![](/upload/messagessmartphone.png "Ecran SMS Smartphone")

### 2.Le téléphone

L'application (parfois intégrée dans contacts) permet comme son nom l'indique d'appeler des personnes en tapant leur numéro puis en appuyant sur le bouton vert. Pour raccrocher il suffit d'appuyer sur le rouge.

A noter qu'il est possible avec les portables récents de prendre des notes tout en téléphonant ou même d'utiliser une autre application en parallèle.

### 3.Les contacts

L'application Contact est spécialisée dans la gestion des contacts comme son nom l'indique mais aussi des historiques d'appels.

Pour rajouter un contact appuyer là encore sur le bouton +, choisissez où stocker ce dernier (dans la mémoire interne du téléphone ou sur un compte de messagerie électronique) puis renseignez le nom, le numéro de téléphone et l'e-mail puis sur la validation en haut à droite.

Votre contact apparaîtra alors dans la colonne de droite.

![](/upload/contactsmartphone.png "Contacts Smartphone")

La colonne au centre (ou de gauche selon les versions) permet quant à elle de gérer les appels reçus et émis : c'est le journal de bord.

### 4.Le calendrier

Souvent appelée Agenda cette application permet de gérer vos rendez-vous. Tantôt directement liée à Google (Google Agenda) tantôt application issue des constructeurs son design est assez simpliste.

Pour créer un rdv cliquer sur le bouton +, événement, donnez un nom à celui-ci, définissez la plage horaire (de à …) les éventuelles occurrences (répéter), les rappels ou alarmes pour vous en avertir (répéter ou icône cloche), le lieu, les éventuels invités… puis cliquez en haut à droite sur valider (ou enregistrer).
Une fois l'événement créé vous pouvez facilement le déplacer en maintenant votre doigt appuyé dessus et en le déplaçant.

![](/upload/calendriersmartphone.png "Calendrier Smartphone")

Exercice : Créer un événement test ?

### 5.Les notes

L'application Notes permet comme son nom l'indique de prendre des notes écrites rapides mais aussi vocales(autorisation nécessaire:microphone).

Là encore le procédé est le même appuyez sur le bouton + pour créer la note, l'enregistrement est ici par contre le plus souvent automatique et il suffit de revenir en arrière pour l'enclencher.

![](/upload/notessmartphone.png "Notes Smartphone")

### 6)L'horloge

Parfois appelée Horloge, parfois Alarme, cette application permet de planifier un réveil, lancer un chronomètre ou indiquer l'heure selon le faisceau horaire. Là encore le principe est le même sur Alarme appuyez sur le bouton + pour en créer une nouvelle puis paramétrez l'heure et les minutes de celle-ci. Vous pouvez y rajouter, selon les versions des options comme Répéter (si vous l'avez oublié) ou le mode vibreur.

![](/upload/alarmessmartphone.png "Alarmes Smartphone")

Exercice : Créer une alarme sonnant à la fin de la séance ?

### 7.L'appareil photo / caméra

Pour lancer cette application cliquez sur l'icône représentant un objectif d'appareil photo. Vous serez alors dans le mode Photo par défaut. Pour prendre une photo pressez votre pouce sur le rond central pendant 1 seconde. Pour enregistrer une vidéo cliquez sur vidéo à côté du rond central. Parmi les options intéressantes le retardateur se trouve dans les paramètres de l'application.

Exercice : Prendre une photo et la partager par Bluetooth/MMS ?

![](/upload/appareilphotosmartphone.png "Appareil Photo sur Smartphone")

### 8.La galerie (ou album photo)

Une fois la photo prise celle-ci apparaîtra dans l'application Galerie qui permet de visualiser toutes celles prises par ordre chronologique et de créer des albums pour les organiser.

Si vous cliquez sur une photo prise vous aurez à 4 boutons minimum que sont le partage, la retouche d'image, la mise en favori et la suppression.

Ex : Envoyer une photo prise ?

### 9.Les documents, La musique et les vidéos

Vous pouvez accéder aux musiques, vidéos, documents téléchargés sur votre téléphone grâce à l'application Fichiers (ou Gestionnaire de fichiers).

Ceux-ci sont par défaut triés par ancienneté mais surtout catégories (vidéos, documents, musiques, apk, téléchargment, archives …).

En cliquant sur un fichier vous pourrez le lancer via l'application la plus appropriée mais en maintenant le clic ou en cliquant sur la droite vous aurez accès à des options de partage, de déplacement ou de suppression du fichier.

![](/upload/gestionnairedefichierssmartphone.jpg "Gestionnaire de fichiers sur smartphone")

### 10.Un navigateur internet

Le navigateur est comme déjà vu lors de précédents cours le logiciel qui permet de naviguer sur le web. Celui installé par défaut s'appelle le plus souvent Navigateur mais il peut aussi s'agir de Chrome sous Android. Vous atterrirez alors sur la barre du moteur de recherche ou vous rentrerez les mots-clefs vous permettant d'obtenir le résultat recherché.

Tout comme un navigateur sur ordinateur celui-ci doit être configuré afin de collecter le moins de données possibles.

### 11.Gmail

Application de messagerie de Google (il en existe bien d'autres) permettant d'envoyer et de recevoir des mails.

Quand vous lancez l'application vous atterrissez sur l'écran dit principal où vous visualisez les mails reçus par défaut.

![](/upload/gmailsmartphone.png "Gmail sur Smartphone")

En cliquant sur le bouton + vous pouvez écrire un nouveau mail en précisant comme déjà vu le destinataire (à), le sujet(objet), le contenu du message, insérer une pièce jointe(épingle), et l'envoyer en cliquant sur l'avion en papier. Si vous revenez en arrière cela enregistrera par défaut le message dans le dossier brouillon.En cliquant sur les 3 barres à Gauche dans le menu principal vous aurez accès à d'autres dossiers comme les brouillons, les messages envoyés, les pourriels(spams), la corbeille …

### 12.YouTube

Youtube est une application permettant regarder des vidéos mais aussi de les commenter ou de s'abonner à des producteurs de contenus.

Une fois l'application lancée vous tomberez sur l'écran d'accueil vous faisant des recommandations selon les vidéos que vous avez déjà vus …

On retrouve 4 autres boutons en bas de l'application : Tendances (les vidéos qui font le buzz), Abonnements(les producteurs que vous suivez), Boite de réception(pour gérer les commentaires) et Bibliothèque(pour enregistrer une vidéo qui pourrait vous plaire mais que vous n'avez pas le temps de lire).

![](/upload/youtubesmartphone.png "Page d'accueil de Youtube")

Si vous cliquez sur une vidéo, sous celle-ci vous pouvez accéder à plusieurs fonctions : le like(J'aime), le Dislike(je n'aime pas), le Partage, le Téléchargement(qui nécessite un compte payant) et l'Enregistrement(pour regarder + tard).

Vous pourrez aussi en scrollant vers le bas accéder aux commentaires de la vidéo (triés par récents ou par popularité) et écrire le propre votre.

Exercice : Leur faire rechercher une vidéo, l'enregistrer pour plus tard et leur faire trouver celle-ci dans la bibliothèque.

Installation de nouvelles applications : 30min

Comment se connecter en Wifi ?

![](/upload/se-connecter-en-wifi-smartphone.png "Se connecter au wifi avec son smartphone")

Dans les paramètres cliquez sur Wi-Fi et activez-le. Vous devriez alors détecter des réseaux.Connectez-vous au réseau XXX et rentrez le code Wifi pour vous y authentifiez. Si cela marque Connecté c'est que vous avez réussi. 

Objectifs intermédiaires : Savoir se connecter en Wifi, connaître la différence avec la 3G/G, connaître le PlayStore

*Les faire se connecter au Wifi local*

Afin d'installer de nouvelles applications vous devez posséder sur Android un compte Google afin d'accéder au magasin d'applications en ligne Google Play. Vous pouvez en créer un à défaut.

![](/upload/creationcomptegoogleplay.png "Creation Compte Google Play")

Ceci étant fait vous pourrez rechercher le(s) applications désirées en tapant leur nom sur la barre de recherche.

![](/upload/installation-application-smartphone.png "Installation application sur téléphone Android")

*Les faire rechercher et télécharger les applications SNCF, TinyScanner, BlueMail*

* Suppression et désinstallation d'applications :

Lorsque vous installez des applications externes vous disposez de 2 options : la supprimer c'est-à-dire juste l'effacer du homescreen (pour cela maintenez le doigt sur l'application et déplacez-la sur supprimer) ou la désinstaller( cette option n'est généralement disponible que sur le drawer, maintenant votre doigt sur l'application et déplacez-la sur désinstaller).

![](/upload/désinstaller-une-application.png)

Un autre paramètre à connaître quand on a un smartphone c'est la gestion de la batterie : certaines applications ou options sont en effet très énergivore et il convient de les désactiver quand vous n'en avez pas l'utilité

\-la Géolocalisation

\-le Wifi ou la 4G

\-l'Écran (baissez alors la luminosité)

\-le Bluetooth

\-certaines applications comme l'antivirus ou des jeux (vous pouvez forcer leur arrêt dans Paramètres, Application, forcer l'arrêt)

\-la veille GSM (à désactiver en cas de batterie très faible via le mode avion de votre téléphone)

De même pour la recharge de vos appareils, privilégiez le chargeur au câble USB (la recharge met en effet 4/5 fois plus de temps), quittez toutes les applications, éteignez l'écran voir l'appareil.

Parfois vous pouvez aussi voir apparaître des alertes appelées « notifications » envoyées par vos applications. Certaines sont utiles comme dans le cas d'une interruption de transport ou juste ennuyantes. Vous pouvez les configurer dans les paramètres des dites applications mais sachez qu'elles consomment nécessairement de la batterie et nécessite d'être connecté souvent pour fonctionner.

Certaines applications ont aussi besoin de mises à jour, néanmoins elles peuvent être lourdes ou se charger au mauvais moment aussi pensez à régler cela dans : Play Store/Trois Barres en haut à gauche/Paramètres/Mise à jour automatique des applications.

![](/upload/parametressmartphone2.png "Parametres Smartphone 2")

## 3. Les applications plus avancées

### **1. Blue Mail : Gestionnaire de boîtes mail**

Une fois l'application installée choisissez la messagerie de votre compte associé puis Renseignez votre **adresse mail** , le **mot de passe** associé à l'e-mail et appuyez sur **suivant.** 

Après synchronisation vous aurez alors accès à vos mails, toutefois quelques paramètres restent encore à régler comme : la fréquence de mise à jour ou la durée de conservation de vos mails. Pour se faire cliquez sur les 3 points en bas à droite puis Notification, paramètres réception instantanée (push ou manuel) et paramètres de l'application, avancé, synchronisation (1 semaine …).

![](/upload/bluemail.png "Blue Mail et ses paramètres")

Les laisser configurer leur boîte mail et leurs paramètres

### **2. Se déplacer : SNCF**

L'application SNCF permet principalement de connaître le meilleur trajet pour aller d'un point A à un point B et peut aussi vous tenir au courant du trafic. Néanmoins son ergonomie est assez complexe aussi allons-y par étapes.

Pour faire une recherche tapez sur « Où allez-vous » et rentrez l'adresse ou la gare de départ, celle d'arrivée, le jour et l'heure de départ ou d'arrivée (pour changer taper sur le mot départ), vous pouvez exclure certains modes de transports en cliquant sur modes de transports en vert là encore. Une fois fait tapez sur la loupe. Un algorithme (un programme informatique) va alors calculer l'itinéraire le plus court, le plus simple et des alternatives. Il vous conseillera un trajet mais vous laisse choisir, sachant que vous pouvez contourner certaines lignes au besoin en tapant sur contourner. Une fois le trajet choisi cliquez dessus, vous aurez alors accès au détail, n'oubliez pas d'enregistrer alors votre trajet en cliquant sur le bouton « enregistrer ». Le parcours choisi apparaîtra alors dans l'onglet « trajets » du menu central en bas à gauche deuxième icône.

Vous avez aussi dans le menu principal la possibilité d'enregistrer l'adresse de votre maison, celle de votre travail ou une autre via les icônes sous « où allez-vous » de sorte à vous éviter de les retaper lors de votre recherche.

![](/upload/applisncf.png "Appli SNCF")

En cas d'urgence absolue vous avez un bouton téléphone 3117 qui composera le dit numéro à droite d'où allez-vous.

Les trois barres à gauche d'où allez-vous vous donne accès à toutes les options de l'application comme les transports autour de vous, le plan des lignes, l'état du trafic ou encore même éditer des bulletins de retard !

*Leur faire chercher leur trajet retour sans bus par exemple et qu'ils l'enregistrent*

### **3. Scanner un document : TinnyScanner**

TinnyScanner est une application qui vous permet de scanner des documents administratifs après les avoir pris en photo en y appliquant plusieurs calques et correctifs. Une fois l'application téléchargée et lancée cliquez sur l'appareil photo en bas à gauche , prenez en photo le document à scanner (posé à plat et bien éclairé) en cliquant à nouveau sur l'appareil photo, cadrez le document, , cliquez sur le V en bas à droite, réglez les niveaux de gris en bas puis cliquez à nouveau sur le V en bas à droite, nommez le document puis sur le v en haut à droite et le tour est joué, vous pourrez alors partager le document avec le bouton partager en bas à droite ou rajouter d'autres pages au document avec l'appareil + en bas à gauche deuxième bouton. Votre document est désormais scanné.

![](/upload/applitinyscanner2.png "Appli Tiny Scanner")

*Que chacun fasse un test*

### **4. Gérer ses comptes : Bankin**

Une fois l'application téléchargée et lancée, cliquez sur se connecter, entrez votre adresse mail, créez un mot de passe puis cliquez sur ajouter une banque, sélectionner la votre dans la liste puis attendez l'importation des données, vous pourrez rajouter d'autre compte en cliquant sur + ajouter un compte, en cliquant sur le camembert à gauche vous pourrez voir la répartition de vos dépenses et revenus, pratique non ?

![](/upload/logobankin.png "Logo de l'application Bankin")