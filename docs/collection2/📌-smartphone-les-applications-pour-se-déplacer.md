---
title: "📌 Smartphone: Les applications pour se déplacer"
---
# Les applications pour se déplacer

::: tip Modules APTIC

* ???

* **Résumé:**

Cet atelier a pour but permettre aux participants d’avoir une vison d’ensemble des différentes applications utiles que l’on peut utiliser dans le cadre d’un déplacement.

* **Nombre d’heures:**

2h

* **Public:**

Grand débutant/débutant

Agile

* **Prérequis:**

Maîtrise minimum du Français: lu, écrit, parlé.

Avoir un smartphone récent permettant d’installer des applications

* **Objectifs de l’atelier:**

Permettre aux participants de connaître les principales applications utiles pour se déplacer en fonction du moyen de transport choisi.

* **Sommaire:**

[[toc]]

:::

## 1. Introduction

Le numérique façonne d’ores et déjà nos modes de vie par de nouvelles solutions qui couvrent autrement nos besoins de mobilité de la vie quotidienne: le commerce à distance, la voiture partagée, la santé à domicile, la formation assistée, le travail mobile. Tous sont des signes tangibles de transformations par le numérique des modes de vie d’un nombre croissant de nos concitoyens qui vivent de plus en plus connectés, du village à la grande agglomération.

Comprise comme moyen d’accès et résultante d’une activité, la mobilité des personnes se voit transformée dans son organisation et dans sa conception même, du fait du numérique.

Mais la transformation la plus importante est portée par de nouvelles générations de personnes dont les modes de vie sont déjà imprégnés par le numérique dans le but de produire, acheter, se cultiver, se soigner, échanger dans un esprit de culture de partage et de frugalité.

Les mobilités innovantes portées par le numérique s’imbriquent depuis plusieurs années dans la réalité des activités quotidiennes, celles qui fondamentalement génèrent le besoin de déplacement et impacteront ainsi leur organisation : le travail, la santé, la culture, le sport, les rencontres professionnelles ou personnelles etc. En effet, l’utilisateur du transport est d’abord un travailleur, un consommateur, un chaland ou un promeneur en quête de services urbains. C’est la nature même de son activité qui génère le besoin de mobilité.

Avec l’arrivée des nouveaux acteurs et des plateformes d’information numériques, chacun peut expérimenter et acquérir de l’expérience. La mobilité dans les territoires peut alors être pensée autrement, en impliquant davantage les citoyens-usagers par la mise en œuvre de solutions coconstruites avec eux, dont ils apprennent l’utilisation en l’expérimentant dans le déroulement de leur activité quotidienne. Le numérique permet, en temps réel, de relier l’apprenant à la connaissance dont il a besoin pour réaliser l’action qu’il envisage : c’est-à-dire l’apprentissage par l’action devient un levier inédit des démarches d’innovation et du développement de l’emploi, et un point de passage pertinent de l’adaptation des systèmes de mobilité dans les territoires.

En ce sens, les modes de vie sont les « traceurs » des dynamiques de transformation des activités humaines, et placent le citoyen-client-utilisateur au cœur de ces processus,

## 2. État des lieux du système de transport actuel et des améliorations apportées par le numérique

### 2.1. Un système actuel cloisonné entre voiture individuelle et transport collectif

En France et en Europe, la deuxième moitié du 20ème siècle a été marquée par le développement de

nouvelles possibilités de déplacement accessibles à la majeure partie de la population, avec la généralisation de la possession de l’automobile, et les modes de transport rapides.

L’automobile est devenue le mode de transport dominant pour les déplacements à courte et moyenne distance, avec, en France, un taux de possession de voiture par les ménages multiplié par 10 entre 1955 et 2005. Le chemin de fer a retrouvé sa place pour les longues distances avec le développement des trains à grande vitesse. Le transport aérien rapide et accessible a soutenu un développement accéléré du tourisme international.

Conjuguées avec l’évolution des modes de vie et les aspirations sociales des Français (maison individuelle, temps libre...), ces nouvelles possibilités de déplacement se sont notamment traduites :

\- Par un étalement urbain avec l’implantation d’activités en périphérie des villes (centres commerciaux, lieux de production et d’entreposage) et un habitat dispersé de plus en plus éloigné des lieux d’activité. En l’espace de 20 ans, la distance moyenne des déplacements quotidiens s’est considérablement accrue: chaque Français parcourait 35,5 km par jour en 1994, au lieu de 16,3 en Le budget-temps transport n’a cependant pas changé car l’allongement des distances a été compensé par une vitesse des déplacements plus importante.

\- Et par une forte croissance des déplacements de loisirs et de tourisme, compte tenu de l’augmentation du temps libre, du nombre des retraités et de la richesse, avec un développement de séjours plus fréquents et plus éloignés ; le transport rapide a été un facteur moteur dans la croissance des flux de transport à longue distance.

Ce mode de croissance n’apparaît plus soutenable à l’aube du 21ème siècle.

De plus le développement du transport collectif et ses limites, en effet le développement des transports collectifs classiques, très soutenu au cours des trente dernières années, atteint aujourd’hui ses limites économiques et environnementales.

Indispensables dans les territoires denses, les transports publics de proximité perdent leur pertinence économique dans les territoires périurbains et ruraux à faible densité

Les mobilités innovantes portées par le numérique s’imbriqueront dans la réalité des activités quotidiennes - celles qui fondamentalement génèrent le besoin de déplacement - et impacteront ainsi leur organisation : le travail, la santé, la culture, le sport, les rencontres professionnelles ou personnelles...

Avec un champ de préoccupations plus large que le seul transport, le concept de mobilité désigne les aptitudes et les possibilités des personnes d’accéder à leurs activités quotidiennes ou occasionnelles en utilisant différents modes de déplacement, voire l’absence de déplacement lorsque l’activité est accessible au lieu où elles se trouvent.

Il englobe les moyens et services de transport (utiliser une voiture, un autobus, un train etc.) mais aussi leurs combinaisons pour réaliser des déplacements porte à porte, et notamment les services immatériels liés aux technologies numériques qui facilitent l’accès et l’usage de différentes solutions possibles : localisation, itinéraire, horaire, correspondances, billets de transport etc.

La mobilité est et sera bouleversée par l’irruption des technologies numériques dans les modes de vie, comme elle l’a été par l’arrivée de l’automobile et du transport aérien au cours du siècle dernier.

Aujourd’hui, dans un contexte marqué par la mise en œuvre des lois sur l’environnement, la transition énergétique etc. La mobilité intelligente a fait l’objet de nombreux travaux, rapports, échanges et orientations ans que soit dégagée une vision d’ensemble sur les devenirs possibles des mobilités des personnes, compte tenu des jeux d’acteurs qui les déterminent.

L’attente des citoyens n’est pas d’augmenter leurs kilomètres parcourus, mais d’améliorer les conditions d’accès à leurs activités quotidiennes ou occasionnelles avec des déplacements plus vertueux, c'est-à-dire à la fois : 

* moins longs, notamment pour accéder au travail et aux services essentiels (santé, formation, culture et loisirs...) ; 
* plus fiables et plus sûrs, pour éviter la congestion des routes, les temps d’attente, l’encombrement des routes ou les aléas des transports collectifs ;

### 2.2. Le numérique ouvre de nouveaux champs à la mobilité

Le numérique porte des perspectives d’évolution des déplacements physiques importantes et encore peu explorées

Le rapport du Conseil économique social et environnemental « Révolution numérique et évolution des mobilités individuelles et collectives » (avril 2015) apporte une vision complète et très documentée sur le champ des possibles, moyennant une nouvelle distribution des rôles entre les acteurs traditionnels et les nouveaux entrants qui portent les innovations numériques.

\- Une plus grande efficacité des transports collectifs : l’information sur les flux pour mieux organiser la réponse aux pointes de trafic (y compris par une tarification modulée) ; l’informatisation des modes de régulation pour optimiser l’usage de l’infrastructure ; l’attractivité par la régularité, l’accroissement de la vitesse et une meilleure gestion des incidents ; de moindres coûts de gestion et de distribution des titres de transport ; une clarification du partage tarifaire entre opérateurs.

\- Une meilleure conception et utilisation de l’automobile : progrès pour la consommation d’énergie, le confort et la sécurité ; interface entre Smartphone, véhicule et route, utile pour optimiser infrastructures et investissements publics (meilleur taux de remplissage, fluidification du trafic, baisse de la congestion).

\- La mise en synergie des modes de transport. Afin de tirer parti de leurs diverses qualités, les collectivités favorisent ainsi de nouvelles solutions sous plusieurs formes : intégration d’autopartage dans leur offre de mobilité, exploitée par des opérateurs privés. Plans de déplacements d’entreprises et interentreprises. Intégration dans le passe mobilité de nouveaux services favorisant la multi-modalité.

\- L’automobile partagée, sous différentes formes de covoiturage et d’autopartage. Elle apporte des réponses pour réduire les inégalités dans la desserte des territoires (périurbain et rural) en complément des solutions de mobilité dans ces territoires où la voiture individuelle est quasiment le seul moyen de transport.

\- La limitation de la congestion routière. Réduire les émissions de gaz à effet de serre qu’elle génère en évitant des déplacements aux heures de pointe, soit par un étalement organisé des horaires de travail (dans le cadre de plans de déplacement d’entreprises ou interentreprises), soit par le développement du télétravail et la mise à disposition de tiers lieux Le développement des usages partagés (covoiturage, autopartage, vélo en libre-service...) a le même effet ; il permet d’augmenter le taux d’occupation des véhicules, d’en réduire le nombre et de libérer de l’espace sur la chaussée.
Google Maps, Waze, Coyotte, Moovit, Uber, BlaBlaCar... sont devenus incontournables. Ils servent les agilités, recensent, circonstancient et guident les arbitrages. Ils assurent les articulations de «mobilités combinées» au sein desquelles l'usager opère ses choix et organise ses trajets.

Leur puissance est liée à la capacité d’accumulation massive de données sur des territoires sans frontières administratives et leur capacité d’adaptation technologique et sociale. Ils se sont glissés naturellement dans l'espace ouvert par les demandes de mobilité (au sens de nouvelles possibilités d’accès à des activités «désirées»), en exploitant les champs d’activité à fort potentiel de croissance.

## 3. Présentation d’applications utiles pour les transports

### 3.1. Google Maps

Le géant du Web propose lui aussi via son application Google Maps, la possibilité de planifier un itinéraire avec les métros à Paris. En plus des itinéraires en métro, Google Maps propose les itinéraires en vélo, à pied et en voiture. En incluant les conditions de trafic en temps réel, l’utilisateur peut choisir entre prendre sa voiture ou les transports en commun et quelques soit le choix, Google Maps l’emmènera à bon port. La navigation GPS détaillée par guidage vocal simplifie les itinéraires en voiture à vélo ou à pied.

Cependant, l’application ne propose pas encore le temps réel, du moins dans la capitale parisienne ni les trajets en bus.

![Logo Google Maps](/upload/logo-appli-google-maps.jpg "Logo Google Maps")

![Capture de l'application Google Maps](/upload/google-maps-illustration.png "Capture de l'application Google Maps")

### 3.2. RATP - transport à Paris et Île de France

L’application officielle de la RATP permet de connaître en temps réel les horaires des prochains passages des RER, Métro, Noctilien, tramway et lignes aéroports. Grâce à la localisation, il est possible de repérer très aisément les stations à proximité et de trouver le meilleur itinéraire vers la destination souhaitée grâce à la recherche d’itinéraire détaillée. La personnalisation de l’application permet de recevoir gratuitement des alertes en cas de perturbation sur l’une des lignes habituelles fréquentées par l’utilisateur. En revanche, cette application ne propose pas la possibilité de combiner les moyens de transport.

![Logo RATP](/upload/logo-ratp.png "Logo RATP")

![Capture application RATP 1](/upload/ratp-capture.jpg "Capture application RATP 1")

![Capture application RATP 2](/upload/ratp-capture5.png "Capture application RATP 2")

### 3.3. Vianavigo

Avec Vianavigo vous allez pouvoir vous géolocaliser et repérer l'ensemble des stations Train, RER, Métro, Bus et Tramway proche de vous. Vous souhaitez rechercher un itinéraire en transports en communs, pas de problème cette dernière vous permet de personnaliser vos préférences de déplacement et de trouver le trajet qui vous convient le mieux avec les fonctions "*Partir plus tard* ", "*Arriver plus tôt*" en prenant soins de vous afficher les perturbations du trafic en cours.

Grâce à ses fonctionnalités avancées, Vianavigo vous donne la possibilité de retrouver en temps réel toutes les informations sur vos arrêts de bus, stations de métro ou bornes de vélos préalablement enregistrés dans vos favoris.

![Logo Via Navigo](/upload/logo-vianavigo.png "Logo Via Navigo")

![Capture application ViaNavigo](/upload/vianavigo-capture4.jpg "Capture application ViaNavigo")

### 3.4. City Mapper

Véritable couteau suisse de la mobilité, CityMapper facilite le déplacement en transport en commun au sein de la capitale. Lauréat de nombreux prix, l’application propose à son utilisateur de bénéficier de la liberté de mouvement en utilisant le bus, le métro, le RER, le tram, le Transilien, la marche, le vélo/vélib’, le taxi et même l’autolib’ pour planifier l’itinéraire. Plus besoin de chercher la borne vélib’ ou autolib’ à proximité, l’application indique les stations à côté disposant encore d’un vélo ou d’une voiture en libre-service. En partenariat avec Uber, les informations sur les voitures disponibles à proximité, le temps d’attente et une estimation du prix pour effectuer le trajet avec ladite compagnie de VTC sont affichés. En un toucher sur une des options, l’application Uber s’ouvre et prend le relai pour effectuer la réservation.

Et comme tous les chemins mènent à Rome, Citymapper suggère les meilleurs itinéraires et offre la possibilité d’emprunter un seul type de transport en commun soit le métro/tram ou soit le bus uniquement. D’autres fonctionnalités comme les horaires et informations en temps réel sont disponibles.

De nombreuses stations sont de véritables labyrinthes comme la station Châtelet qui dispose de pas moins de 17 sorties. La bonne nouvelle est que l’application permet d’indiquer la bonne entrée/sortie d’une station donnée et proposera prochainement des indications sur le positionnement au sein de la rame à prendre en fonction des sorties et des correspondances.

Les multiples options de personnalisation rendent le trajet très agréable. C’est une application performante qui ne cesse d’évoluer pour avoir en permanence une information de qualité.

![Logo City Mapper](/upload/logo-appli-citymapper.jpg "Logo City Mapper")

![Capture application City Mapper](/upload/citymapper-capture.png "Capture application City Mapper")

### 3.5. Moovit

L’application Moovit est une application de transport multimodale permettant de planifier un trajet, consulter les horaires des prochains départs et arrivées, le plan du réseau, les alertes trafics et naviguer étape par étape jusqu’à sa destination. En plus des fonctionnalités classiques comme la suggestion de l’itinéraire, cet assistant de transport ne se contente pas de donner la position des stations de métro ou de bus à proximité. Grâce à une communauté de 500 000 utilisateurs français, chaque utilisateur envoie de manière anonyme en temps réel les informations sur sa position via le GPS de son smartphone. Ainsi, l’application peut prendre en compte les éventuels retards ou difficultés du trafic. Les utilisateurs ont également la possibilité d’envoyer des informations sur le remplissage des lignes, la propreté des transports, la disponibilité des places pour les personnes à mobilité réduite, …

Autre avantage, l’application fonctionne également en cas de perte de connexion Internet une fois le trajet calculé car l’application stocke les données nécessaires à la navigation de son utilisateur.

Le concept de Moovit est semblable à celui de l’application communautaire Waze qui permet d’enrichir la carte via les informations des utilisateurs. Grâce à Moovit, les bonnes informations sont disponibles au bon moment.

![Logo Moovit](/upload/logo-appli-movit.jpg "Logo Moovit")

![Capture application Moovit](/upload/moovit-illustration.png "Capture application Moovit")

### 3.6. SNCF

La nouvelle application SNCF permet d’avoir les informations en temps réel au bout de son doigt. Avec son nouveau design, il est possible de trouver le meilleur itinéraire à Paris et en Ile-de-France en transport en commun, quel que soit le mode métro, train, RER, bus, tramway, autocar. Pratique pour anticiper ses déplacements du week end ou des vacances scolaires ou tout simplement pour vérifier si des modifications d’horaires habituels ont lieu. La personnalisation de l’application permet d’obtenir des alertes sur les perturbations, travaux, numéros de quais de départ. Toutes les évolutions du voyage sont consignées dans l’application afin d’optimiser les déplacements. L’application inclut en plus également les informations sur vos voyages en train et autocars opérés par le groupe SNCF et permet d’accéder très facilement aux applications de mobilité SNCF comme iDPASS.

![](/upload/logo-application-sncf.png)

![Capture application SNCF](/upload/sncf-capture1.jpg "Capture application SNCF")

## 4. Présentation d’applications utiles pour les déplacements autres

### 4.1. Google Maps

Le géant du Web propose lui aussi via son application Google Maps, la possibilité de planifier un itinéraire en voiture. Cette application fait plus que vous indiquer le bon chemin puisqu’il inclut également les conditions de trafic afin de connaitre les éventuels bouchons et ralentissements pour adapter votre itinéraire. Suite au rachat de la start-up Waze en 2013, la firme de Moutain View intègre les données en temps réel de l’application Waze qui se base sur une communauté d’utilisateurs. L’application reprend les fonctionnalités disponibles sur le site web Google Maps et notamment la fonctionnalité Google Street View qui vous permettra de visualiser des lieux ou votre destination comme si vous y étiez. Cette application inclut également les itinéraires à vélo, à pied ou en transport en commun.

![Logo google maps](/upload/logo-appli-google-maps.jpg "Logo google maps")

![Capture google maps 2](/upload/googlemap-capture-2.png "Capture google maps 2")

### 4.2. Waze

Pour les conducteurs, c’est une application des plus précieuses : un **GPS fiable qui vous informe du trafic en temps réel,** et une communauté qui partage des informations utiles Waze recherche aussi les stations-essence les moins chères dans les environs.

En tant qu'application de trafic et de navigation basée sur la communauté, Waze est conçue à l'origine comme un média social de navigation destiné aux voitures privées. Pour cette raison, l’outil de navigation n'inclut pas actuellement les voies réservées aux transports en commun, aux vélos ni aux poids lourds.

![](/upload/logo-waze.png)

![](/upload/waze-capture.png)

### 4.3. Openbike Now

L’application vous permet de **trouver tous les Vélib’ disponibles autour de vous**, et les **places disponibles pour le garer**. Basée sur l’open data de JCDecaux, vous aurez toutes les données en temps réel pour vous déplacer dans plus de 40 villes en France grâce aux Velib’, Velo’V, Vélô (pourquoi un nom différent par ville, c’est une question que je me pose…). Bref, l’application est indispensable pour vous déplacer à vélo en ville.

![](/upload/openbike-logo.png)

![](/upload/openbike-capture-2.png)

### 4.4. Getaround

Besoin d'une voiture pour une heure, une journée ou une semaine ? L'application Getaround vous permet de trouver une voiture au coin de la rue quand vous en avez besoin.

Getaround est la plateforme de carsharing leader dans le monde avec plus 5 millions d'utilisateurs et plus de 70 000 voitures disponibles dans plus de 300 villes.

Toutes les locations sont couvertes par Allianz.

Pour louer une voiture c'est très simple:

1. Recherchez des voitures autour de vous\
2. Choisissez la voiture qui vous convient\
3. Louez-la depuis l'appli\
4. Déverrouillez la voiture avec votre smartphone\
5. Laissez-la une fois terminé

![](/upload/getaround-logo.png)

![](/upload/getaroud-capture-1.png)

![](/upload/getaroud-capture-3.png)

::: tip 
***Sources internet***

<https://www.transportshaker-wavestone.com/top-5-applications-se-deplacer-a-paris/>

<https://www.applicationiphone.info/top-applications-gratuites-se-deplacer/>

<https://www.presse-citron.net/google-maps-guidera-transports-commun/>

***Sources littéraires***

*Histoire des transports et de la mobilité / Transport and mobility history* , Editeur Editions Alphil-Presses universitaires suisses, Auteur : Gérard Duc, Olivier Perroux, Hans-Ulrich Schiedt, François Walter (éd.)

*Histoire de la mobilité, Les transports modernes, ed . Cultures visuelles et médiatiques En Suisse et en Europe (1930-1960)*, Auteurs : Bauer Juliette, Ménétrey Samuel, Philippe Kaenel, François Vallotton
:::