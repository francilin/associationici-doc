---
title: 📝 E-admin Pratique
---
# LES DÉMARCHES ADMINISTRATIVES DE BASE

:::tip Modules APTIC

*85. Déclarer ses revenus en ligne et découvertes des services proposés*

*90. Gérer ses droits et allocations (CAF…) en ligne/sur internet*


*93 - Utiliser la plateforme Amelie.fr*

* **Format:** 3h
* **Public :** Grand Débutant/Débutant
* **Résumé**
  Découverte des sites de:

  * Pôle emploi, 
  * la CAF,
  * AMELI,
  * Impôts
  * Site du département du Val-de-Marne
* **Objectifs**

  * Connaître les fonctionnements premiers de quelques sites d'e-administration
  * Gagner en autonomie et de pouvoir réaliser seul certaines démarches.
  * Faciliter l'accès aux droit
* **Prérequis**

  * Maîtrise de la langue de française
  * Savoir utiliser un clavier et une souris
  * Avoir un smartphone

:::

**Sommaire :**

[[toc]]

## 1. Les principaux services dématérialisés

### 1.1 Pôle emploi

![Logo Pôle Emploi](/upload/pole-emploi-1.png "Pole emploi 1")

Le but de cette première étape est de faire le tour des applications mobiles créées à ce jour par l’État concernant le Pôle emploi.

![L'ensemble des applications pôle emploi](/upload/pole-emploi-2.png "Pole emploi 2")

Ici il faudra lister les trois applications les plus utilisées ( photo ci-dessus) et notamment les deux premières qui sont les plus courantes dans l’usage quotidien des chercheurs d’emplois.

![](/upload/pole-emploi-3.png "Pole emploi 3")

* L'application pole emploi "mon espace"

Cette application s’adresse aux demandeurs d’emploi inscrits à Pôle emploi qui souhaitent pouvoir accéder à leur dossier, gérer leurs démarches administratives et s’actualiser.

\- S’actualiser

\- Accéder à son courrier et attestations

\- Suivre une indemnisation

\- Rechercher une agence Pôle emploi

![](/upload/pole-emploi-4.png "Pole emploi 4")

![Se connecter à l'application](/upload/pole-emploi-5.png "Pole emploi 5")

![](/upload/pole-emploi-6.png "Pole emploi 6")

![](/upload/pole-emploi-7.png "Pole emploi 7")

![](/upload/pole-emploi-8.png "Pole emploi 8")

* L'application pole emploi "mes offres"

L'application dite « Mes Offres » vous permet de trouver rapidement les offres qui vous intéressent et de postuler simplement sur des milliers d'offres issues de Pôle emploi ou d'un de ses nombreux partenaires.

**L’espace « Rechercher » un emploi permet :**

* Recherche simple, rapide et complète parmi plus de 600 000 offres
* Enregistrez vos recherches afin de ne rater aucune opportunité de travail
* Visualisez tous les jours les nouvelles offres d'emploi parues depuis votre dernière recherche
* Soyez notifié des offres qui vous correspondent
* Mettez en favoris les CDI, CDD, missions d'intérim, temps partiels, et contrat saisonnier qui vous intéressent pour les retrouver facilement
* Les offres sont contrôlées tous les jours (légalité, réalité, unicité et fraîcheur) pour une vision réelle et unifiée du marché du travail

**L’espace «Postuler » à un emploi permet de :**

* Postuler par email, téléphone, ou grâce à votre profil de compétences Pôle emploi
* Importer votre CV
* Montrer votre motivation en personnalisant vos candidatures
* Visualiser votre profil et contrôler votre visibilité auprès des recruteurs

![L'application "mes offres"](/upload/pole-emploi-9.png "Pole emploi 9")

![Envoyer sa candidature avec l'application](/upload/pole-emploi-10.png "pôle emploi 10")

### 1.2 CAF

L'application Caf dite « Mon Compte » permet d'accéder de manière simple, pratique et sécurisée aux informations relatives à votre dossier d'allocataire.

L'application vous demande l'autorisation d'accéder à vos photos/multimédia/fichiers pour vous permettre de joindre des pièces justificatives lorsque cela est nécessaire et à l'état de votre réseau Wi-Fi pour vérifier votre connexion en cas d'erreur de l'application.

C’est-à-dire ?

![](/upload/caf-1.png "Caf 1")

* **Gérer ses informations professionnelles**

Vous pouvez « consultez votre profil et modifiez », si nécessaire :

\- votre adresse

\- vos coordonnées bancaires

\- vos coordonnées de contacts (adresse mél, téléphone)

Dans votre profil, vous pouvez également :

\- déclarer une grossesse

\- déclarer une naissance.

* **Faire ses démarches en ligne**

\- Suivez l'avancement de vos démarches faites sur caf.fr et soyez informés à chaque connexion ou via les notifications pushs en cas d'éléments manquants

\- Répondez aux questions de la Caf dans la rubrique Mes démarches et joignez les pièces justificatives demandées directement depuis votre smartphone

\- Bénéficiaire de RSA ou de prime d'activité, faites vos déclarations trimestrielles directement dans l'application

\- Etudiant, faites vos déclarations (boursier et maintien dans les lieux) directement depuis l'application

\- Utilisez l'application pour déclarer vos certificats de scolarité (16-18 ans) pour bénéficier de l'allocation de rentrée scolaire

\- Vous avez une dette vis-à-vis de la Caf ? Remboursez la en une ou plusieurs fois depuis l'application

\- Téléchargez vos attestations et votre relevé de compte Caf.

* **Simplifier les échanges avec la CAF**

En une seule rubrique, retrouvez vos échanges avec votre Caf (courriers, courriels, relevés…) et soyez alertés en cas de nouveaux messages

Retrouvez la liste des points d'accueil et des modes de contact de votre Caf.

* **Consulter les versements de la CAF**

Accédez rapidement à vos 10 derniers paiements (dates et montants) en parcourant votre historique jusqu'aux 24 derniers mois. Vous pouvez également télécharger sur votre mobile vos relevés ou attestations au format PDF.

* **Découvrir les prestations**

Retrouvez la liste des prestations versées par la Caf et leurs conditions d'accès.

![](/upload/caf-2.png "caf 2")

![](/upload/caf-3.png "caf 3")

### 1.3 Améli

**Définition**

L’application « le compte Ameli », c’est votre espace personnel sécurisé pour accéder à tous vos services depuis votre ordinateur, votre mobile ou votre tablette : consulter vos remboursements, télécharger vos attestations, obtenir votre carte européenne, contacter un conseiller par e-mail.

Avec votre compte Ameli, où que vous soyez, vous pouvez :

* consulter vos remboursements en temps réel, retrouver et enregistrer vos relevés mensuels ;
* télécharger votre attestation de droits ou d'indemnités journalières ;
* télécharger votre relevé fiscal, avec les prestations perçues durant l’année précédente que vous devez déclarer aux services fiscaux ou à d’autres organismes ;
* consulter l’avancement de votre dossier accident du travail ou l’historique des arrêts ;
* connaître la date estimée du traitement d’une démarche (changement de situation, arrêt de travail, remboursement de frais de santé..) dans votre CPAM (prochainement disponible sur l’appli du compte Ameli) ;
* consulter les informations sur votre [affection de longue durée](https://www.ameli.fr/assure/droits-demarches/maladie-accident-hospitalisation/affection-longue-duree-ald/affection-longue-duree-ald) (ALD).

![](/upload/amelie-1.png "Ameli 1")

![](/upload/amelie-2.png "Ameli 2")

### 1.4 Impôts

Impots.gouv est l’application officielle de la Direction Générale des Finances Publiques (DGFiP).\
\
Pour un bon fonctionnement de l'application, vous devez autoriser l'accès à l'appareil photo pour payer grâce au flashcode et vous identifier plus facilement.

Avec cette application d’Impots.gouv, une fois connecté à votre espace particulier en saisissant vos identifiants, vous pouvez :

#### **1.4.1 Consulter vos documents**

\- Consulter vos documents fiscaux des trois dernières années directement dans l'application\

* Partager vos documents (enregistrement au format PDF ou envoi par SMS ou courriel à un tiers).

#### **1.4.2 Déclarer vos revenus**

Si vous n’avez aucune modification ou complément (ou uniquement celle de la contribution à l’audiovisuel public) à apporter à votre déclaration pré-remplie ou à vos coordonnées bancaires, vous pouvez valider votre déclaration en quelques secondes :

\- Connectez-vous à votre espace Particulier soit en flashant indistinctement le code sur votre déclaration ou votre dernier avis d’imposition, soit en saisissant votre identifiant\

* Saisissez votre mot de passe. Si vous ne possédez pas de mot de passe, vous pouvez en créer un.

\- Sélectionnez le service « Déclarer mes revenus »

\- Vérifiez votre déclaration et validez-la !

#### **1.4.3 Payer vos impôts par Flashcode**

Pour payer simplement et rapidement votre impôt :

\- Flashez le code de l’avis d'impôt à régler

\- Vérifiez le montant et corrigez-le si nécessaire

\- Vérifiez, modifiez ou saisissez votre compte bancaire

\- Validez !

Le montant sera prélevé sur votre compte bancaire 10 jours après la date limite de paiement.\*\*\

#### **1.4.4 Accéder à tous vos impôts payables en ligne**

Pour payer simplement et rapidement tous vos impôts éligibles au paiement en ligne :\

* Sélectionnez le service « Payer mes impôts »

\- Visualisez vos avis d’impôt à payer en ligne

\- Sélectionnez chaque avis d’impôt que vous souhaitez payer, vérifiez le montant et corrigez-le si nécessaire

\- Vérifiez, modifiez ou saisissez votre compte bancaire

\- Validez !

Pour chaque avis payé, le montant sera prélevé sur votre compte bancaire 10 jours après la date limite de paiement.

#### **1.4.5 Consulter l’historique de vos derniers paiements effectués en ligne**

Pour consulter vos avis impôts déjà payés en ligne :

\- Sélectionnez le service « Payer mes impôts »

\- Visualisez le détail des derniers avis d’impôt payés en ligne

#### **1.4.6 Mettre à jour votre profil (adresse électronique, numéros de téléphone, mot de passe, options de dématérialisation)**

Pour mettre à jour votre profil :

\- Sélectionnez le service « Modifier mon profil »

\- D’un coup d’œil, visualisez les informations disponibles et celles que vous souhaitez compléter (rouge)

\- Enregistrez. Vos modifications sont immédiatement prises en compte. Un mail de validation vous est adressé pour toute nouvelle adresse électronique signalée

#### **1.4.7 Créer et modifier vos contrats de mensualisation et de prélèvement à l’échéance**

## 2. Les sites du département du Val-de-Marne (94)

### 2.1. Les actualités / évenements sur le territoire du Val-de-Marne

<https://www.valdemarne.fr/vivre-en-val-de-marne/informations>

### 2.2 Les services en ligne du site 

<https://www.valdemarne.fr/a-votre-service/services-en-ligne>

::: tip SOURCES

** Site officiels **

*[https://www.modernisation.gouv.fr/action-publique-2022/comprendre/action-publique-2022-un-programme-pour-accelerer-la-transformation-du-service-publi](https://www.modernisation.gouv.fr/action-publique-2022/comprendre/action-publique-2022-un-programme-pour-accelerer-la-transformation-du-service-public)*

*[https://www.afnic.fr/fr/produits-et-services/services/whois](https://www.afnic.fr/fr/produits-et-services/services/whois/)*

*[https://observatoire.numerique.gouv.fr/observatoir](https://observatoire.numerique.gouv.fr/observatoire/)*

*<https://franceconnect.gouv.fr/>*

*<https://ants.gouv.fr/monespace/s-inscrire>*

*<https://www.valdemarne.fr/vivre-en-val-de-marne/informations>*

*<https://www.valdemarne.fr/a-votre-service/services-en-ligne>*

*<https://www.valdemarne.fr/le-conseil-departemental/le-meilleur-pour-lavenir-des-eleves-des-colleges>*

*<https://www.valdemarne.fr/le-conseil-departemental/offres-d-emploi>*

*<https://www.valdemarne.fr/newsletters>*

*<https://mes-aides.gouv.fr/>*

*<https://www.service-public.fr/>*

*<https://www.demarches.interieur.gouv.fr/>*

*<https://www.ameli.fr/>*

** Sources complémentaires **

*<https://www.service-public.fr/particuliers/actualites/liste-e-administration>*

*<https://www.vie-publique.fr/eclairage/18925-e-administration-du-pagsi-au-programme-action-publique-2022>*

*<https://www.ena.fr/content/download/1958/32828/version/4/file/bib_administration%20electronique_2016_sf.pdf>*

*[https://fr.wikipedia.org/wiki/Administration_%C3%A9lectronique](https://fr.wikipedia.org/wiki/Administration_%C3%A9lectronique)*

*<https://usbeketrica.com/article/en-france-l-administration-sert-l-etat-en-estonie-elle-sert-le-citoyen>*

*<http://www.slate.fr/story/78990/estonie-administration-numerique-linnar-viik>*

*[https://www.cairn.info/revue-francaise-d-administration-publique-2004-2-page-225.htm#](https://www.cairn.info/revue-francaise-d-administration-publique-2004-2-page-225.htm)*

*:::*